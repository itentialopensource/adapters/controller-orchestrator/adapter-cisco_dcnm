# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Cisco_dcnm System. The API that was used to build the adapter for Cisco_dcnm is usually available in the report directory of this adapter. The adapter utilizes the Cisco_dcnm API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco DCNM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco DCNM.

With this adapter you have the ability to perform operations with Cisco DCNM on items such as:

- Fabrics
- Networks
- VRFs
- Alarms
- Policy
- Telemetry

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
