
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:39PM

See merge request itentialopensource/adapters/adapter-cisco_dcnm!13

---

## 0.5.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_dcnm!11

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:55PM

See merge request itentialopensource/adapters/adapter-cisco_dcnm!10

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_20:08PM

See merge request itentialopensource/adapters/adapter-cisco_dcnm!9

---

## 0.5.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!8

---

## 0.4.4 [03-28-2024]

* Changes made at 2024.03.28_13:41PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!7

---

## 0.4.3 [03-13-2024]

* Changes made at 2024.03.13_11:11AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!6

---

## 0.4.2 [03-11-2024]

* Changes made at 2024.03.11_15:58PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!5

---

## 0.4.1 [02-28-2024]

* Changes made at 2024.02.28_11:27AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!4

---

## 0.4.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!3

---

## 0.3.0 [07-10-2023]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!2

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!2

---

## 0.1.2 [03-03-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_dcnm!1

---

## 0.1.1 [02-23-2021]

- Initial Commit

See commit 14168f4

---
