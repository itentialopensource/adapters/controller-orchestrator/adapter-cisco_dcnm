# Cisco Data Center Network Management

Vendor: Cisco Systems
Homepage: https://www.cisco.com/

Product: Data Center Network Manager
Product Page: https://developer.cisco.com/docs/data-center-network-manager/latest/

## Introduction
We classify Cisco DCNM into the Data Center and Network Services domains as it facilitates efficient network operations, visibility, and fabric management across various data center deployments​.

## Why Integrate
The Cisco DCNM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco DCNM.

With this adapter you have the ability to perform operations with Cisco DCNM on items such as:

- Fabrics
- Networks
- VRFs
- Alarms
- Policy
- Telemetry

## Additional Product Documentation
The [API documents for Cisco DCNM](https://www.cisco.com/c/en/us/td/docs/dcn/dcnm/1154/restapi/cisco-dcnm-rest-api-guide-1154.html)
