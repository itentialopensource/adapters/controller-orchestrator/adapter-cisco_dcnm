/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_dcnm',
      type: 'CiscoDcnm',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const CiscoDcnm = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Cisco_dcnm Adapter Test', () => {
  describe('CiscoDcnm Class Tests', () => {
    const a = new CiscoDcnm(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const alarmsAcknowledgeAlarmsBodyParam = {
      alarmId: [
        'string'
      ],
      actBy: 'string'
    };
    describe('#acknowledgeAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.acknowledgeAlarms(alarmsAcknowledgeAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'acknowledgeAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsActivateDeactivatePoliciesBodyParam = [
      'string'
    ];
    describe('#activateDeactivatePolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateDeactivatePolicies(null, alarmsActivateDeactivatePoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FAIL', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'activateDeactivatePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsClearAlarmsBodyParam = {
      alarmId: [
        'string'
      ],
      actBy: 'string'
    };
    describe('#clearAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.clearAlarms(alarmsClearAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'clearAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsCreatePolicyBodyParam = {
      policyName: 'string',
      description: 'string',
      deviceIps: [
        'string'
      ],
      policyType: 'string',
      sensorIndex: 'string',
      severity: 'string',
      forwarding: true,
      active: false,
      hsrp: 'string',
      bgp: 'string',
      bfd: 'string',
      module: 'string',
      snmp: 'string',
      ssh: 'string',
      icmp: 'string',
      fan: 'string',
      power: 'string',
      cpuRows: [
        [
          'string'
        ]
      ],
      memRows: [
        [
          'string'
        ]
      ],
      tempRows: [
        [
          'string'
        ]
      ],
      interfaces: {},
      linkstate: 'string',
      bdwthRows: [
        [
          'string'
        ]
      ],
      inErrRows: [
        [
          'string'
        ]
      ],
      outErrRows: [
        [
          'string'
        ]
      ],
      inDisRows: [
        [
          'string'
        ]
      ],
      outDisRows: [
        [
          'string'
        ]
      ],
      syslogRaise: 'string',
      syslogClear: 'string',
      valueIdentity: 'string'
    };
    describe('#createPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicy(alarmsCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'createPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsDeleteAlarmsBodyParam = {
      alarmId: [
        'string'
      ],
      actBy: 'string'
    };
    describe('#deleteAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAlarms(alarmsDeleteAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FAIL', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'deleteAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enabledisablealarm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enabledisablealarm(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FAIL', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'enabledisablealarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportAlarmList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportAlarmList(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'exportAlarmList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recreateAlarmMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.recreateAlarmMapping((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'recreateAlarmMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rediscoverESurl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rediscoverESurl((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'rediscoverESurl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsUnAcknowledgeAlarmsBodyParam = {
      alarmId: [
        'string'
      ],
      actBy: 'string'
    };
    describe('#unAcknowledgeAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unAcknowledgeAlarms(alarmsUnAcknowledgeAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FAIL', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'unAcknowledgeAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlarmPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlarmPolicies(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'updateAlarmPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsUploadPolicyFileBodyParam = {
      fileName: 'string',
      fileContents: 'string'
    };
    describe('#uploadPolicyFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadPolicyFile(alarmsUploadPolicyFileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FAIL', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'uploadPolicyFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmByID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmByID(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmByID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAlarmList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmState((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmStatistics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmStatistics((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmStatistics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getcachedata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getcachedata((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getcachedata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoliciesSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPoliciesSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getPoliciesSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyDetails(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FAIL', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getPolicyDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicy(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FAIL', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaisedAlarmCountByCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaisedAlarmCountByCategory((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UNDEFINED', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getRaisedAlarmCountByCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaisedAlarmCountByCategoryAndSwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaisedAlarmCountByCategoryAndSwitch((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('object', typeof data.response.lastOperDataObject);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getRaisedAlarmCountByCategoryAndSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsUpdatePoliciesBodyParam = {
      policyName: 'string',
      description: 'string',
      deviceIps: [
        'string'
      ],
      policyType: 'string',
      sensorIndex: 'string',
      severity: 'string',
      forwarding: false,
      active: false,
      hsrp: 'string',
      bgp: 'string',
      bfd: 'string',
      module: 'string',
      snmp: 'string',
      ssh: 'string',
      icmp: 'string',
      fan: 'string',
      power: 'string',
      cpuRows: [
        [
          'string'
        ]
      ],
      memRows: [
        [
          'string'
        ]
      ],
      tempRows: [
        [
          'string'
        ]
      ],
      interfaces: {},
      linkstate: 'string',
      bdwthRows: [
        [
          'string'
        ]
      ],
      inErrRows: [
        [
          'string'
        ]
      ],
      outErrRows: [
        [
          'string'
        ]
      ],
      inDisRows: [
        [
          'string'
        ]
      ],
      outDisRows: [
        [
          'string'
        ]
      ],
      syslogRaise: 'string',
      syslogClear: 'string',
      valueIdentity: 'string'
    };
    describe('#updatePolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicies(null, alarmsUpdatePoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'updatePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudExtensionDiscoverCSRBodyParam = {
      fabricId: 9,
      routerHostOrIP: 'string',
      username: 'string',
      password: 'string',
      switches: [
        {
          reachable: false,
          auth: true,
          known: true,
          valid: false,
          selectable: false,
          sysName: 'string',
          ipaddr: 'string',
          platform: 'string',
          version: 'string',
          lastChange: 'string',
          hopCount: 7,
          deviceIndex: 'string',
          statusReason: 'string'
        }
      ],
      platform: 'string'
    };
    describe('#discoverCSR - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.discoverCSR(cloudExtensionDiscoverCSRBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudExtension', 'discoverCSR', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudExtensionDiscoverNonNexusBodyParam = {
      fabricId: 5,
      routerHostOrIP: 'string',
      username: 'string',
      password: 'string',
      switches: [
        {
          reachable: false,
          auth: true,
          known: true,
          valid: false,
          selectable: true,
          sysName: 'string',
          ipaddr: 'string',
          platform: 'string',
          version: 'string',
          lastChange: 'string',
          hopCount: 9,
          deviceIndex: 'string',
          statusReason: 'string'
        }
      ],
      platform: 'string'
    };
    describe('#discoverNonNexus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.discoverNonNexus(cloudExtensionDiscoverNonNexusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudExtension', 'discoverNonNexus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetStats((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudExtension', 'resetStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudExtensionIpAddress = 'fakedata';
    describe('#stats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stats(cloudExtensionIpAddress, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudExtension', 'stats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configDeployerTemplateName = 'fakedata';
    const configDeployerDeployExecConfigBodyParam = {
      deviceList: [
        'string'
      ],
      paramValueMap: {}
    };
    describe('#deployExecConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployExecConfig(configDeployerTemplateName, configDeployerDeployExecConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigDeployer', 'deployExecConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupTracker - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.backupTracker((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlDCNMTracker', 'backupTracker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdiscoveryEnableStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdiscoveryEnableStatus((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlDCNMTracker', 'getdiscoveryEnableStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlDCNMTrackerFabricName = 'fakedata';
    const controlDCNMTrackerManageTrackerBodyParam = {
      action: 'INSTALL',
      switchList: [
        'string'
      ]
    };
    describe('#manageTracker - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.manageTracker(controlDCNMTrackerFabricName, controlDCNMTrackerManageTrackerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlDCNMTracker', 'manageTracker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getStatus(controlDCNMTrackerFabricName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlDCNMTracker', 'getStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsFabricName = 'fakedata';
    const controlFabricsGroupName = 'fakedata';
    describe('#createInterfaceGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createInterfaceGroup(controlFabricsFabricName, controlFabricsGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'createInterfaceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsCreateIfGroupIfAssocBodyParam = {
      switchandPortList: [
        {
          switchSN: 'string',
          switchPort: 'string'
        }
      ]
    };
    describe('#createIfGroupIfAssoc - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createIfGroupIfAssoc(controlFabricsFabricName, controlFabricsGroupName, controlFabricsCreateIfGroupIfAssocBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'createIfGroupIfAssoc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsCreateIfGroupNwAssocBodyParam = [
      'string'
    ];
    describe('#createIfGroupNwAssoc - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createIfGroupNwAssoc(controlFabricsFabricName, controlFabricsGroupName, controlFabricsCreateIfGroupNwAssocBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'createIfGroupNwAssoc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configDeploy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.configDeploy(controlFabricsFabricName, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'configDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsSwitchId = 'fakedata';
    describe('#postconfigDeploy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postconfigDeploy(controlFabricsFabricName, controlFabricsSwitchId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'postconfigDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configSave - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.configSave(controlFabricsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'configSave', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsSerialNumber = 'fakedata';
    describe('#deployMaintenanceModePti - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployMaintenanceModePti(controlFabricsFabricName, controlFabricsSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'deployMaintenanceModePti', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsTemplateName = 'fakedata';
    const controlFabricsCreateFabricWithNvPairsBodyParam = {};
    describe('#createFabricWithNvPairs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createFabricWithNvPairs(controlFabricsFabricName, controlFabricsTemplateName, controlFabricsCreateFabricWithNvPairsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'createFabricWithNvPairs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFabrics - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllFabrics((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getAllFabrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFabricAssociations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listFabricAssociations((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'listFabricAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsTaskId = 'fakedata';
    const controlFabricsSerialnum = 'fakedata';
    describe('#getPoliciesForRestore - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPoliciesForRestore(controlFabricsTaskId, controlFabricsSerialnum, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getPoliciesForRestore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchAsnIneBGPRoutedFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchAsnIneBGPRoutedFabric(controlFabricsSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getSwitchAsnIneBGPRoutedFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIfGroupInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIfGroupInterfaces(controlFabricsFabricName, controlFabricsGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getIfGroupInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInterfaceGroups(controlFabricsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getInterfaceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFabric(controlFabricsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessMode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccessMode(controlFabricsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getAccessMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configDiff - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.configDiff(controlFabricsFabricName, controlFabricsSwitchId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'configDiff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configPreview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.configPreview(controlFabricsFabricName, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'configPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsSourceName = 'fakedata';
    describe('#getconfigPreview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getconfigPreview(controlFabricsFabricName, controlFabricsSourceName, controlFabricsSwitchId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getconfigPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getconfigPreview1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getconfigPreview1(controlFabricsFabricName, controlFabricsSwitchId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'getconfigPreview1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsSetFabricFreezeModeBodyParam = {
      readOnly: true
    };
    describe('#setFabricFreezeMode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setFabricFreezeMode(controlFabricsFabricName, controlFabricsSetFabricFreezeModeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'setFabricFreezeMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsUpdateFabricWithNvPairsBodyParam = {};
    describe('#updateFabricWithNvPairs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateFabricWithNvPairs(controlFabricsFabricName, controlFabricsTemplateName, controlFabricsUpdateFabricWithNvPairsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'updateFabricWithNvPairs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInterfaceServiceAddInterfaceBodyParam = {
      policy: 'string',
      interfaceType: 'SUBINTERFACE'
    };
    describe('#addInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addInterface(controlInterfaceServiceAddInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'addInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInterfaceServiceAdminstatusBodyParam = {
      operation: 'shut'
    };
    describe('#adminstatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adminstatus(controlInterfaceServiceAdminstatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'adminstatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInterfaceServiceAddBreakoutBodyParam = [
      {
        serialNumber: 'string',
        ifName: 'string',
        map: '50g-2x'
      }
    ];
    describe('#addBreakout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addBreakout(controlInterfaceServiceAddBreakoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'addBreakout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInterfaceServiceDeployBodyParam = [
      {
        serialNumber: 'string',
        ifName: 'string'
      }
    ];
    describe('#deploy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deploy(controlInterfaceServiceDeployBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'deploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInterfaceServiceUpdateInterfaceBodyParam = {
      policy: 'string'
    };
    describe('#updateInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInterface(controlInterfaceServiceUpdateInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'updateInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInterfaceServiceSerialNumber = 'fakedata';
    describe('#getInterfaceUpdateDTO - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInterfaceUpdateDTO(controlInterfaceServiceSerialNumber, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'getInterfaceUpdateDTO', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobaInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGlobaInterface(controlInterfaceServiceSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'getGlobaInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCombinedSerialNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCombinedSerialNumber(controlInterfaceServiceSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'getCombinedSerialNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInventoryFabricName = 'fakedata';
    const controlInventoryDiscoverSwitchesBodyParam = {
      seedIP: 'string',
      snmpV3AuthProtocol: 4,
      username: 'string',
      password: 'string',
      maxHops: 9,
      cdpSecondTimeout: 9,
      preserveConfig: false,
      switches: [
        {
          reachable: false,
          auth: false,
          known: false,
          valid: true,
          selectable: true,
          sysName: 'string',
          ipaddr: 'string',
          platform: 'string',
          version: 'string',
          lastChange: 'string',
          hopCount: 3,
          deviceIndex: 'string',
          statusReason: 'string'
        }
      ],
      platform: 'string'
    };
    describe('#discoverSwitches - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.discoverSwitches(controlInventoryFabricName, controlInventoryDiscoverSwitchesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInventory', 'discoverSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInventoryStartPoapBodyParam = [
      {
        serialNumber: 'string',
        model: 'string',
        version: 'string',
        hostname: 'string',
        ipAddress: 'string',
        password: 'string',
        discoveryUsername: 'string',
        discoveryPassword: 'string',
        discoveryAuthProtocol: 1,
        data: 'string'
      }
    ];
    describe('#startPoap - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.startPoap(controlInventoryFabricName, controlInventoryStartPoapBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInventory', 'startPoap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSwitches - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllSwitches(controlInventoryFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInventory', 'getAllSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#switchesWaitingToPOAP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.switchesWaitingToPOAP(controlInventoryFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInventory', 'switchesWaitingToPOAP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlLinksCreateLinkBodyParam = {
      linkUUID: 'string',
      linkType: 'string',
      sourceFabric: 'string',
      destinationFabric: 'string',
      sourceDevice: 'string',
      destinationDevice: 'string',
      sourceSwitchName: 'string',
      destinationSwitchName: 'string',
      sourceInterface: 'string',
      destinationInterface: 'string',
      templateName: 'string',
      nvPairs: {}
    };
    describe('#createLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createLink(controlLinksCreateLinkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'createLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlLinksCreateImportLinksBodyParam = {};
    describe('#createImportLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createImportLinks(controlLinksCreateImportLinksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'createImportLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllLinks(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'getAllLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlLinksFabricName = 'fakedata';
    describe('#createExportLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createExportLinks(controlLinksFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'createExportLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFabricLinks(controlLinksFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'getFabricLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLogicalLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllLogicalLinks(controlLinksFabricName, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'getAllLogicalLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlLinksLinkUUID = 'fakedata';
    const controlLinksUpdateLinkBodyParam = {
      linkUUID: 'string',
      linkType: 'string',
      sourceFabric: 'string',
      destinationFabric: 'string',
      sourceDevice: 'string',
      destinationDevice: 'string',
      sourceSwitchName: 'string',
      destinationSwitchName: 'string',
      sourceInterface: 'string',
      destinationInterface: 'string',
      templateName: 'string',
      nvPairs: {}
    };
    describe('#updateLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateLink(controlLinksLinkUUID, controlLinksUpdateLinkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'updateLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLink(controlLinksLinkUUID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'getLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteFabricName = 'fakedata';
    const controlMultisiteDeleteNetworksBodyParam = [
      'string'
    ];
    describe('#deleteNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworks(controlMultisiteFabricName, controlMultisiteDeleteNetworksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deleteNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteDeleteVrfsBodyParam = [
      'string'
    ];
    describe('#deleteVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVrfs(controlMultisiteFabricName, controlMultisiteDeleteVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deleteVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteDeployMultiSiteConfigsBodyParam = {
      multisiteLoopbackIp: 'string',
      switchList: [
        {
          serialNumber: 'string',
          controlPlaneLoopbackIp: 'string',
          dataPlaneLoopbackIp: 'string',
          dataPlaneLoopbackVip: 'string'
        }
      ]
    };
    describe('#deployMultiSiteConfigs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployMultiSiteConfigs(controlMultisiteFabricName, controlMultisiteDeployMultiSiteConfigsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deployMultiSiteConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteDeployDciOverlayConfigsBodyParam = [
      {
        serialNumber: 'string',
        bgpEvpnPeerList: [
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 1,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 2,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 2,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 6,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 3,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 6,
            'remote-site': 'string'
          }
        ]
      }
    ];
    describe('#deployDciOverlayConfigs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployDciOverlayConfigs(controlMultisiteFabricName, controlMultisiteDeployDciOverlayConfigsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deployDciOverlayConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteDeployDciUnderlayConfigsBodyParam = [
      {
        serialNumber: 'string',
        ports: [
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 1,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 5,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 3,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 2,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 1,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 6,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 2,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 8,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 10,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          },
          {
            address: 'string',
            mtu: 'string',
            id: 'string',
            bgp: {
              remoteAddress: 'string',
              remoteAsn: 1,
              auth: {
                authType: 'string',
                authKeyID: 'string',
                authKeyValue: 'string'
              }
            }
          }
        ]
      }
    ];
    describe('#deployDciUnderlayConfigs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployDciUnderlayConfigs(controlMultisiteFabricName, controlMultisiteDeployDciUnderlayConfigsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deployDciUnderlayConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteDeployNetworksBodyParam = [
      {
        vrf: 'string',
        networkName: 'string',
        networkId: 5,
        isLayer2Only: true,
        networkTemplate: 'string',
        networkExtensionTemplate: 'string',
        tenantName: 'string',
        networkTemplateConfig: {
          vlanId: 8,
          gatewayIpAddress: 'string',
          tag: 2,
          secondarygatewayIpAddress: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          gatewayIpV6Address: [
            'string'
          ],
          vlanName: 'string',
          intfDescription: 'string',
          mtu: 9,
          suppressArp: true,
          enableL3OnBorder: false,
          dhcpConfigs: {
            dhcpServers: [
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              },
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              },
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              },
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              },
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              },
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              },
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              }
            ],
            loopbackId: 10
          },
          tnRoutedMCast: false
        },
        networkAttachments: [
          {
            switchSerialNumber: 'string',
            vlan: 3,
            interfaces: [
              'string',
              'string'
            ]
          }
        ],
        status: 'string'
      }
    ];
    describe('#deployNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployNetworks(controlMultisiteFabricName, controlMultisiteDeployNetworksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deployNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteAddFabricToSiteGroupBodyParam = {
      controllerId: 'string',
      dcnmIP: 'string',
      siteGroupName: 'string',
      siteId: 'string',
      l2VniRange: 'string',
      l3VniRange: 'string',
      multisiteLoopbackPool: 'string',
      kafkaTopic: 'string',
      msiteAnycastMac: 'string',
      peeringType: 'string',
      routeServers: [
        {
          ipAddress: 'string',
          asn: 3,
          fabricName: 'string',
          interfaceName: 'string',
          switchSerialNumber: 'string',
          switchName: 'string'
        }
      ]
    };
    describe('#addFabricToSiteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addFabricToSiteGroup(controlMultisiteFabricName, controlMultisiteAddFabricToSiteGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'addFabricToSiteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteDeployVrfsBodyParam = [
      {
        vrfName: 'string',
        vrfId: 7,
        vrfTemplate: 'string',
        vrfExtensionTemplate: 'string',
        tenantName: 'string',
        vrfTemplateConfig: {
          vrfDescription: 'string',
          tag: 6,
          vrfRouteMap: 'string',
          trmEnabled: true,
          isRPExternal: true,
          rpAddress: 'string',
          multicastGroup: 'string',
          routeTargetImport: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          routeTargetExport: [
            'string',
            'string'
          ]
        },
        vrfAttachments: [
          {
            switchSerialNumber: 'string',
            vlan: 9,
            status: 'string'
          },
          {
            switchSerialNumber: 'string',
            vlan: 8,
            status: 'string'
          },
          {
            switchSerialNumber: 'string',
            vlan: 1,
            status: 'string'
          },
          {
            switchSerialNumber: 'string',
            vlan: 5,
            status: 'string'
          },
          {
            switchSerialNumber: 'string',
            vlan: 5,
            status: 'string'
          },
          {
            switchSerialNumber: 'string',
            vlan: 1,
            status: 'string'
          }
        ],
        status: 'string'
      }
    ];
    describe('#deployVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployVrfs(controlMultisiteFabricName, controlMultisiteDeployVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deployVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabrics - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFabrics(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getFabrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetFabric(controlMultisiteFabricName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getgetFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultiSiteConfigs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMultiSiteConfigs(controlMultisiteFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getMultiSiteConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteUpdateDciOverlayConfigsBodyParam = [
      {
        serialNumber: 'string',
        bgpEvpnPeerList: [
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 1,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 4,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 2,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 6,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 5,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 6,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 9,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 6,
            'remote-site': 'string'
          },
          {
            neighbor: 'string',
            remoteController: 'string',
            remoteSwitchName: 'string',
            remoteLoopbackId: 'string',
            remoteSwitchSerial: 'string',
            'remote-as': 5,
            'remote-site': 'string'
          }
        ]
      }
    ];
    describe('#updateDciOverlayConfigs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDciOverlayConfigs(controlMultisiteFabricName, null, controlMultisiteUpdateDciOverlayConfigsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'updateDciOverlayConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDciOverlayConfigs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDciOverlayConfigs(controlMultisiteFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getDciOverlayConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDciUnderlayConfigs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDciUnderlayConfigs(controlMultisiteFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getDciUnderlayConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListofFaults - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getListofFaults(controlMultisiteFabricName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getListofFaults', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetAllSwitches - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetAllSwitches(controlMultisiteFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getgetAllSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteUpdateNetworksBodyParam = [
      {
        vrf: 'string',
        networkName: 'string',
        networkId: 10,
        isLayer2Only: false,
        networkTemplate: 'string',
        networkExtensionTemplate: 'string',
        tenantName: 'string',
        networkTemplateConfig: {
          vlanId: 10,
          gatewayIpAddress: 'string',
          tag: 6,
          secondarygatewayIpAddress: [
            'string',
            'string'
          ],
          gatewayIpV6Address: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          vlanName: 'string',
          intfDescription: 'string',
          mtu: 7,
          suppressArp: false,
          enableL3OnBorder: false,
          dhcpConfigs: {
            dhcpServers: [
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              },
              {
                dhcpServerAddr: 'string',
                vrfDhcp: 'string'
              }
            ],
            loopbackId: 10
          },
          tnRoutedMCast: true
        },
        networkAttachments: [
          {
            switchSerialNumber: 'string',
            vlan: 9,
            interfaces: [
              'string',
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 9,
            interfaces: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 4,
            interfaces: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 5,
            interfaces: [
              'string',
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 3,
            interfaces: [
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 6,
            interfaces: [
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 9,
            interfaces: [
              'string',
              'string',
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 1,
            interfaces: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 1,
            interfaces: [
              'string',
              'string',
              'string',
              'string',
              'string'
            ]
          },
          {
            switchSerialNumber: 'string',
            vlan: 9,
            interfaces: [
              'string'
            ]
          }
        ],
        status: 'string'
      }
    ];
    describe('#updateNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworks(controlMultisiteFabricName, controlMultisiteUpdateNetworksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'updateNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworks(controlMultisiteFabricName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVniUsage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVniUsage(controlMultisiteFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getVniUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteUpdateSiteGroupBodyParam = {
      controllerId: 'string',
      dcnmIP: 'string',
      siteGroupName: 'string',
      siteId: 'string',
      l2VniRange: 'string',
      l3VniRange: 'string',
      multisiteLoopbackPool: 'string',
      kafkaTopic: 'string',
      msiteAnycastMac: 'string',
      peeringType: 'string',
      routeServers: [
        {
          ipAddress: 'string',
          asn: 3,
          fabricName: 'string',
          interfaceName: 'string',
          switchSerialNumber: 'string',
          switchName: 'string'
        }
      ]
    };
    describe('#updateSiteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSiteGroup(controlMultisiteFabricName, controlMultisiteUpdateSiteGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'updateSiteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteUpdateVrfsBodyParam = [
      {
        vrfName: 'string',
        vrfId: 2,
        vrfTemplate: 'string',
        vrfExtensionTemplate: 'string',
        tenantName: 'string',
        vrfTemplateConfig: {
          vrfDescription: 'string',
          tag: 5,
          vrfRouteMap: 'string',
          trmEnabled: false,
          isRPExternal: false,
          rpAddress: 'string',
          multicastGroup: 'string',
          routeTargetImport: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          routeTargetExport: [
            'string',
            'string',
            'string'
          ]
        },
        vrfAttachments: [
          {
            switchSerialNumber: 'string',
            vlan: 5,
            status: 'string'
          },
          {
            switchSerialNumber: 'string',
            vlan: 6,
            status: 'string'
          },
          {
            switchSerialNumber: 'string',
            vlan: 2,
            status: 'string'
          }
        ],
        status: 'string'
      }
    ];
    describe('#updateVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateVrfs(controlMultisiteFabricName, controlMultisiteUpdateVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'updateVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVrfs(controlMultisiteFabricName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetGlobaInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetGlobaInterface(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getgetGlobaInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteSiteGroupName = 'fakedata';
    describe('#convertMsoSiteGroupToMsd - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.convertMsoSiteGroupToMsd(controlMultisiteSiteGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'convertMsoSiteGroupToMsd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplates((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlMultisiteTemplateName = 'fakedata';
    describe('#getTemplatesFields - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplatesFields(controlMultisiteTemplateName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'getTemplatesFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesPostControlPoliciesBodyParam = {
      id: 6,
      policyId: 'string',
      description: 'string',
      serialNumber: 'string',
      entityType: 'string',
      entityName: 'string',
      templateName: 'string',
      templateContentType: 'string',
      nvPairs: {},
      generatedConfig: 'string',
      autoGenerated: true,
      deleted: true,
      source: 'string',
      priority: 7,
      status: 'string',
      statusOn: 'string',
      createdOn: 'string',
      modifiedOn: 'string',
      secondaryEntityType: 'string',
      secondaryEntityName: 'string',
      hierarchicalKey: 'string'
    };
    describe('#postControlPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postControlPolicies(controlPoliciesPostControlPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'postControlPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesDeployPoliciesBodyParam = [
      'string'
    ];
    describe('#deployPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployPolicies(controlPoliciesDeployPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'deployPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesSerialNumbers = 'fakedata';
    const controlPoliciesSource = 'fakedata';
    const controlPoliciesCreatePoliciesInSwitchesAndSourceBodyParam = {
      id: 9,
      policyId: 'string',
      description: 'string',
      serialNumber: 'string',
      entityType: 'string',
      entityName: 'string',
      templateName: 'string',
      templateContentType: 'string',
      nvPairs: {},
      generatedConfig: 'string',
      autoGenerated: false,
      deleted: true,
      source: 'string',
      priority: 5,
      status: 'string',
      statusOn: 'string',
      createdOn: 'string',
      modifiedOn: 'string',
      secondaryEntityType: 'string',
      secondaryEntityName: 'string',
      hierarchicalKey: 'string'
    };
    describe('#createPoliciesInSwitchesAndSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPoliciesInSwitchesAndSource(controlPoliciesSerialNumbers, controlPoliciesSource, controlPoliciesCreatePoliciesInSwitchesAndSourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'createPoliciesInSwitchesAndSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateKey(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'generateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesSerialNumber = 'fakedata';
    describe('#getPtiHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPtiHistory(controlPoliciesSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'getPtiHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesUpdatePoliciesInSwitchesAndSourceBodyParam = {
      id: 10,
      policyId: 'string',
      description: 'string',
      serialNumber: 'string',
      entityType: 'string',
      entityName: 'string',
      templateName: 'string',
      templateContentType: 'string',
      nvPairs: {},
      generatedConfig: 'string',
      autoGenerated: false,
      deleted: false,
      source: 'string',
      priority: 6,
      status: 'string',
      statusOn: 'string',
      createdOn: 'string',
      modifiedOn: 'string',
      secondaryEntityType: 'string',
      secondaryEntityName: 'string',
      hierarchicalKey: 'string'
    };
    describe('#updatePoliciesInSwitchesAndSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePoliciesInSwitchesAndSource(controlPoliciesSerialNumbers, controlPoliciesSource, controlPoliciesUpdatePoliciesInSwitchesAndSourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'updatePoliciesInSwitchesAndSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipleSwitchPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMultipleSwitchPolicies(controlPoliciesSerialNumber, controlPoliciesSource, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'getMultipleSwitchPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markDeleteSwitchPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.markDeleteSwitchPolicies(controlPoliciesSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'markDeleteSwitchPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchPolicies(controlPoliciesSerialNumber, controlPoliciesSource, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'getSwitchPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchIntentConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchIntentConfig(controlPoliciesSerialNumber, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'getSwitchIntentConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesEntityType = 'fakedata';
    const controlPoliciesEntityName = 'fakedata';
    describe('#markDeleteSwitchPolicies4Entity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.markDeleteSwitchPolicies4Entity(controlPoliciesSerialNumber, controlPoliciesEntityType, controlPoliciesEntityName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'markDeleteSwitchPolicies4Entity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchPolicies4Entity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchPolicies4Entity(controlPoliciesSerialNumber, controlPoliciesEntityType, controlPoliciesEntityName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'getSwitchPolicies4Entity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesPolicyId = 'fakedata';
    const controlPoliciesUpdatePolicyBodyParam = {
      id: 9,
      policyId: 'string',
      description: 'string',
      serialNumber: 'string',
      entityType: 'string',
      entityName: 'string',
      templateName: 'string',
      templateContentType: 'string',
      nvPairs: {},
      generatedConfig: 'string',
      autoGenerated: false,
      deleted: true,
      source: 'string',
      priority: 1,
      status: 'string',
      statusOn: 'string',
      createdOn: 'string',
      modifiedOn: 'string',
      secondaryEntityType: 'string',
      secondaryEntityName: 'string',
      hierarchicalKey: 'string'
    };
    describe('#updatePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePolicy(controlPoliciesPolicyId, controlPoliciesUpdatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'updatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControlPoliciesPolicyId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getControlPoliciesPolicyId(controlPoliciesPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'getControlPoliciesPolicyId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPolicyConfig(controlPoliciesPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'getPolicyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markDeletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.markDeletePolicy(controlPoliciesPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'markDeletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlSwitchesSetSwitchesRoleBodyParam = [
      {
        serialNumber: 'string',
        role: 'string'
      }
    ];
    describe('#setSwitchesRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSwitchesRole(controlSwitchesSetSwitchesRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlSwitches', 'setSwitchesRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlSwitchesSerialNumber = 'fakedata';
    describe('#getSwitchesRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchesRole(controlSwitchesSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlSwitches', 'getSwitchesRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricNameForSwitch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFabricNameForSwitch(controlSwitchesSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlSwitches', 'getFabricNameForSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPAddressForSwitch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIPAddressForSwitch(controlSwitchesSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlSwitches', 'getIPAddressForSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customizationSetMOTDBodyParam = {
      motd: 'string'
    };
    describe('#setMOTD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setMOTD(customizationSetMOTDBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customization', 'setMOTD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDefaults - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.restoreDefaults((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customization', 'restoreDefaults', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customizationChangeSplashScreenImageBodyParam = {
      urlEncodedImage: 'string'
    };
    describe('#changeSplashScreenImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeSplashScreenImage(customizationChangeSplashScreenImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customization', 'changeSplashScreenImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMOTD - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMOTD((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.motd);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customization', 'getMOTD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSplashScreenImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSplashScreenImage((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customization', 'getSplashScreenImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMConfigBackupAndRestoreFabricName = 'fakedata';
    const dCNMConfigBackupAndRestoreTag = 'fakedata';
    describe('#triggerBackup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.triggerBackup(dCNMConfigBackupAndRestoreFabricName, dCNMConfigBackupAndRestoreTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMConfigBackupAndRestore', 'triggerBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMEPLDImageManagementExecuteBodyParam = {
      taskList: [
        {
          ipAddress: 'string',
          type: 'DEVICE',
          vrfSelected: 'string',
          epldImageFile: 'string',
          epldModuleNo: 'string',
          golden: false
        }
      ]
    };
    describe('#execute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.execute(dCNMEPLDImageManagementExecuteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMEPLDImageManagement', 'execute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMEPLDImageManagementPlatform = 'fakedata';
    describe('#getUploadedEPLDImagesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUploadedEPLDImagesList(dCNMEPLDImageManagementPlatform, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMEPLDImageManagement', 'getUploadedEPLDImagesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMEPLDImageManagementIpAddress = 'fakedata';
    const dCNMEPLDImageManagementEpldName = 'fakedata';
    describe('#getEPLDModuleList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEPLDModuleList(dCNMEPLDImageManagementIpAddress, dCNMEPLDImageManagementEpldName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMEPLDImageManagement', 'getEPLDModuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMImagePolicyManagementAttachPolicyBodyParam = {
      mappingList: [
        {
          policyName: 'string',
          hostName: 'string',
          ipAddr: 'string',
          platform: 'string',
          serialNumber: 'string'
        }
      ]
    };
    describe('#attachPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachPolicy(dCNMImagePolicyManagementAttachPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'attachPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMImagePolicyManagementPostcreatePolicyBodyParam = {
      policyName: 'string',
      policyType: 'PLATFORM',
      nxosVersion: 'string',
      packageName: 'string',
      platform: 'string',
      policyDescr: 'string'
    };
    describe('#postcreatePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postcreatePolicy(dCNMImagePolicyManagementPostcreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'postcreatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMImagePolicyManagementCreateUmberellaPolicyBodyParam = {
      policyNames: [
        'string'
      ],
      policy: {
        policyName: 'string',
        policyType: 'PLATFORM',
        nxosVersion: 'string',
        packageName: 'string',
        platform: 'string',
        policyDescr: 'string'
      }
    };
    describe('#createUmberellaPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUmberellaPolicy(dCNMImagePolicyManagementCreateUmberellaPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'createUmberellaPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMImagePolicyManagementSerialNumber = 'fakedata';
    describe('#getAttachedPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAttachedPolicies(dCNMImagePolicyManagementSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'getAttachedPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMImagePolicyManagementPlatform = 'fakedata';
    describe('#getuploadedNXOSList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getuploadedNXOSList(dCNMImagePolicyManagementPlatform, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'getuploadedNXOSList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMImagePolicyManagementNxosversion = 'fakedata';
    describe('#getPackageList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPackageList(dCNMImagePolicyManagementPlatform, dCNMImagePolicyManagementNxosversion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'getPackageList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlatformPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPlatformPolicies((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'getPlatformPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getuploadedPlatformList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getuploadedPlatformList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'getuploadedPlatformList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllPolicies((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'getAllPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthScores - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateHealthScores((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'updateHealthScores', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndPorts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEndPorts((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getEndPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetFabrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getgetFabrics((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getgetFabrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostEnclList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostEnclList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getHostEnclList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLandingDashboardTopNAnalytics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLandingDashboardTopNAnalytics('fakedata', 'fakedata', 'fakedata', 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.clientRequestId);
                assert.equal('string', data.response.queryScope);
                assert.equal('string', data.response.queryScopeID);
                assert.equal('string', data.response.queryType);
                assert.equal(true, Array.isArray(data.response.readData));
                assert.equal(true, Array.isArray(data.response.writeData));
                assert.equal(true, Array.isArray(data.response.getxLabels));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getLandingDashboardTopNAnalytics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInterfaceDetails(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getInterfaceDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIsls - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIsls((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getIsls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLicenses((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getModules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getModules((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getModules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeighborLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNeighborLinks((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getNeighborLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeighborLinksBySwitch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNeighborLinksBySwitch((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getNeighborLinksBySwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNpvLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNpvLinks((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getNpvLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitches - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitches((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVsans - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVsans((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getVsans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZones - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZones((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMInventory', 'getZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPackageManagementActivatePackageBodyParam = {
      packageMgrTask: [
        {
          switchSerial: 'string',
          packageName: 'string',
          type: 'DEVICE',
          jobType: 'INSTALL',
          serverName: 'string',
          vrfSelected: 'string'
        }
      ]
    };
    describe('#activatePackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activatePackage(dCNMPackageManagementActivatePackageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPackageManagement', 'activatePackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPackageManagementDeActivatePackageBodyParam = {
      packageMgrTask: [
        {
          switchSerial: 'string',
          packageName: 'string',
          type: 'DEVICE',
          jobType: 'DEACTIVATE',
          serverName: 'string',
          vrfSelected: 'string'
        }
      ]
    };
    describe('#deActivatePackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deActivatePackage(dCNMPackageManagementDeActivatePackageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPackageManagement', 'deActivatePackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPackageManagementInstallBodyParam = {
      packageMgrTask: [
        {
          switchSerial: 'string',
          packageName: 'string',
          type: 'DEVICE',
          jobType: 'DEACTIVATE',
          serverName: 'string',
          vrfSelected: 'string'
        }
      ]
    };
    describe('#install - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.install(dCNMPackageManagementInstallBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPackageManagement', 'install', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPackageManagementUninstallBodyParam = {
      packageMgrTask: [
        {
          switchSerial: 'string',
          packageName: 'string',
          type: 'SERVER',
          jobType: 'ACTIVATE',
          serverName: 'string',
          vrfSelected: 'string'
        }
      ]
    };
    describe('#uninstall - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uninstall(dCNMPackageManagementUninstallBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPackageManagement', 'uninstall', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPackageManagementFabric = 'fakedata';
    describe('#getPackages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPackages(dCNMPackageManagementFabric, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPackageManagement', 'getPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPackageManagementPlatform = 'fakedata';
    describe('#getUploadedPackageList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUploadedPackageList(dCNMPackageManagementPlatform, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPackageManagement', 'getUploadedPackageList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPackageManagementFabricId = 555;
    describe('#getISSUSwitches - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getISSUSwitches(dCNMPackageManagementFabricId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPackageManagement', 'getISSUSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPTPManagementUpdateBandwidthBodyParam = {};
    describe('#updateBandwidth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBandwidth(dCNMPTPManagementUpdateBandwidthBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPTPManagement', 'updateBandwidth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMPTPManagementSwitchName = 'fakedata';
    describe('#getPTPCorrections - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPTPCorrections(dCNMPTPManagementSwitchName, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPTPManagement', 'getPTPCorrections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPParents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPTPParents(dCNMPTPManagementSwitchName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPTPManagement', 'getPTPParents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPPortStatuses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPTPPortStatuses(dCNMPTPManagementSwitchName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPTPManagement', 'getPTPPortStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPThreshold - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPTPThreshold((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMPTPManagement', 'getPTPThreshold', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMSessionManagementLoginBodyParam = {};
    describe('#login - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.login(dCNMSessionManagementLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMSessionManagement', 'login', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logout((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMSessionManagement', 'logout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcnmVersion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDcnmVersion((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMSessionManagement', 'getDcnmVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMTemplatesTemplateName = 'fakedata';
    const dCNMTemplatesCreateTemplateJson2BodyParam = {
      templatename: 'string',
      content: 'string'
    };
    describe('#createTemplateJson2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTemplateJson2(dCNMTemplatesTemplateName, dCNMTemplatesCreateTemplateJson2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'createTemplateJson2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMTemplatesValidateTemplateBodyParam = {};
    describe('#validateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.validateTemplate(dCNMTemplatesValidateTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'validateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMTemplatesGetTemplateDefaultValueBodyParam = {};
    describe('#getTemplateDefaultValue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateDefaultValue(dCNMTemplatesTemplateName, dCNMTemplatesGetTemplateDefaultValueBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'getTemplateDefaultValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMTemplatesGetPopulatedContentBodyParam = {};
    describe('#getPopulatedContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPopulatedContent(dCNMTemplatesTemplateName, null, dCNMTemplatesGetPopulatedContentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'getPopulatedContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllTemplate((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'getAllTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMTemplatesModifyTemplateBodyParam = {
      id: 'string',
      content: 'string'
    };
    describe('#modifyTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyTemplate(dCNMTemplatesTemplateName, dCNMTemplatesModifyTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'modifyTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMTemplatesPopulate = true;
    describe('#getTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplate(dCNMTemplatesTemplateName, dCNMTemplatesPopulate, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'getTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementCreateJobBodyParam = {
      eraseStartUp: true,
      saverunningConfig: true,
      archiveFailureLog: true,
      concurrentExec: true,
      executeImmediately: false,
      maintenanceMode: false,
      skipDisruptiveUpgrade: false,
      scheduledTime: 'string',
      completedTime: 'string',
      jobCreationTime: 'string',
      strScheduledTime: 'string',
      strCompletedTime: 'string',
      strJobCreationTime: 'string',
      comments: 'string',
      jobOwner: 'string',
      statusDescr: 'string',
      devices: 'string',
      prePostTemplate: 'string',
      jobType: 9,
      nvPairs: {},
      status: 'FAILED',
      id: 6,
      taskList: [
        {
          kickStartImage: 'string',
          systemImageFile: 'string',
          ssiImageFile: 'string',
          installLog: 'string',
          compatibilty_log: 'string',
          taskAction: 'string',
          statusDescr: 'string',
          compatibilty_desc: 'string',
          path: 'string',
          strStatus: 'string',
          strCompStatus: 'string',
          vrfSelected: 'string',
          versionCheckResult: 'string',
          actionType: 7,
          sequence: 8,
          jobId: 10,
          bdualSup: true,
          scheduledTime: 'string',
          completedTime: 'string',
          strScheduledTime: 'string',
          strCompletedTime: 'string',
          bMaintenanceMode: false,
          parallelLineCardUpgrade: true,
          noReload: true,
          nonDisruptive: false,
          nonInterruptive: true,
          biosForce: false,
          id: 9,
          ethSwitch_id: 9,
          hostName: 'string',
          ipAddress: 'string',
          platform: 'string',
          version: 'string',
          serverName: 'string',
          maintenanceDesc: 'string',
          type: 'DEVICE',
          status: 'SKIPPED',
          compatibilty_status: 'FAILED',
          preReportStatus: 'COMPLETED_WITH_ERROR',
          postReportStatus: 'NA',
          reportId: 'string',
          preReportSummary: 'string',
          postReportSummary: 'string',
          epldImageFile: 'string',
          epldModuleNo: 'string',
          golden: false,
          forceNonDisruptive: false,
          userSelected: false,
          postReportComplete: false
        }
      ],
      reportComplete: false
    };
    describe('#createJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createJob(imageManagementCreateJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'createJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementCreateServerBodyParam = {
      id: 10,
      serverName: 'string',
      url: 'string',
      userName: 'string',
      password: 'string',
      hostName: 'string',
      protocol: 'string',
      path: 'string',
      lastUpdateTime: 8
    };
    describe('#createServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createServer(imageManagementCreateServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'createServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementSmartImageUploadBodyParam = {
      formData: {},
      formDataMap: {},
      preamble: 'string',
      parts: [
        {
          mediaType: {
            type: 'string',
            subtype: 'string',
            parameters: {},
            wildcardType: false,
            wildcardSubtype: true
          },
          bodyAsString: 'string',
          contentTypeFromMessage: true,
          headers: {}
        }
      ]
    };
    describe('#smartImageUpload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.smartImageUpload(imageManagementSmartImageUploadBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'smartImageUpload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementServerName = 'fakedata';
    const imageManagementFileType = 'fakedata';
    const imageManagementPlatformType = 'fakedata';
    const imageManagementUploadFileBodyParam = {
      formData: {},
      formDataMap: {},
      preamble: 'string',
      parts: [
        {
          mediaType: {
            type: 'string',
            subtype: 'string',
            parameters: {},
            wildcardType: true,
            wildcardSubtype: true
          },
          bodyAsString: 'string',
          contentTypeFromMessage: false,
          headers: {}
        }
      ]
    };
    describe('#uploadFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadFile(imageManagementServerName, imageManagementFileType, imageManagementPlatformType, imageManagementUploadFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'uploadFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementSmartImageDataUploadBodyParam = {
      formData: {},
      formDataMap: {},
      preamble: 'string',
      parts: [
        {
          mediaType: {
            type: 'string',
            subtype: 'string',
            parameters: {},
            wildcardType: false,
            wildcardSubtype: true
          },
          bodyAsString: 'string',
          contentTypeFromMessage: true,
          headers: {}
        }
      ]
    };
    describe('#smartImageDataUpload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.smartImageDataUpload(imageManagementSmartImageDataUploadBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'smartImageDataUpload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementImageName = 'fakedata';
    describe('#smartDeleteImages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.smartDeleteImages(imageManagementImageName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'smartDeleteImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementGetServerStatusBodyParam = {
      id: 5,
      serverName: 'string',
      url: 'string',
      userName: 'string',
      password: 'string',
      hostName: 'string',
      protocol: 'string',
      path: 'string',
      lastUpdateTime: 10
    };
    describe('#getServerStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServerStatus(imageManagementGetServerStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'getServerStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementJobId = 555;
    describe('#getTaskList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTaskList(imageManagementJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'getTaskList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllJobs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllJobs((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'getAllJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementSwitchId = 555;
    describe('#getBootMode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBootMode(imageManagementSwitchId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'getBootMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementSwitchIp = 'fakedata';
    describe('#getInstalTaskForDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInstalTaskForDevice(imageManagementSwitchIp, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'getInstalTaskForDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listServers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listServers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'listServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listFiles(imageManagementServerName, imageManagementFileType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'listFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listImageFiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listImageFiles(imageManagementServerName, imageManagementFileType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'listImageFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartListImages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.smartListImages((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'smartListImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartImagesUploadedList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.smartImagesUploadedList(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'smartImagesUploadedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementUpdateServerBodyParam = {
      id: 6,
      serverName: 'string',
      url: 'string',
      userName: 'string',
      password: 'string',
      hostName: 'string',
      protocol: 'string',
      path: 'string',
      lastUpdateTime: 7
    };
    describe('#updateServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateServer(imageManagementServerName, imageManagementUpdateServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'updateServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServer(imageManagementServerName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'getServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementIp = 'fakedata';
    describe('#fetchDirectorySize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.fetchDirectorySize(null, null, imageManagementIp, null, null, null, imageManagementServerName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'fetchDirectorySize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchDirectoryContents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.fetchDirectoryContents(null, null, imageManagementIp, null, null, null, null, imageManagementServerName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'fetchDirectoryContents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchListOfFiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.searchListOfFiles(null, null, imageManagementIp, null, null, null, imageManagementServerName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'searchListOfFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigServers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getConfigServers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'getConfigServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemDefinedDelimiter - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemDefinedDelimiter((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProgressTracker', 'getSystemDefinedDelimiter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsAllocateResourceIdBodyParam = {
      id: 6,
      scopeType: 'string',
      scopeValue: 9,
      resourceId: 2,
      usageTypeId: 10,
      serialNumber: 'string',
      ifName: 'string',
      allocatedTo: 'string',
      allocatedOn: 'string',
      fabricName: 'string',
      sharedUsage: 'string',
      allocatedFlag: false,
      usageType: 'string'
    };
    describe('#allocateResourceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.allocateResourceId(resourceManagerOperationsAllocateResourceIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'allocateResourceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsFabricName = 'fakedata';
    const resourceManagerOperationsAddResourceBodyParam = {
      poolName: 'string',
      scopeType: 'string',
      scopeValue: 'string',
      entityName: 'string',
      resource: 'string'
    };
    describe('#addResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addResource(resourceManagerOperationsFabricName, resourceManagerOperationsAddResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'addResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsAddMetaDataBodyParam = {
      usageType: 'string',
      dynamicRange: 'string',
      scope: 'string'
    };
    describe('#addMetaData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMetaData(resourceManagerOperationsAddMetaDataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'addMetaData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsAddOrUpdatePoolBodyParam = {
      id: 8,
      poolName: 'string',
      fabricName: 'string',
      vrfName: 'string',
      poolType: 'string',
      dynamicSubnetRange: 'string',
      targetSubnet: 3
    };
    describe('#addOrUpdatePool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addOrUpdatePool(resourceManagerOperationsAddOrUpdatePoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'addOrUpdatePool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsAllocateIpResourceAndReturnBodyParam = {
      id: 9,
      resourcePool: {
        id: 10,
        poolName: 'string',
        fabricName: 'string',
        vrfName: 'string',
        poolType: 'string',
        dynamicSubnetRange: 'string',
        targetSubnet: 5
      },
      entityType: 'string',
      entityName: 'string',
      allocatedIp: 'string',
      allocatedOn: 'string',
      allocatedFlag: false,
      allocatedScopeValue: 'string',
      ipAddress: 'string',
      switchName: 'string'
    };
    describe('#allocateIpResourceAndReturn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.allocateIpResourceAndReturn(resourceManagerOperationsAllocateIpResourceAndReturnBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'allocateIpResourceAndReturn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolDetailsOfFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPoolDetailsOfFabric(resourceManagerOperationsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'getPoolDetailsOfFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsPoolName = 'fakedata';
    describe('#getPoolDataOfFabricAndPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPoolDataOfFabricAndPool(resourceManagerOperationsFabricName, resourceManagerOperationsPoolName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'getPoolDataOfFabricAndPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listResourceUsageforFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listResourceUsageforFabric(resourceManagerOperationsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'listResourceUsageforFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPoolNames - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPoolNames(resourceManagerOperationsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'listPoolNames', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkSubnetOrIpResourceAvailable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkSubnetOrIpResourceAvailable(resourceManagerOperationsFabricName, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'checkSubnetOrIpResourceAvailable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsSerialNumber = 'fakedata';
    describe('#getPoolDetailsOfSwitch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPoolDetailsOfSwitch(resourceManagerOperationsSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'getPoolDetailsOfSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolDataOfSwitchAndPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPoolDataOfSwitchAndPool(resourceManagerOperationsSerialNumber, resourceManagerOperationsPoolName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'getPoolDataOfSwitchAndPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceNetworkOperationsFabricName = 'fakedata';
    describe('#listServiceNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listServiceNetworks(serviceNetworkOperationsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNetworkOperations', 'listServiceNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const telemetryApplyIntentBodyParam = {
      app: 'string',
      cliTag: 'string',
      timeout: 'string',
      configs: [
        {
          fabric: 'string',
          serial: 'string',
          commands: 'string'
        }
      ]
    };
    describe('#applyIntent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applyIntent(telemetryApplyIntentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'applyIntent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const telemetryCreateIntentBodyParam = {
      app: 'string',
      cliTag: 'string',
      timeout: 'string',
      configs: [
        {
          fabric: 'string',
          serial: 'string',
          commands: 'string'
        }
      ]
    };
    describe('#createIntent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createIntent(telemetryCreateIntentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'createIntent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFabricInfo((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'getFabricInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const telemetryAppName = 'fakedata';
    const telemetryCliTag = 'fakedata';
    const telemetryFabricName = 'fakedata';
    const telemetrySwitchList = 'fakedata';
    describe('#getPreviewInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPreviewInfo(telemetryAppName, telemetryCliTag, telemetryFabricName, telemetrySwitchList, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'getPreviewInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const telemetryJobId = 'fakedata';
    const telemetryVerbose = 'fakedata';
    describe('#getJobStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJobStatus(telemetryAppName, telemetryCliTag, telemetryJobId, telemetryVerbose, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'getJobStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const telemetryFabricId = 'fakedata';
    describe('#getSwitchesInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchesInfo(telemetryFabricId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'getSwitchesInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsCreateBulkNetworkBodyParam = [
      {
        fabric: 'string',
        networkName: 'string',
        displayName: 'string',
        networkId: 7,
        networkTemplate: 'string',
        networkExtensionTemplate: 'string',
        networkTemplateConfig: 'string',
        vrf: 'string',
        tenantName: 'string',
        serviceNetworkTemplate: 'string',
        interfaceGroups: 'string'
      }
    ];
    describe('#createBulkNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkNetwork(topDownLANNetworkOperationsCreateBulkNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'createBulkNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsCreateBulkImportNetworkBodyParam = {};
    describe('#createBulkImportNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkImportNetwork(topDownLANNetworkOperationsCreateBulkImportNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'createBulkImportNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsFabricName = 'fakedata';
    const topDownLANNetworkOperationsCreateNetworkBodyParam = {
      fabric: 'string',
      networkName: 'string',
      displayName: 'string',
      networkId: 3,
      networkTemplate: 'string',
      networkExtensionTemplate: 'string',
      networkTemplateConfig: 'string',
      vrf: 'string',
      tenantName: 'string',
      serviceNetworkTemplate: 'string',
      interfaceGroups: 'string'
    };
    describe('#createNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsCreateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'createNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsAttachNetworksBodyParam = [
      {
        networkName: 'string',
        lanAttachList: [
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 6,
            dot1QVlan: 4,
            untagged: false,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 4,
            dot1QVlan: 7,
            untagged: false,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 10,
            dot1QVlan: 6,
            untagged: true,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 8,
            dot1QVlan: 5,
            untagged: true,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 6,
            dot1QVlan: 6,
            untagged: false,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 2,
            dot1QVlan: 5,
            untagged: false,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 3,
            dot1QVlan: 3,
            untagged: true,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 2,
            dot1QVlan: 9,
            untagged: true,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 1,
            dot1QVlan: 8,
            untagged: true,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          }
        ]
      }
    ];
    describe('#attachNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachNetworks(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsAttachNetworksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'attachNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsPostdeployNetworksBodyParam = {
      networkNames: 'string'
    };
    describe('#postdeployNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postdeployNetworks(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsPostdeployNetworksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'postdeployNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsNetworkName = 'fakedata';
    const topDownLANNetworkOperationsAttachNetworkBodyParam = {
      fabric: 'string',
      networkName: 'string',
      serialNumber: 'string',
      switchPorts: 'string',
      detachSwitchPorts: 'string',
      vlan: 6,
      dot1QVlan: 1,
      untagged: false,
      deployment: true,
      extensionValues: 'string',
      instanceValues: 'string',
      freeformConfig: 'string'
    };
    describe('#attachNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, topDownLANNetworkOperationsAttachNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'attachNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'deployNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listNetworks(topDownLANNetworkOperationsFabricName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'listNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsNetworkNames = 'fakedata';
    describe('#getNetworkAttachDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkAttachDetails(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'getNetworkAttachDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.previewNetworks(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'previewNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsSerialNumbers = 'fakedata';
    describe('#getAllPortsAndStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllPortsAndStatus(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkNames, topDownLANNetworkOperationsSerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'getAllPortsAndStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchDeploymentHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchDeploymentHistory(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkNames, topDownLANNetworkOperationsSerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'getSwitchDeploymentHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsUpdateNetworkBodyParam = {
      fabric: 'string',
      networkName: 'string',
      displayName: 'string',
      networkId: 10,
      networkTemplate: 'string',
      networkExtensionTemplate: 'string',
      networkTemplateConfig: 'string',
      vrf: 'string',
      tenantName: 'string',
      serviceNetworkTemplate: 'string',
      interfaceGroups: 'string'
    };
    describe('#updateNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, topDownLANNetworkOperationsUpdateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'updateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'getNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleNetworkAttachDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSingleNetworkAttachDetails(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'getSingleNetworkAttachDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.previewNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'previewNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsSerialNumber = 'fakedata';
    describe('#networkStatusDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkStatusDetails(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, topDownLANNetworkOperationsSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'networkStatusDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleSwitchDeploymentHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSingleSwitchDeploymentHistory(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, topDownLANNetworkOperationsSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'getSingleSwitchDeploymentHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPortsAndStatusForSingle - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllPortsAndStatusForSingle(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, topDownLANNetworkOperationsSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'getAllPortsAndStatusForSingle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2PostcreateBulkNetworkBodyParam = [
      {
        fabric: 'string',
        networkName: 'string',
        displayName: 'string',
        networkId: 3,
        networkTemplate: 'string',
        networkExtensionTemplate: 'string',
        vrf: 'string',
        tenantName: 'string',
        serviceNetworkTemplate: 'string',
        interfaceGroups: 'string',
        networkTemplateConfig: {}
      }
    ];
    describe('#postcreateBulkNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postcreateBulkNetwork(topDownLANNetworkOperationsV2PostcreateBulkNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'postcreateBulkNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2PostcreateBulkImportNetworkBodyParam = {};
    describe('#postcreateBulkImportNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postcreateBulkImportNetwork(topDownLANNetworkOperationsV2PostcreateBulkImportNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'postcreateBulkImportNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2FabricName = 'fakedata';
    const topDownLANNetworkOperationsV2PostcreateNetworkBodyParam = {
      fabric: 'string',
      networkName: 'string',
      displayName: 'string',
      networkId: 5,
      networkTemplate: 'string',
      networkExtensionTemplate: 'string',
      vrf: 'string',
      tenantName: 'string',
      serviceNetworkTemplate: 'string',
      interfaceGroups: 'string',
      networkTemplateConfig: {}
    };
    describe('#postcreateNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postcreateNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2PostcreateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'postcreateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2PostattachNetworksBodyParam = [
      {
        networkName: 'string',
        lanAttachList: [
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 8,
            dot1QVlan: 4,
            untagged: false,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            networkName: 'string',
            serialNumber: 'string',
            switchPorts: 'string',
            detachSwitchPorts: 'string',
            vlan: 9,
            dot1QVlan: 10,
            untagged: false,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          }
        ]
      }
    ];
    describe('#postattachNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postattachNetworks(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2PostattachNetworksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'postattachNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2PostdeployNetworks1BodyParam = {
      networkNames: 'string'
    };
    describe('#postdeployNetworks1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postdeployNetworks1(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2PostdeployNetworks1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'postdeployNetworks1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2NetworkName = 'fakedata';
    const topDownLANNetworkOperationsV2PostattachNetworkBodyParam = {
      fabric: 'string',
      networkName: 'string',
      serialNumber: 'string',
      switchPorts: 'string',
      detachSwitchPorts: 'string',
      vlan: 3,
      dot1QVlan: 4,
      untagged: false,
      deployment: false,
      extensionValues: 'string',
      instanceValues: 'string',
      freeformConfig: 'string'
    };
    describe('#postattachNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postattachNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, topDownLANNetworkOperationsV2PostattachNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'postattachNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postdeployNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postdeployNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'postdeployNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getlistNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getlistNetworks(topDownLANNetworkOperationsV2FabricName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getlistNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2NetworkNames = 'fakedata';
    describe('#getgetNetworkAttachDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetNetworkAttachDetails(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getgetNetworkAttachDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpreviewNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getpreviewNetworks(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getpreviewNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2SerialNumbers = 'fakedata';
    describe('#getgetAllPortsAndStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetAllPortsAndStatus(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkNames, topDownLANNetworkOperationsV2SerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getgetAllPortsAndStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSwitchDeploymentHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetSwitchDeploymentHistory(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkNames, topDownLANNetworkOperationsV2SerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getgetSwitchDeploymentHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2PutupdateNetworkBodyParam = {
      fabric: 'string',
      networkName: 'string',
      displayName: 'string',
      networkId: 9,
      networkTemplate: 'string',
      networkExtensionTemplate: 'string',
      vrf: 'string',
      tenantName: 'string',
      serviceNetworkTemplate: 'string',
      interfaceGroups: 'string',
      networkTemplateConfig: {}
    };
    describe('#putupdateNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putupdateNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, topDownLANNetworkOperationsV2PutupdateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'putupdateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getgetNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSingleNetworkAttachDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetSingleNetworkAttachDetails(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getgetSingleNetworkAttachDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpreviewNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getpreviewNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getpreviewNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANNetworkOperationsV2SerialNumber = 'fakedata';
    describe('#getnetworkStatusDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getnetworkStatusDetails(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, topDownLANNetworkOperationsV2SerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getnetworkStatusDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSingleSwitchDeploymentHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetSingleSwitchDeploymentHistory(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, topDownLANNetworkOperationsV2SerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getgetSingleSwitchDeploymentHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetAllPortsAndStatusForSingle - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetAllPortsAndStatusForSingle(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, topDownLANNetworkOperationsV2SerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'getgetAllPortsAndStatusForSingle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsCreateBulkVRFBodyParam = [
      {
        fabric: 'string',
        vrfName: 'string',
        vrfTemplate: 'string',
        vrfExtensionTemplate: 'string',
        vrfTemplateConfig: 'string',
        tenantName: 'string',
        vrfId: 9,
        serviceVrfTemplate: 'string'
      }
    ];
    describe('#createBulkVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkVRF(topDownLANVRFOperationsCreateBulkVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'createBulkVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsCreateBulkImportVRFBodyParam = {};
    describe('#createBulkImportVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkImportVRF(topDownLANVRFOperationsCreateBulkImportVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'createBulkImportVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsGetCdpNeighborBodyParam = {
      sourceFabricName: 'string',
      sourceSwitchName: 'string',
      sourcePortName: 'string',
      destFabricName: 'string',
      destSwitchName: 'string',
      destPortName: 'string'
    };
    describe('#getCdpNeighbor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCdpNeighbor(topDownLANVRFOperationsGetCdpNeighborBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'getCdpNeighbor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsFabricName = 'fakedata';
    const topDownLANVRFOperationsDeployExtensionPrototypeBodyParam = {
      sourceFabric: 'string',
      sourceSwitch: 'string',
      sourceSwitchSN: 'string',
      sourcePort: 'string',
      destFabric: 'string',
      destSwitch: 'string',
      destSwitchSN: 'string',
      destPort: 'string',
      extensionType: 'string',
      templateName: 'string',
      templateValues: 'string',
      extensionLayer: 'string',
      extensionName: 'string',
      state: 'string',
      baseTemplateName: 'string',
      baseTemplateValues: 'string',
      deploymentConfig: 'string',
      errorMessage: 'string',
      currentState: 'string'
    };
    describe('#deployExtensionPrototype - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployExtensionPrototype(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsDeployExtensionPrototypeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'deployExtensionPrototype', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsGenerateExtensionPreviewBodyParam = {
      sourceFabric: 'string',
      sourceSwitch: 'string',
      sourceSwitchSN: 'string',
      sourcePort: 'string',
      destFabric: 'string',
      destSwitch: 'string',
      destSwitchSN: 'string',
      destPort: 'string',
      extensionType: 'string',
      templateName: 'string',
      templateValues: 'string',
      extensionLayer: 'string',
      extensionName: 'string',
      state: 'string',
      baseTemplateName: 'string',
      baseTemplateValues: 'string',
      deploymentConfig: 'string',
      errorMessage: 'string',
      currentState: 'string'
    };
    describe('#generateExtensionPreview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateExtensionPreview(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsGenerateExtensionPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'generateExtensionPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsCreateVRFBodyParam = {
      fabric: 'string',
      vrfName: 'string',
      vrfTemplate: 'string',
      vrfExtensionTemplate: 'string',
      vrfTemplateConfig: 'string',
      tenantName: 'string',
      vrfId: 3,
      serviceVrfTemplate: 'string'
    };
    describe('#createVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createVRF(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsCreateVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'createVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsAttachVrfsBodyParam = [
      {
        vrfName: 'string',
        lanAttachList: [
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 3,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 2,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 9,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          }
        ]
      }
    ];
    describe('#attachVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachVrfs(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsAttachVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'attachVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsPostdeployVrfsBodyParam = {
      vrfNames: 'string'
    };
    describe('#postdeployVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postdeployVrfs(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsPostdeployVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'postdeployVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listVRF(topDownLANVRFOperationsFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'listVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsVrfNames = 'fakedata';
    describe('#getVRFAttachDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVRFAttachDetails(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'getVRFAttachDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.previewVrfs(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'previewVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsSerialNumbers = 'fakedata';
    describe('#getSwitchDetailsByVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSwitchDetailsByVRF(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfNames, topDownLANVRFOperationsSerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'getSwitchDetailsByVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSwitchDeploymentHistory1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetSwitchDeploymentHistory1(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfNames, topDownLANVRFOperationsSerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'getgetSwitchDeploymentHistory1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsVrfName = 'fakedata';
    const topDownLANVRFOperationsUpdateVRFBodyParam = {
      fabric: 'string',
      vrfName: 'string',
      vrfTemplate: 'string',
      vrfExtensionTemplate: 'string',
      vrfTemplateConfig: 'string',
      tenantName: 'string',
      vrfId: 6,
      serviceVrfTemplate: 'string'
    };
    describe('#updateVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateVRF(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfName, topDownLANVRFOperationsUpdateVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'updateVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVRF(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'getVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2PostcreateBulkVRFBodyParam = [
      {
        fabric: 'string',
        vrfName: 'string',
        vrfTemplate: 'string',
        vrfExtensionTemplate: 'string',
        tenantName: 'string',
        vrfId: 6,
        serviceVrfTemplate: 'string',
        vrfTemplateConfig: {}
      }
    ];
    describe('#postcreateBulkVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postcreateBulkVRF(topDownLANVRFOperationsV2PostcreateBulkVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'postcreateBulkVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2PostcreateBulkImportVRFBodyParam = {};
    describe('#postcreateBulkImportVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postcreateBulkImportVRF(topDownLANVRFOperationsV2PostcreateBulkImportVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'postcreateBulkImportVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2PostgetCdpNeighborBodyParam = {
      sourceFabricName: 'string',
      sourceSwitchName: 'string',
      sourcePortName: 'string',
      destFabricName: 'string',
      destSwitchName: 'string',
      destPortName: 'string'
    };
    describe('#postgetCdpNeighbor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postgetCdpNeighbor(topDownLANVRFOperationsV2PostgetCdpNeighborBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'postgetCdpNeighbor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2FabricName = 'fakedata';
    const topDownLANVRFOperationsV2PostcreateVRFBodyParam = {
      fabric: 'string',
      vrfName: 'string',
      vrfTemplate: 'string',
      vrfExtensionTemplate: 'string',
      tenantName: 'string',
      vrfId: 4,
      serviceVrfTemplate: 'string',
      vrfTemplateConfig: {}
    };
    describe('#postcreateVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postcreateVRF(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2PostcreateVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'postcreateVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2PostattachVrfsBodyParam = [
      {
        vrfName: 'string',
        lanAttachList: [
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 4,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 4,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 10,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 10,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 10,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 3,
            deployment: false,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          },
          {
            fabric: 'string',
            vrfName: 'string',
            serialNumber: 'string',
            vlan: 3,
            deployment: true,
            extensionValues: 'string',
            instanceValues: 'string',
            freeformConfig: 'string'
          }
        ]
      }
    ];
    describe('#postattachVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postattachVrfs(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2PostattachVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'postattachVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2PostdeployVrfs1BodyParam = {
      vrfNames: 'string'
    };
    describe('#postdeployVrfs1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postdeployVrfs1(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2PostdeployVrfs1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'postdeployVrfs1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getlistVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getlistVRF(topDownLANVRFOperationsV2FabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'getlistVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2VrfNames = 'fakedata';
    describe('#getgetVRFAttachDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetVRFAttachDetails(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'getgetVRFAttachDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpreviewVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getpreviewVrfs(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfNames, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'getpreviewVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2SerialNumbers = 'fakedata';
    describe('#getgetSwitchDetailsByVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetSwitchDetailsByVRF(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfNames, topDownLANVRFOperationsV2SerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'getgetSwitchDetailsByVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSwitchDeploymentHistory12 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetSwitchDeploymentHistory12(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfNames, topDownLANVRFOperationsV2SerialNumbers, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'getgetSwitchDeploymentHistory12', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topDownLANVRFOperationsV2VrfName = 'fakedata';
    const topDownLANVRFOperationsV2PutupdateVRFBodyParam = {
      fabric: 'string',
      vrfName: 'string',
      vrfTemplate: 'string',
      vrfExtensionTemplate: 'string',
      tenantName: 'string',
      vrfId: 10,
      serviceVrfTemplate: 'string',
      vrfTemplateConfig: {}
    };
    describe('#putupdateVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putupdateVRF(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfName, topDownLANVRFOperationsV2PutupdateVRFBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'putupdateVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgetVRF(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'getgetVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsDeletePoliciesBodyParam = [
      'string'
    ];
    describe('#deletePolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicies(alarmsDeletePoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'deletePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsDeleteIfGroupNwAssocBodyParam = [
      'string'
    ];
    describe('#deleteIfGroupNwAssoc - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIfGroupNwAssoc(controlFabricsFabricName, controlFabricsGroupName, controlFabricsDeleteIfGroupNwAssocBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'deleteIfGroupNwAssoc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlFabricsDeleteIfGroupIfAssocBodyParam = {
      switchandPortList: [
        {
          switchSN: 'string',
          switchPort: 'string'
        }
      ]
    };
    describe('#deleteIfGroupIfAssoc - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIfGroupIfAssoc(controlFabricsFabricName, controlFabricsDeleteIfGroupIfAssocBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlFabrics', 'deleteIfGroupIfAssoc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlInterfaceServiceDeleteInterfaceBodyParam = [
      {
        serialNumber: 'string',
        ifName: 'string'
      }
    ];
    describe('#deleteInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInterface(controlInterfaceServiceDeleteInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlInterfaceService', 'deleteInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLink(controlLinksLinkUUID, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlLinks', 'deleteLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFabricFromSiteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFabricFromSiteGroup(controlMultisiteFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlMultisite', 'deleteFabricFromSiteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controlPoliciesPolicyIds = 'fakedata';
    describe('#deletePoliciesInBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePoliciesInBulk(controlPoliciesPolicyIds, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'deletePoliciesInBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePoliciesInSwitchesAndSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePoliciesInSwitchesAndSource(controlPoliciesSerialNumbers, controlPoliciesSource, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'deletePoliciesInSwitchesAndSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSwitchPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSwitchPolicies(controlPoliciesSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'deleteSwitchPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSwitchPolicies4Entity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSwitchPolicies4Entity(controlPoliciesSerialNumber, controlPoliciesEntityType, controlPoliciesEntityName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'deleteSwitchPolicies4Entity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicy(controlPoliciesPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControlPolicies', 'deletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeSplashScreenImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeSplashScreenImage((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customization', 'removeSplashScreenImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllBackups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllBackups(dCNMConfigBackupAndRestoreFabricName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMConfigBackupAndRestore', 'deleteAllBackups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMConfigBackupAndRestoreBackupVersion = 'fakedata';
    describe('#deleteSingleBackup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSingleBackup(dCNMConfigBackupAndRestoreFabricName, dCNMConfigBackupAndRestoreBackupVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMConfigBackupAndRestore', 'deleteSingleBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicyMapping(dCNMImagePolicyManagementSerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'deletePolicyMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMImagePolicyManagementDeletedeletePolicyBodyParam = {
      policyNames: [
        'string'
      ]
    };
    describe('#deletedeletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletedeletePolicy(dCNMImagePolicyManagementDeletedeletePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMImagePolicyManagement', 'deletedeletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCNMTemplatesDeleteTemplateBulkBodyParam = {};
    describe('#deleteTemplateBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateBulk(dCNMTemplatesDeleteTemplateBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'deleteTemplateBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplate(dCNMTemplatesTemplateName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCNMTemplates', 'deleteTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJob(imageManagementJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'deleteJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagementSmartImageDeleteBodyParam = {
      deleteTasksList: [
        {
          platform: 'string',
          version: 'string',
          imageType: 'string',
          imagename: 'string'
        }
      ]
    };
    describe('#smartImageDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.smartImageDelete(imageManagementSmartImageDeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'smartImageDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServer(imageManagementServerName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'deleteServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFile(null, null, imageManagementIp, null, imageManagementServerName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManagement', 'deleteFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsReleaseResourceIdBodyParam = {
      id: 1,
      scopeType: 'string',
      scopeValue: 9,
      resourceId: 5,
      usageTypeId: 3,
      serialNumber: 'string',
      ifName: 'string',
      allocatedTo: 'string',
      allocatedOn: 'string',
      fabricName: 'string',
      sharedUsage: 'string',
      allocatedFlag: true,
      usageType: 'string'
    };
    describe('#releaseResourceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.releaseResourceId(resourceManagerOperationsReleaseResourceIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'releaseResourceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsId = 'fakedata';
    describe('#releaseResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.releaseResource(resourceManagerOperationsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'releaseResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceManagerOperationsRmid = 'fakedata';
    describe('#releaseResourceIp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.releaseResourceIp(resourceManagerOperationsRmid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceManagerOperations', 'releaseResourceIp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const telemetryDeleteIntentBodyParam = {
      app: 'string',
      cliTag: 'string',
      timeout: 'string',
      configs: [
        {
          fabric: 'string',
          serial: 'string',
          commands: 'string'
        }
      ]
    };
    describe('#deleteIntent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIntent(telemetryDeleteIntentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'deleteIntent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'deleteNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServiceNetwork(topDownLANNetworkOperationsFabricName, topDownLANNetworkOperationsNetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperations', 'deleteServiceNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletedeleteNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'deletedeleteNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteServiceNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletedeleteServiceNetwork(topDownLANNetworkOperationsV2FabricName, topDownLANNetworkOperationsV2NetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANNetworkOperationsV2', 'deletedeleteServiceNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServiceVRF(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'deleteServiceVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVRF(topDownLANVRFOperationsFabricName, topDownLANVRFOperationsVrfName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperations', 'deleteVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteServiceVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletedeleteServiceVRF(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'deletedeleteServiceVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteVRF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletedeleteVRF(topDownLANVRFOperationsV2FabricName, topDownLANVRFOperationsV2VrfName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopDownLANVRFOperationsV2', 'deletedeleteVRF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditLogsUsingGET(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditLog', 'getAuditLogsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuditLogsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuditLogsUsingDELETE(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditLog', 'deleteAuditLogsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePeeringsUsingGET1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoutePeeringsUsingGET1(null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'getRoutePeeringsUsingGET1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routePeeringAddRoutePeeringUsingPOSTBodyParam = {
      attachedFabricName: 'attachedEasyFabric',
      deploymentMode: 'IntraTenantFW',
      fabricName: 'extFabric',
      nextHopIp: '1.2.3.4',
      peeringName: 'peering1',
      peeringOption: 'None',
      reverseNextHopIp: '2.3.4.5',
      serviceNetworks: [
        {}
      ],
      serviceNodeName: 'sn1',
      serviceNodeType: 'VNF'
    };
    describe('#addRoutePeeringUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRoutePeeringUsingPOST('fakedata', 'fakedata', routePeeringAddRoutePeeringUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('attachedEasyFabric', data.response.attachedFabricName);
                assert.equal('IntraTenantFW', data.response.deploymentMode);
                assert.equal(true, data.response.enabled);
                assert.equal('extFabric', data.response.fabricName);
                assert.equal(5, data.response.lastUpdated);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal('peering1', data.response.peeringName);
                assert.equal('EBGPDynamicPeering', data.response.peeringOption);
                assert.equal('2.3.4.5', data.response.reverseNextHopIp);
                assert.equal(true, Array.isArray(data.response.routes));
                assert.equal(true, Array.isArray(data.response.serviceNetworks));
                assert.equal('sn1', data.response.serviceNodeName);
                assert.equal('VNF', data.response.serviceNodeType);
                assert.equal('In-Sync', data.response.status);
                assert.equal(true, Array.isArray(data.response.statusDetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'addRoutePeeringUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePeeringUsingDELETE2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE2('fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'deleteRoutePeeringUsingDELETE2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePeeringsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoutePeeringsUsingGET('fakedata', null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'getRoutePeeringsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePeeringUsingDELETE1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE1('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'deleteRoutePeeringUsingDELETE1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routePeeringAttachPeeringsUsingPOSTBodyParam = {
      peeringNames: [
        'RP-1',
        'RP-2'
      ]
    };
    describe('#attachPeeringsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachPeeringsUsingPOST('fakedata', 'fakedata', 'fakedata', routePeeringAttachPeeringsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'attachPeeringsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPeeringsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.detachPeeringsUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'detachPeeringsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routePeeringDeployPeeringsUsingPOSTBodyParam = {
      peeringNames: [
        'RP-1',
        'RP-2'
      ]
    };
    describe('#deployPeeringsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployPeeringsUsingPOST('fakedata', 'fakedata', 'fakedata', routePeeringDeployPeeringsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'deployPeeringsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePeeringUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoutePeeringUsingGET('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('attachedEasyFabric', data.response.attachedFabricName);
                assert.equal('OneArmVNF', data.response.deploymentMode);
                assert.equal(true, data.response.enabled);
                assert.equal('extFabric', data.response.fabricName);
                assert.equal(10, data.response.lastUpdated);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal('peering1', data.response.peeringName);
                assert.equal('EBGPDynamicPeering', data.response.peeringOption);
                assert.equal('2.3.4.5', data.response.reverseNextHopIp);
                assert.equal(true, Array.isArray(data.response.routes));
                assert.equal(true, Array.isArray(data.response.serviceNetworks));
                assert.equal('sn1', data.response.serviceNodeName);
                assert.equal('Firewall', data.response.serviceNodeType);
                assert.equal('In-Sync', data.response.status);
                assert.equal(true, Array.isArray(data.response.statusDetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'getRoutePeeringUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routePeeringUpdateRoutePeeringUsingPUTBodyParam = {
      attachedFabricName: 'attachedEasyFabric',
      deploymentMode: 'OneArmADC',
      fabricName: 'extFabric',
      nextHopIp: '1.2.3.4',
      peeringName: 'peering1',
      peeringOption: 'None',
      reverseNextHopIp: '2.3.4.5',
      serviceNetworks: [
        {}
      ],
      serviceNodeName: 'sn1',
      serviceNodeType: 'ADC'
    };
    describe('#updateRoutePeeringUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRoutePeeringUsingPUT('fakedata', 'fakedata', 'fakedata', 'fakedata', routePeeringUpdateRoutePeeringUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'updateRoutePeeringUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePeeringUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'deleteRoutePeeringUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPeeringAttachmentsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPeeringAttachmentsUsingGET('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'getPeeringAttachmentsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachPeeringUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachPeeringUsingPOST('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'attachPeeringUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPeeringUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.detachPeeringUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'detachPeeringUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployPeeringUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployPeeringUsingPOST('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePeering', 'deployPeeringUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachedServiceNodesUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAttachedServiceNodesUsingGET('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNode', 'getAttachedServiceNodesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceNodesUsingGET1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServiceNodesUsingGET1('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNode', 'getServiceNodesUsingGET1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceNodeAddServiceNodeUsingPOSTBodyParam = {
      attachedFabricName: 'attachedEasyFabric',
      attachedSwitchInterfaceName: 'Ethernet1/52',
      attachedSwitchSn: 'attachedSwitchSerialNumber',
      fabricName: 'extFabric',
      formFactor: 'Physical',
      interfaceName: 'G1/1',
      linkTemplateName: 'service_link_trunk',
      name: 'sn1',
      nvPairs: {
        SPEED: 'Auto',
        MTU: 'jumbo',
        ALLOWED_VLANS: 'none',
        BPDUGUARD_ENABLED: 'no',
        PORTTYPE_FAST_ENABLED: 'true',
        ADMIN_STATE: 'true'
      },
      type: 'ADC'
    };
    describe('#addServiceNodeUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addServiceNodeUsingPOST('fakedata', serviceNodeAddServiceNodeUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('attachedEasyFabric', data.response.attachedFabricName);
                assert.equal('Ethernet1/52', data.response.attachedSwitchInterfaceName);
                assert.equal('attachedSwitchSerialNumber', data.response.attachedSwitchSn);
                assert.equal('extFabric', data.response.fabricName);
                assert.equal('Virtual', data.response.formFactor);
                assert.equal('G1/1', data.response.interfaceName);
                assert.equal(9, data.response.lastUpdated);
                assert.equal('service_link_trunk', data.response.linkTemplateName);
                assert.equal('string', data.response.linkUuid);
                assert.equal('sn1', data.response.name);
                assert.equal('object', typeof data.response.nvPairs);
                assert.equal('string', data.response.peerName);
                assert.equal('Firewall', data.response.type);
                assert.equal(true, data.response.vpcSwitchesAttached);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNode', 'addServiceNodeUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceNodeUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServiceNodeUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('attachedEasyFabric', data.response.attachedFabricName);
                assert.equal('Ethernet1/52', data.response.attachedSwitchInterfaceName);
                assert.equal('attachedSwitchSerialNumber', data.response.attachedSwitchSn);
                assert.equal('extFabric', data.response.fabricName);
                assert.equal('Physical', data.response.formFactor);
                assert.equal('G1/1', data.response.interfaceName);
                assert.equal(8, data.response.lastUpdated);
                assert.equal('service_link_trunk', data.response.linkTemplateName);
                assert.equal('string', data.response.linkUuid);
                assert.equal('sn1', data.response.name);
                assert.equal('object', typeof data.response.nvPairs);
                assert.equal('string', data.response.peerName);
                assert.equal('VNF', data.response.type);
                assert.equal(false, data.response.vpcSwitchesAttached);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNode', 'getServiceNodeUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceNodeUpdateServiceNodeUsingPUT1BodyParam = {
      attachedFabricName: 'attachedEasyFabric',
      attachedSwitchInterfaceName: 'Ethernet1/52',
      attachedSwitchSn: 'attachedSwitchSerialNumber',
      fabricName: 'extFabric',
      formFactor: 'Physical',
      interfaceName: 'G1/1',
      linkTemplateName: 'service_link_trunk',
      name: 'sn1',
      nvPairs: {
        SPEED: 'Auto',
        MTU: 'jumbo',
        ALLOWED_VLANS: 'none',
        BPDUGUARD_ENABLED: 'no',
        PORTTYPE_FAST_ENABLED: 'true',
        ADMIN_STATE: 'true'
      },
      type: 'VNF'
    };
    describe('#updateServiceNodeUsingPUT1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateServiceNodeUsingPUT1('fakedata', 'fakedata', serviceNodeUpdateServiceNodeUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNode', 'updateServiceNodeUsingPUT1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceNodeUsingDELETE1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServiceNodeUsingDELETE1('fakedata', null, 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNode', 'deleteServiceNodeUsingDELETE1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePoliciesUsingGET1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicePoliciesUsingGET1(null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'getServicePoliciesUsingGET1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicePolicyAddServicePolicyUsingPOSTBodyParam = {
      attachedFabricName: 'attachedEasyFabric',
      destinationNetwork: '12.1.1.2/32',
      destinationNetworkName: 'destNet1',
      destinationVrfName: 'vrf1',
      fabricName: 'extFabric',
      nextHopIp: '1.2.3.4',
      nvPairs: {
        PROTOCOL: 'tcp',
        SRC_PORT: 'any',
        DEST_PORT: '443',
        ROUTE_MAP_ACTION: 'permit',
        NEXT_HOP_OPTION: 'none'
      },
      peeringName: 'peering1',
      policyName: 'policy1',
      policyTemplateName: 'service_pbr',
      reverseEnabled: true,
      reverseNextHopIp: '2.3.4.5',
      serviceNodeName: 'sn1',
      serviceNodeType: 'Firewall',
      sourceNetwork: '11.1.1.1/24',
      sourceNetworkName: 'srcNet1',
      sourceVrfName: 'vrf1'
    };
    describe('#addServicePolicyUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addServicePolicyUsingPOST('fakedata', 'fakedata', servicePolicyAddServicePolicyUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attachDetails));
                assert.equal('attachedEasyFabric', data.response.attachedFabricName);
                assert.equal('object', typeof data.response.destinationInterfaces);
                assert.equal('12.1.1.2/32', data.response.destinationNetwork);
                assert.equal('destNet1', data.response.destinationNetworkName);
                assert.equal('vrf1', data.response.destinationVrfName);
                assert.equal(true, data.response.enabled);
                assert.equal('extFabric', data.response.fabricName);
                assert.equal(9, data.response.lastUpdated);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal('object', typeof data.response.nvPairs);
                assert.equal('peering1', data.response.peeringName);
                assert.equal('policy1', data.response.policyName);
                assert.equal('service_pbr', data.response.policyTemplateName);
                assert.equal(true, data.response.reverseEnabled);
                assert.equal('2.3.4.5', data.response.reverseNextHopIp);
                assert.equal('string', data.response.routeMapName);
                assert.equal('sn1', data.response.serviceNodeName);
                assert.equal('Firewall', data.response.serviceNodeType);
                assert.equal('object', typeof data.response.sourceInterfaces);
                assert.equal('11.1.1.1/24', data.response.sourceNetwork);
                assert.equal('srcNet1', data.response.sourceNetworkName);
                assert.equal('vrf1', data.response.sourceVrfName);
                assert.equal('In-Sync', data.response.status);
                assert.equal(true, Array.isArray(data.response.statusDetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'addServicePolicyUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicePolicyUsingDELETE2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE2('fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'deleteServicePolicyUsingDELETE2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePoliciesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicePoliciesUsingGET('fakedata', null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'getServicePoliciesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicePolicyUsingDELETE1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE1('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'deleteServicePolicyUsingDELETE1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicePolicyAttachPoliciesUsingPOSTBodyParam = {
      policyNames: [
        'SP-1',
        'SP-2'
      ]
    };
    describe('#attachPoliciesUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachPoliciesUsingPOST('fakedata', 'fakedata', 'fakedata', servicePolicyAttachPoliciesUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'attachPoliciesUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPoliciesUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.detachPoliciesUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'detachPoliciesUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicePolicyDeployPoliciesUsingPOSTBodyParam = {
      policyNames: [
        'SP-1',
        'SP-2'
      ]
    };
    describe('#deployPoliciesUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployPoliciesUsingPOST('fakedata', 'fakedata', 'fakedata', servicePolicyDeployPoliciesUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'deployPoliciesUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePolicyUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicePolicyUsingGET('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attachDetails));
                assert.equal('attachedEasyFabric', data.response.attachedFabricName);
                assert.equal('object', typeof data.response.destinationInterfaces);
                assert.equal('12.1.1.2/32', data.response.destinationNetwork);
                assert.equal('destNet1', data.response.destinationNetworkName);
                assert.equal('vrf1', data.response.destinationVrfName);
                assert.equal(true, data.response.enabled);
                assert.equal('extFabric', data.response.fabricName);
                assert.equal(2, data.response.lastUpdated);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal('object', typeof data.response.nvPairs);
                assert.equal('peering1', data.response.peeringName);
                assert.equal('policy1', data.response.policyName);
                assert.equal('service_pbr', data.response.policyTemplateName);
                assert.equal(true, data.response.reverseEnabled);
                assert.equal('2.3.4.5', data.response.reverseNextHopIp);
                assert.equal('string', data.response.routeMapName);
                assert.equal('sn1', data.response.serviceNodeName);
                assert.equal('ADC', data.response.serviceNodeType);
                assert.equal('object', typeof data.response.sourceInterfaces);
                assert.equal('11.1.1.1/24', data.response.sourceNetwork);
                assert.equal('srcNet1', data.response.sourceNetworkName);
                assert.equal('vrf1', data.response.sourceVrfName);
                assert.equal('In-Sync', data.response.status);
                assert.equal(true, Array.isArray(data.response.statusDetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'getServicePolicyUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicePolicyUpdateServicePolicyUsingPUTBodyParam = {
      attachedFabricName: 'attachedEasyFabric',
      destinationNetwork: '12.1.1.2/32',
      destinationNetworkName: 'destNet1',
      destinationVrfName: 'vrf1',
      fabricName: 'extFabric',
      nextHopIp: '1.2.3.4',
      nvPairs: {
        PROTOCOL: 'tcp',
        SRC_PORT: 'any',
        DEST_PORT: '443',
        ROUTE_MAP_ACTION: 'permit',
        NEXT_HOP_OPTION: 'none'
      },
      peeringName: 'peering1',
      policyName: 'policy1',
      policyTemplateName: 'service_pbr',
      reverseEnabled: true,
      reverseNextHopIp: '2.3.4.5',
      serviceNodeName: 'sn1',
      serviceNodeType: 'VNF',
      sourceNetwork: '11.1.1.1/24',
      sourceNetworkName: 'srcNet1',
      sourceVrfName: 'vrf1'
    };
    describe('#updateServicePolicyUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateServicePolicyUsingPUT('fakedata', 'fakedata', 'fakedata', 'fakedata', servicePolicyUpdateServicePolicyUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'updateServicePolicyUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicePolicyUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'deleteServicePolicyUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyAttachmentsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyAttachmentsUsingGET('fakedata', 'fakedata', null, null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'getPolicyAttachmentsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachPolicyUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachPolicyUsingPOST('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'attachPolicyUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPolicyUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.detachPolicyUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'detachPolicyUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployPolicyUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployPolicyUsingPOST('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'deployPolicyUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPBRStatsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPBRStatsUsingGET('fakedata', 555, 'fakedata', 'fakedata', 'fakedata', 'fakedata', 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'getPBRStatsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearPBRStatsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clearPBRStatsUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'clearPBRStatsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePoliciesForSwitchUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicePoliciesForSwitchUsingGET('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServicePolicy', 'getServicePoliciesForSwitchUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findEndpointsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findEndpointsUsingGET('fakedata', null, null, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'findEndpointsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveCountUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveCountUsingGET(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getActiveCountUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveSwitchesGroupedByNetworkUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getActiveSwitchesGroupedByNetworkUsingGET(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getActiveSwitchesGroupedByNetworkUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataTableUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataTableUsingGET(null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getDataTableUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHistoricDataUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHistoricDataUsingGET('fakedata', null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getHistoricDataUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHistoricDataDownloadUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHistoricDataDownloadUsingGET('fakedata', null, 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getHistoricDataDownloadUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostLifeDataUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostLifeDataUsingGET(null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getHostLifeDataUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNameUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNodeNameUsingGET(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getNodeNameUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopEndPointCountsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopEndPointCountsUsingGET(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getTopEndPointCountsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrendsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTrendsUsingGET(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_dcnm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EplController', 'getTrendsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
