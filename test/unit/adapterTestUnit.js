/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_dcnm',
      type: 'CiscoDcnm',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const CiscoDcnm = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Cisco_dcnm Adapter Test', () => {
  describe('CiscoDcnm Class Tests', () => {
    const a = new CiscoDcnm(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('cisco_dcnm'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('cisco_dcnm'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('CiscoDcnm', pronghornDotJson.export);
          assert.equal('Cisco_dcnm', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-cisco_dcnm', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('cisco_dcnm'));
          assert.equal('CiscoDcnm', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-cisco_dcnm', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-cisco_dcnm', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#acknowledgeAlarms - errors', () => {
      it('should have a acknowledgeAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.acknowledgeAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateDeactivatePolicies - errors', () => {
      it('should have a activateDeactivatePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.activateDeactivatePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmByID - errors', () => {
      it('should have a getAlarmByID function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmList - errors', () => {
      it('should have a getAlarmList function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearAlarms - errors', () => {
      it('should have a clearAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.clearAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicy - errors', () => {
      it('should have a createPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarms - errors', () => {
      it('should have a deleteAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicies - errors', () => {
      it('should have a deletePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enabledisablealarm - errors', () => {
      it('should have a enabledisablealarm function', (done) => {
        try {
          assert.equal(true, typeof a.enabledisablealarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportAlarmList - errors', () => {
      it('should have a exportAlarmList function', (done) => {
        try {
          assert.equal(true, typeof a.exportAlarmList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmState - errors', () => {
      it('should have a getAlarmState function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmStatistics - errors', () => {
      it('should have a getAlarmStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getcachedata - errors', () => {
      it('should have a getcachedata function', (done) => {
        try {
          assert.equal(true, typeof a.getcachedata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoliciesSummary - errors', () => {
      it('should have a getPoliciesSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getPoliciesSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyDetails - errors', () => {
      it('should have a getPolicyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should have a getPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaisedAlarmCountByCategory - errors', () => {
      it('should have a getRaisedAlarmCountByCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getRaisedAlarmCountByCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaisedAlarmCountByCategoryAndSwitch - errors', () => {
      it('should have a getRaisedAlarmCountByCategoryAndSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.getRaisedAlarmCountByCategoryAndSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recreateAlarmMapping - errors', () => {
      it('should have a recreateAlarmMapping function', (done) => {
        try {
          assert.equal(true, typeof a.recreateAlarmMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rediscoverESurl - errors', () => {
      it('should have a rediscoverESurl function', (done) => {
        try {
          assert.equal(true, typeof a.rediscoverESurl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unAcknowledgeAlarms - errors', () => {
      it('should have a unAcknowledgeAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.unAcknowledgeAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlarmPolicies - errors', () => {
      it('should have a updateAlarmPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlarmPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicies - errors', () => {
      it('should have a updatePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadPolicyFile - errors', () => {
      it('should have a uploadPolicyFile function', (done) => {
        try {
          assert.equal(true, typeof a.uploadPolicyFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoverCSR - errors', () => {
      it('should have a discoverCSR function', (done) => {
        try {
          assert.equal(true, typeof a.discoverCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoverNonNexus - errors', () => {
      it('should have a discoverNonNexus function', (done) => {
        try {
          assert.equal(true, typeof a.discoverNonNexus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetStats - errors', () => {
      it('should have a resetStats function', (done) => {
        try {
          assert.equal(true, typeof a.resetStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stats - errors', () => {
      it('should have a stats function', (done) => {
        try {
          assert.equal(true, typeof a.stats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.stats(null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-stats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployExecConfig - errors', () => {
      it('should have a deployExecConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deployExecConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deployExecConfig(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployExecConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupTracker - errors', () => {
      it('should have a backupTracker function', (done) => {
        try {
          assert.equal(true, typeof a.backupTracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdiscoveryEnableStatus - errors', () => {
      it('should have a getdiscoveryEnableStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getdiscoveryEnableStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatus - errors', () => {
      it('should have a getStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getStatus(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#manageTracker - errors', () => {
      it('should have a manageTracker function', (done) => {
        try {
          assert.equal(true, typeof a.manageTracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.manageTracker(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-manageTracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFabrics - errors', () => {
      it('should have a getAllFabrics function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFabrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFabricAssociations - errors', () => {
      it('should have a listFabricAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.listFabricAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoliciesForRestore - errors', () => {
      it('should have a getPoliciesForRestore function', (done) => {
        try {
          assert.equal(true, typeof a.getPoliciesForRestore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getPoliciesForRestore(null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoliciesForRestore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialnum', (done) => {
        try {
          a.getPoliciesForRestore('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialnum is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoliciesForRestore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchAsnIneBGPRoutedFabric - errors', () => {
      it('should have a getSwitchAsnIneBGPRoutedFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchAsnIneBGPRoutedFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getSwitchAsnIneBGPRoutedFabric(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchAsnIneBGPRoutedFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInterfaceGroup - errors', () => {
      it('should have a createInterfaceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createInterfaceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.createInterfaceGroup(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createInterfaceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.createInterfaceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createInterfaceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIfGroupInterfaces - errors', () => {
      it('should have a getIfGroupInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getIfGroupInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getIfGroupInterfaces(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getIfGroupInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.getIfGroupInterfaces('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getIfGroupInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIfGroupIfAssoc - errors', () => {
      it('should have a createIfGroupIfAssoc function', (done) => {
        try {
          assert.equal(true, typeof a.createIfGroupIfAssoc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.createIfGroupIfAssoc(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createIfGroupIfAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.createIfGroupIfAssoc('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createIfGroupIfAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIfGroupIfAssoc('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createIfGroupIfAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIfGroupNwAssoc - errors', () => {
      it('should have a createIfGroupNwAssoc function', (done) => {
        try {
          assert.equal(true, typeof a.createIfGroupNwAssoc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.createIfGroupNwAssoc(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createIfGroupNwAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.createIfGroupNwAssoc('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createIfGroupNwAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIfGroupNwAssoc('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createIfGroupNwAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIfGroupNwAssoc - errors', () => {
      it('should have a deleteIfGroupNwAssoc function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIfGroupNwAssoc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteIfGroupNwAssoc(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteIfGroupNwAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.deleteIfGroupNwAssoc('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteIfGroupNwAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteIfGroupNwAssoc('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteIfGroupNwAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIfGroupIfAssoc - errors', () => {
      it('should have a deleteIfGroupIfAssoc function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIfGroupIfAssoc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteIfGroupIfAssoc(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteIfGroupIfAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteIfGroupIfAssoc('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteIfGroupIfAssoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceGroups - errors', () => {
      it('should have a getInterfaceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getInterfaceGroups(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getInterfaceGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabric - errors', () => {
      it('should have a getFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getFabric(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessMode - errors', () => {
      it('should have a getAccessMode function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getAccessMode(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAccessMode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configDeploy - errors', () => {
      it('should have a configDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.configDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.configDeploy(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-configDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postconfigDeploy - errors', () => {
      it('should have a postconfigDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postconfigDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postconfigDeploy(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postconfigDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchId', (done) => {
        try {
          a.postconfigDeploy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postconfigDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configDiff - errors', () => {
      it('should have a configDiff function', (done) => {
        try {
          assert.equal(true, typeof a.configDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.configDiff(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-configDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchId', (done) => {
        try {
          a.configDiff('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-configDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configPreview - errors', () => {
      it('should have a configPreview function', (done) => {
        try {
          assert.equal(true, typeof a.configPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.configPreview(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-configPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getconfigPreview - errors', () => {
      it('should have a getconfigPreview function', (done) => {
        try {
          assert.equal(true, typeof a.getconfigPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getconfigPreview(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getconfigPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceName', (done) => {
        try {
          a.getconfigPreview('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'sourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getconfigPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchId', (done) => {
        try {
          a.getconfigPreview('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'switchId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getconfigPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getconfigPreview1 - errors', () => {
      it('should have a getconfigPreview1 function', (done) => {
        try {
          assert.equal(true, typeof a.getconfigPreview1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getconfigPreview1(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getconfigPreview1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchId', (done) => {
        try {
          a.getconfigPreview1('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'switchId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getconfigPreview1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configSave - errors', () => {
      it('should have a configSave function', (done) => {
        try {
          assert.equal(true, typeof a.configSave === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.configSave(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-configSave', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setFabricFreezeMode - errors', () => {
      it('should have a setFabricFreezeMode function', (done) => {
        try {
          assert.equal(true, typeof a.setFabricFreezeMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.setFabricFreezeMode(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-setFabricFreezeMode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setFabricFreezeMode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-setFabricFreezeMode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployMaintenanceModePti - errors', () => {
      it('should have a deployMaintenanceModePti function', (done) => {
        try {
          assert.equal(true, typeof a.deployMaintenanceModePti === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployMaintenanceModePti(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployMaintenanceModePti', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.deployMaintenanceModePti('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployMaintenanceModePti', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFabricWithNvPairs - errors', () => {
      it('should have a createFabricWithNvPairs function', (done) => {
        try {
          assert.equal(true, typeof a.createFabricWithNvPairs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.createFabricWithNvPairs(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createFabricWithNvPairs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.createFabricWithNvPairs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createFabricWithNvPairs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFabricWithNvPairs - errors', () => {
      it('should have a updateFabricWithNvPairs function', (done) => {
        try {
          assert.equal(true, typeof a.updateFabricWithNvPairs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateFabricWithNvPairs(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateFabricWithNvPairs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updateFabricWithNvPairs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateFabricWithNvPairs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceUpdateDTO - errors', () => {
      it('should have a getInterfaceUpdateDTO function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceUpdateDTO === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInterface - errors', () => {
      it('should have a addInterface function', (done) => {
        try {
          assert.equal(true, typeof a.addInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInterface(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInterface - errors', () => {
      it('should have a updateInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInterface(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInterface - errors', () => {
      it('should have a deleteInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteInterface(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adminstatus - errors', () => {
      it('should have a adminstatus function', (done) => {
        try {
          assert.equal(true, typeof a.adminstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.adminstatus(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-adminstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBreakout - errors', () => {
      it('should have a addBreakout function', (done) => {
        try {
          assert.equal(true, typeof a.addBreakout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBreakout(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addBreakout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deploy - errors', () => {
      it('should have a deploy function', (done) => {
        try {
          assert.equal(true, typeof a.deploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deploy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobaInterface - errors', () => {
      it('should have a getGlobaInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getGlobaInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCombinedSerialNumber - errors', () => {
      it('should have a getCombinedSerialNumber function', (done) => {
        try {
          assert.equal(true, typeof a.getCombinedSerialNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getCombinedSerialNumber(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getCombinedSerialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSwitches - errors', () => {
      it('should have a getAllSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getAllSwitches(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoverSwitches - errors', () => {
      it('should have a discoverSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.discoverSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.discoverSwitches(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-discoverSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#switchesWaitingToPOAP - errors', () => {
      it('should have a switchesWaitingToPOAP function', (done) => {
        try {
          assert.equal(true, typeof a.switchesWaitingToPOAP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.switchesWaitingToPOAP(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-switchesWaitingToPOAP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startPoap - errors', () => {
      it('should have a startPoap function', (done) => {
        try {
          assert.equal(true, typeof a.startPoap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.startPoap(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-startPoap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLinks - errors', () => {
      it('should have a getAllLinks function', (done) => {
        try {
          assert.equal(true, typeof a.getAllLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLink - errors', () => {
      it('should have a createLink function', (done) => {
        try {
          assert.equal(true, typeof a.createLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExportLinks - errors', () => {
      it('should have a createExportLinks function', (done) => {
        try {
          assert.equal(true, typeof a.createExportLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.createExportLinks(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createExportLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricLinks - errors', () => {
      it('should have a getFabricLinks function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getFabricLinks(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getFabricLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createImportLinks - errors', () => {
      it('should have a createImportLinks function', (done) => {
        try {
          assert.equal(true, typeof a.createImportLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createImportLinks(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createImportLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLogicalLinks - errors', () => {
      it('should have a getAllLogicalLinks function', (done) => {
        try {
          assert.equal(true, typeof a.getAllLogicalLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getAllLogicalLinks(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllLogicalLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLink - errors', () => {
      it('should have a getLink function', (done) => {
        try {
          assert.equal(true, typeof a.getLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkUUID', (done) => {
        try {
          a.getLink(null, (data, error) => {
            try {
              const displayE = 'linkUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLink - errors', () => {
      it('should have a updateLink function', (done) => {
        try {
          assert.equal(true, typeof a.updateLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkUUID', (done) => {
        try {
          a.updateLink(null, null, (data, error) => {
            try {
              const displayE = 'linkUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLink - errors', () => {
      it('should have a deleteLink function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkUUID', (done) => {
        try {
          a.deleteLink(null, null, (data, error) => {
            try {
              const displayE = 'linkUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabrics - errors', () => {
      it('should have a getFabrics function', (done) => {
        try {
          assert.equal(true, typeof a.getFabrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetFabric - errors', () => {
      it('should have a getgetFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getgetFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetFabric(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworks - errors', () => {
      it('should have a deleteNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVrfs - errors', () => {
      it('should have a deleteVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultiSiteConfigs - errors', () => {
      it('should have a getMultiSiteConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.getMultiSiteConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getMultiSiteConfigs(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getMultiSiteConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployMultiSiteConfigs - errors', () => {
      it('should have a deployMultiSiteConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.deployMultiSiteConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployMultiSiteConfigs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployMultiSiteConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDciOverlayConfigs - errors', () => {
      it('should have a getDciOverlayConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.getDciOverlayConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getDciOverlayConfigs(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getDciOverlayConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployDciOverlayConfigs - errors', () => {
      it('should have a deployDciOverlayConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.deployDciOverlayConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployDciOverlayConfigs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployDciOverlayConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDciOverlayConfigs - errors', () => {
      it('should have a updateDciOverlayConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.updateDciOverlayConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateDciOverlayConfigs(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateDciOverlayConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDciUnderlayConfigs - errors', () => {
      it('should have a getDciUnderlayConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.getDciUnderlayConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getDciUnderlayConfigs(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getDciUnderlayConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployDciUnderlayConfigs - errors', () => {
      it('should have a deployDciUnderlayConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.deployDciUnderlayConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployDciUnderlayConfigs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployDciUnderlayConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListofFaults - errors', () => {
      it('should have a getListofFaults function', (done) => {
        try {
          assert.equal(true, typeof a.getListofFaults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getListofFaults(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getListofFaults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetAllSwitches - errors', () => {
      it('should have a getgetAllSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getgetAllSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetAllSwitches(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetAllSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should have a getNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployNetworks - errors', () => {
      it('should have a deployNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.deployNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworks - errors', () => {
      it('should have a updateNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVniUsage - errors', () => {
      it('should have a getVniUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getVniUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getVniUsage(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getVniUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addFabricToSiteGroup - errors', () => {
      it('should have a addFabricToSiteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addFabricToSiteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.addFabricToSiteGroup(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addFabricToSiteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSiteGroup - errors', () => {
      it('should have a updateSiteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateSiteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateSiteGroup(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateSiteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFabricFromSiteGroup - errors', () => {
      it('should have a deleteFabricFromSiteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFabricFromSiteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteFabricFromSiteGroup(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteFabricFromSiteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVrfs - errors', () => {
      it('should have a getVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.getVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployVrfs - errors', () => {
      it('should have a deployVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.deployVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVrfs - errors', () => {
      it('should have a updateVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.updateVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetGlobaInterface - errors', () => {
      it('should have a getgetGlobaInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getgetGlobaInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#convertMsoSiteGroupToMsd - errors', () => {
      it('should have a convertMsoSiteGroupToMsd function', (done) => {
        try {
          assert.equal(true, typeof a.convertMsoSiteGroupToMsd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteGroupName', (done) => {
        try {
          a.convertMsoSiteGroupToMsd(null, (data, error) => {
            try {
              const displayE = 'siteGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-convertMsoSiteGroupToMsd', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplates - errors', () => {
      it('should have a getTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplatesFields - errors', () => {
      it('should have a getTemplatesFields function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplatesFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getTemplatesFields(null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getTemplatesFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postControlPolicies - errors', () => {
      it('should have a postControlPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.postControlPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployPolicies - errors', () => {
      it('should have a deployPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.deployPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deployPolicies(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateKey - errors', () => {
      it('should have a generateKey function', (done) => {
        try {
          assert.equal(true, typeof a.generateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePoliciesInBulk - errors', () => {
      it('should have a deletePoliciesInBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deletePoliciesInBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyIds', (done) => {
        try {
          a.deletePoliciesInBulk(null, (data, error) => {
            try {
              const displayE = 'policyIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletePoliciesInBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPtiHistory - errors', () => {
      it('should have a getPtiHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getPtiHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getPtiHistory(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPtiHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipleSwitchPolicies - errors', () => {
      it('should have a getMultipleSwitchPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getMultipleSwitchPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getMultipleSwitchPolicies(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getMultipleSwitchPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPoliciesInSwitchesAndSource - errors', () => {
      it('should have a createPoliciesInSwitchesAndSource function', (done) => {
        try {
          assert.equal(true, typeof a.createPoliciesInSwitchesAndSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.createPoliciesInSwitchesAndSource(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createPoliciesInSwitchesAndSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePoliciesInSwitchesAndSource - errors', () => {
      it('should have a updatePoliciesInSwitchesAndSource function', (done) => {
        try {
          assert.equal(true, typeof a.updatePoliciesInSwitchesAndSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.updatePoliciesInSwitchesAndSource(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updatePoliciesInSwitchesAndSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing source', (done) => {
        try {
          a.updatePoliciesInSwitchesAndSource('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'source is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updatePoliciesInSwitchesAndSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePoliciesInSwitchesAndSource - errors', () => {
      it('should have a deletePoliciesInSwitchesAndSource function', (done) => {
        try {
          assert.equal(true, typeof a.deletePoliciesInSwitchesAndSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.deletePoliciesInSwitchesAndSource(null, null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletePoliciesInSwitchesAndSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing source', (done) => {
        try {
          a.deletePoliciesInSwitchesAndSource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'source is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletePoliciesInSwitchesAndSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchPolicies - errors', () => {
      it('should have a getSwitchPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getSwitchPolicies(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markDeleteSwitchPolicies - errors', () => {
      it('should have a markDeleteSwitchPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.markDeleteSwitchPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.markDeleteSwitchPolicies(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-markDeleteSwitchPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSwitchPolicies - errors', () => {
      it('should have a deleteSwitchPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSwitchPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.deleteSwitchPolicies(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteSwitchPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchIntentConfig - errors', () => {
      it('should have a getSwitchIntentConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchIntentConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getSwitchIntentConfig(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchIntentConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchPolicies4Entity - errors', () => {
      it('should have a getSwitchPolicies4Entity function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchPolicies4Entity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getSwitchPolicies4Entity(null, null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityType', (done) => {
        try {
          a.getSwitchPolicies4Entity('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'entityType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityName', (done) => {
        try {
          a.getSwitchPolicies4Entity('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'entityName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markDeleteSwitchPolicies4Entity - errors', () => {
      it('should have a markDeleteSwitchPolicies4Entity function', (done) => {
        try {
          assert.equal(true, typeof a.markDeleteSwitchPolicies4Entity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.markDeleteSwitchPolicies4Entity(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-markDeleteSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityType', (done) => {
        try {
          a.markDeleteSwitchPolicies4Entity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'entityType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-markDeleteSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityName', (done) => {
        try {
          a.markDeleteSwitchPolicies4Entity('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'entityName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-markDeleteSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSwitchPolicies4Entity - errors', () => {
      it('should have a deleteSwitchPolicies4Entity function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSwitchPolicies4Entity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.deleteSwitchPolicies4Entity(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityType', (done) => {
        try {
          a.deleteSwitchPolicies4Entity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'entityType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityName', (done) => {
        try {
          a.deleteSwitchPolicies4Entity('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'entityName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteSwitchPolicies4Entity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControlPoliciesPolicyId - errors', () => {
      it('should have a getControlPoliciesPolicyId function', (done) => {
        try {
          assert.equal(true, typeof a.getControlPoliciesPolicyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getControlPoliciesPolicyId(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getControlPoliciesPolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicy - errors', () => {
      it('should have a updatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updatePolicy(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should have a deletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deletePolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyConfig - errors', () => {
      it('should have a getPolicyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getPolicyConfig(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPolicyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markDeletePolicy - errors', () => {
      it('should have a markDeletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.markDeletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.markDeletePolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-markDeletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchesRole - errors', () => {
      it('should have a getSwitchesRole function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchesRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setSwitchesRole - errors', () => {
      it('should have a setSwitchesRole function', (done) => {
        try {
          assert.equal(true, typeof a.setSwitchesRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setSwitchesRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-setSwitchesRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricNameForSwitch - errors', () => {
      it('should have a getFabricNameForSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricNameForSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getFabricNameForSwitch(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getFabricNameForSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPAddressForSwitch - errors', () => {
      it('should have a getIPAddressForSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.getIPAddressForSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getIPAddressForSwitch(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getIPAddressForSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMOTD - errors', () => {
      it('should have a getMOTD function', (done) => {
        try {
          assert.equal(true, typeof a.getMOTD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setMOTD - errors', () => {
      it('should have a setMOTD function', (done) => {
        try {
          assert.equal(true, typeof a.setMOTD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDefaults - errors', () => {
      it('should have a restoreDefaults function', (done) => {
        try {
          assert.equal(true, typeof a.restoreDefaults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSplashScreenImage - errors', () => {
      it('should have a getSplashScreenImage function', (done) => {
        try {
          assert.equal(true, typeof a.getSplashScreenImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeSplashScreenImage - errors', () => {
      it('should have a changeSplashScreenImage function', (done) => {
        try {
          assert.equal(true, typeof a.changeSplashScreenImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeSplashScreenImage - errors', () => {
      it('should have a removeSplashScreenImage function', (done) => {
        try {
          assert.equal(true, typeof a.removeSplashScreenImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#triggerBackup - errors', () => {
      it('should have a triggerBackup function', (done) => {
        try {
          assert.equal(true, typeof a.triggerBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.triggerBackup(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-triggerBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.triggerBackup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-triggerBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllBackups - errors', () => {
      it('should have a deleteAllBackups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllBackups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteAllBackups(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteAllBackups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSingleBackup - errors', () => {
      it('should have a deleteSingleBackup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSingleBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteSingleBackup(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteSingleBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupVersion', (done) => {
        try {
          a.deleteSingleBackup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'backupVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteSingleBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUploadedEPLDImagesList - errors', () => {
      it('should have a getUploadedEPLDImagesList function', (done) => {
        try {
          assert.equal(true, typeof a.getUploadedEPLDImagesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platform', (done) => {
        try {
          a.getUploadedEPLDImagesList(null, (data, error) => {
            try {
              const displayE = 'platform is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getUploadedEPLDImagesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEPLDModuleList - errors', () => {
      it('should have a getEPLDModuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getEPLDModuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getEPLDModuleList(null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getEPLDModuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epldName', (done) => {
        try {
          a.getEPLDModuleList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'epldName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getEPLDModuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#execute - errors', () => {
      it('should have a execute function', (done) => {
        try {
          assert.equal(true, typeof a.execute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.execute(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-execute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachPolicy - errors', () => {
      it('should have a attachPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.attachPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.attachPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachedPolicies - errors', () => {
      it('should have a getAttachedPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachedPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getAttachedPolicies(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAttachedPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyMapping - errors', () => {
      it('should have a deletePolicyMapping function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.deletePolicyMapping(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletePolicyMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getuploadedNXOSList - errors', () => {
      it('should have a getuploadedNXOSList function', (done) => {
        try {
          assert.equal(true, typeof a.getuploadedNXOSList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platform', (done) => {
        try {
          a.getuploadedNXOSList(null, (data, error) => {
            try {
              const displayE = 'platform is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getuploadedNXOSList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPackageList - errors', () => {
      it('should have a getPackageList function', (done) => {
        try {
          assert.equal(true, typeof a.getPackageList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platform', (done) => {
        try {
          a.getPackageList(null, null, (data, error) => {
            try {
              const displayE = 'platform is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPackageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nxosversion', (done) => {
        try {
          a.getPackageList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nxosversion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPackageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlatformPolicies - errors', () => {
      it('should have a getPlatformPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getPlatformPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreatePolicy - errors', () => {
      it('should have a postcreatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postcreatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreatePolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getuploadedPlatformList - errors', () => {
      it('should have a getuploadedPlatformList function', (done) => {
        try {
          assert.equal(true, typeof a.getuploadedPlatformList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicies - errors', () => {
      it('should have a getAllPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeletePolicy - errors', () => {
      it('should have a deletedeletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletedeletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deletedeletePolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUmberellaPolicy - errors', () => {
      it('should have a createUmberellaPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createUmberellaPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUmberellaPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createUmberellaPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndPorts - errors', () => {
      it('should have a getEndPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getEndPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetFabrics - errors', () => {
      it('should have a getgetFabrics function', (done) => {
        try {
          assert.equal(true, typeof a.getgetFabrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostEnclList - errors', () => {
      it('should have a getHostEnclList function', (done) => {
        try {
          assert.equal(true, typeof a.getHostEnclList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLandingDashboardTopNAnalytics - errors', () => {
      it('should have a getLandingDashboardTopNAnalytics function', (done) => {
        try {
          assert.equal(true, typeof a.getLandingDashboardTopNAnalytics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryScope', (done) => {
        try {
          a.getLandingDashboardTopNAnalytics(null, null, null, null, (data, error) => {
            try {
              const displayE = 'queryScope is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getLandingDashboardTopNAnalytics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryScopeID', (done) => {
        try {
          a.getLandingDashboardTopNAnalytics('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'queryScopeID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getLandingDashboardTopNAnalytics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryType', (done) => {
        try {
          a.getLandingDashboardTopNAnalytics('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'queryType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getLandingDashboardTopNAnalytics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientRequestId', (done) => {
        try {
          a.getLandingDashboardTopNAnalytics('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getLandingDashboardTopNAnalytics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceDetails - errors', () => {
      it('should have a getInterfaceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIsls - errors', () => {
      it('should have a getIsls function', (done) => {
        try {
          assert.equal(true, typeof a.getIsls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenses - errors', () => {
      it('should have a getLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getModules - errors', () => {
      it('should have a getModules function', (done) => {
        try {
          assert.equal(true, typeof a.getModules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeighborLinks - errors', () => {
      it('should have a getNeighborLinks function', (done) => {
        try {
          assert.equal(true, typeof a.getNeighborLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeighborLinksBySwitch - errors', () => {
      it('should have a getNeighborLinksBySwitch function', (done) => {
        try {
          assert.equal(true, typeof a.getNeighborLinksBySwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNpvLinks - errors', () => {
      it('should have a getNpvLinks function', (done) => {
        try {
          assert.equal(true, typeof a.getNpvLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitches - errors', () => {
      it('should have a getSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthScores - errors', () => {
      it('should have a updateHealthScores function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthScores === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVsans - errors', () => {
      it('should have a getVsans function', (done) => {
        try {
          assert.equal(true, typeof a.getVsans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZones - errors', () => {
      it('should have a getZones function', (done) => {
        try {
          assert.equal(true, typeof a.getZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activatePackage - errors', () => {
      it('should have a activatePackage function', (done) => {
        try {
          assert.equal(true, typeof a.activatePackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.activatePackage(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-activatePackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deActivatePackage - errors', () => {
      it('should have a deActivatePackage function', (done) => {
        try {
          assert.equal(true, typeof a.deActivatePackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deActivatePackage(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deActivatePackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#install - errors', () => {
      it('should have a install function', (done) => {
        try {
          assert.equal(true, typeof a.install === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.install(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-install', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPackages - errors', () => {
      it('should have a getPackages function', (done) => {
        try {
          assert.equal(true, typeof a.getPackages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabric', (done) => {
        try {
          a.getPackages(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPackages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uninstall - errors', () => {
      it('should have a uninstall function', (done) => {
        try {
          assert.equal(true, typeof a.uninstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.uninstall(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-uninstall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUploadedPackageList - errors', () => {
      it('should have a getUploadedPackageList function', (done) => {
        try {
          assert.equal(true, typeof a.getUploadedPackageList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platform', (done) => {
        try {
          a.getUploadedPackageList(null, null, (data, error) => {
            try {
              const displayE = 'platform is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getUploadedPackageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getISSUSwitches - errors', () => {
      it('should have a getISSUSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getISSUSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricId', (done) => {
        try {
          a.getISSUSwitches(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getISSUSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPCorrections - errors', () => {
      it('should have a getPTPCorrections function', (done) => {
        try {
          assert.equal(true, typeof a.getPTPCorrections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchName', (done) => {
        try {
          a.getPTPCorrections(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'switchName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPTPCorrections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPParents - errors', () => {
      it('should have a getPTPParents function', (done) => {
        try {
          assert.equal(true, typeof a.getPTPParents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPPortStatuses - errors', () => {
      it('should have a getPTPPortStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.getPTPPortStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPThreshold - errors', () => {
      it('should have a getPTPThreshold function', (done) => {
        try {
          assert.equal(true, typeof a.getPTPThreshold === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBandwidth - errors', () => {
      it('should have a updateBandwidth function', (done) => {
        try {
          assert.equal(true, typeof a.updateBandwidth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateBandwidth(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateBandwidth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcnmVersion - errors', () => {
      it('should have a getDcnmVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getDcnmVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#login - errors', () => {
      it('should have a login function', (done) => {
        try {
          assert.equal(true, typeof a.login === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.login(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-login', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logout - errors', () => {
      it('should have a logout function', (done) => {
        try {
          assert.equal(true, typeof a.logout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTemplate - errors', () => {
      it('should have a getAllTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateBulk - errors', () => {
      it('should have a deleteTemplateBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTemplateJson2 - errors', () => {
      it('should have a createTemplateJson2 function', (done) => {
        try {
          assert.equal(true, typeof a.createTemplateJson2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTemplateJson2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createTemplateJson2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateTemplate - errors', () => {
      it('should have a validateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.validateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.validateTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-validateTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplate - errors', () => {
      it('should have a getTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing populate', (done) => {
        try {
          a.getTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'populate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyTemplate - errors', () => {
      it('should have a modifyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.modifyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.modifyTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-modifyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.modifyTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-modifyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplate - errors', () => {
      it('should have a deleteTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deleteTemplate(null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDefaultValue - errors', () => {
      it('should have a getTemplateDefaultValue function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateDefaultValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getTemplateDefaultValue(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getTemplateDefaultValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPopulatedContent - errors', () => {
      it('should have a getPopulatedContent function', (done) => {
        try {
          assert.equal(true, typeof a.getPopulatedContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPopulatedContent(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPopulatedContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createJob - errors', () => {
      it('should have a createJob function', (done) => {
        try {
          assert.equal(true, typeof a.createJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createJob(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskList - errors', () => {
      it('should have a getTaskList function', (done) => {
        try {
          assert.equal(true, typeof a.getTaskList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getTaskList(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getTaskList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllJobs - errors', () => {
      it('should have a getAllJobs function', (done) => {
        try {
          assert.equal(true, typeof a.getAllJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJob - errors', () => {
      it('should have a deleteJob function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.deleteJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBootMode - errors', () => {
      it('should have a getBootMode function', (done) => {
        try {
          assert.equal(true, typeof a.getBootMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchId', (done) => {
        try {
          a.getBootMode(null, (data, error) => {
            try {
              const displayE = 'switchId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getBootMode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInstalTaskForDevice - errors', () => {
      it('should have a getInstalTaskForDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getInstalTaskForDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchIp', (done) => {
        try {
          a.getInstalTaskForDevice(null, (data, error) => {
            try {
              const displayE = 'switchIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getInstalTaskForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listServers - errors', () => {
      it('should have a listServers function', (done) => {
        try {
          assert.equal(true, typeof a.listServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServer - errors', () => {
      it('should have a createServer function', (done) => {
        try {
          assert.equal(true, typeof a.createServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartImageUpload - errors', () => {
      it('should have a smartImageUpload function', (done) => {
        try {
          assert.equal(true, typeof a.smartImageUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadFile - errors', () => {
      it('should have a uploadFile function', (done) => {
        try {
          assert.equal(true, typeof a.uploadFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverName', (done) => {
        try {
          a.uploadFile(null, null, null, null, (data, error) => {
            try {
              const displayE = 'serverName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-uploadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileType', (done) => {
        try {
          a.uploadFile('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fileType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-uploadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platformType', (done) => {
        try {
          a.uploadFile('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'platformType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-uploadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFiles - errors', () => {
      it('should have a listFiles function', (done) => {
        try {
          assert.equal(true, typeof a.listFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverName', (done) => {
        try {
          a.listFiles(null, null, (data, error) => {
            try {
              const displayE = 'serverName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-listFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listImageFiles - errors', () => {
      it('should have a listImageFiles function', (done) => {
        try {
          assert.equal(true, typeof a.listImageFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverName', (done) => {
        try {
          a.listImageFiles(null, null, (data, error) => {
            try {
              const displayE = 'serverName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-listImageFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartImageDelete - errors', () => {
      it('should have a smartImageDelete function', (done) => {
        try {
          assert.equal(true, typeof a.smartImageDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.smartImageDelete(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-smartImageDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartImageDataUpload - errors', () => {
      it('should have a smartImageDataUpload function', (done) => {
        try {
          assert.equal(true, typeof a.smartImageDataUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartDeleteImages - errors', () => {
      it('should have a smartDeleteImages function', (done) => {
        try {
          assert.equal(true, typeof a.smartDeleteImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageName', (done) => {
        try {
          a.smartDeleteImages(null, (data, error) => {
            try {
              const displayE = 'imageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-smartDeleteImages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartListImages - errors', () => {
      it('should have a smartListImages function', (done) => {
        try {
          assert.equal(true, typeof a.smartListImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServerStatus - errors', () => {
      it('should have a getServerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getServerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartImagesUploadedList - errors', () => {
      it('should have a smartImagesUploadedList function', (done) => {
        try {
          assert.equal(true, typeof a.smartImagesUploadedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServer - errors', () => {
      it('should have a getServer function', (done) => {
        try {
          assert.equal(true, typeof a.getServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverName', (done) => {
        try {
          a.getServer(null, (data, error) => {
            try {
              const displayE = 'serverName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServer - errors', () => {
      it('should have a updateServer function', (done) => {
        try {
          assert.equal(true, typeof a.updateServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverName', (done) => {
        try {
          a.updateServer(null, null, (data, error) => {
            try {
              const displayE = 'serverName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServer - errors', () => {
      it('should have a deleteServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverName', (done) => {
        try {
          a.deleteServer(null, (data, error) => {
            try {
              const displayE = 'serverName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchDirectorySize - errors', () => {
      it('should have a fetchDirectorySize function', (done) => {
        try {
          assert.equal(true, typeof a.fetchDirectorySize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.fetchDirectorySize('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-fetchDirectorySize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchDirectoryContents - errors', () => {
      it('should have a fetchDirectoryContents function', (done) => {
        try {
          assert.equal(true, typeof a.fetchDirectoryContents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.fetchDirectoryContents('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-fetchDirectoryContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFile - errors', () => {
      it('should have a deleteFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchListOfFiles - errors', () => {
      it('should have a searchListOfFiles function', (done) => {
        try {
          assert.equal(true, typeof a.searchListOfFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.searchListOfFiles('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-searchListOfFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigServers - errors', () => {
      it('should have a getConfigServers function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemDefinedDelimiter - errors', () => {
      it('should have a getSystemDefinedDelimiter function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemDefinedDelimiter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allocateResourceId - errors', () => {
      it('should have a allocateResourceId function', (done) => {
        try {
          assert.equal(true, typeof a.allocateResourceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.allocateResourceId(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-allocateResourceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolDetailsOfFabric - errors', () => {
      it('should have a getPoolDetailsOfFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolDetailsOfFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getPoolDetailsOfFabric(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoolDetailsOfFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolDataOfFabricAndPool - errors', () => {
      it('should have a getPoolDataOfFabricAndPool function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolDataOfFabricAndPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getPoolDataOfFabricAndPool(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoolDataOfFabricAndPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolName', (done) => {
        try {
          a.getPoolDataOfFabricAndPool('fakeparam', null, (data, error) => {
            try {
              const displayE = 'poolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoolDataOfFabricAndPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listResourceUsageforFabric - errors', () => {
      it('should have a listResourceUsageforFabric function', (done) => {
        try {
          assert.equal(true, typeof a.listResourceUsageforFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.listResourceUsageforFabric(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-listResourceUsageforFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPoolNames - errors', () => {
      it('should have a listPoolNames function', (done) => {
        try {
          assert.equal(true, typeof a.listPoolNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.listPoolNames(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-listPoolNames', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addResource - errors', () => {
      it('should have a addResource function', (done) => {
        try {
          assert.equal(true, typeof a.addResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.addResource(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMetaData - errors', () => {
      it('should have a addMetaData function', (done) => {
        try {
          assert.equal(true, typeof a.addMetaData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addMetaData(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addMetaData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseResourceId - errors', () => {
      it('should have a releaseResourceId function', (done) => {
        try {
          assert.equal(true, typeof a.releaseResourceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.releaseResourceId(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-releaseResourceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseResource - errors', () => {
      it('should have a releaseResource function', (done) => {
        try {
          assert.equal(true, typeof a.releaseResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.releaseResource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-releaseResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOrUpdatePool - errors', () => {
      it('should have a addOrUpdatePool function', (done) => {
        try {
          assert.equal(true, typeof a.addOrUpdatePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addOrUpdatePool(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addOrUpdatePool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allocateIpResourceAndReturn - errors', () => {
      it('should have a allocateIpResourceAndReturn function', (done) => {
        try {
          assert.equal(true, typeof a.allocateIpResourceAndReturn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.allocateIpResourceAndReturn(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-allocateIpResourceAndReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseResourceIp - errors', () => {
      it('should have a releaseResourceIp function', (done) => {
        try {
          assert.equal(true, typeof a.releaseResourceIp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rmid', (done) => {
        try {
          a.releaseResourceIp(null, (data, error) => {
            try {
              const displayE = 'rmid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-releaseResourceIp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkSubnetOrIpResourceAvailable - errors', () => {
      it('should have a checkSubnetOrIpResourceAvailable function', (done) => {
        try {
          assert.equal(true, typeof a.checkSubnetOrIpResourceAvailable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.checkSubnetOrIpResourceAvailable(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-checkSubnetOrIpResourceAvailable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolDetailsOfSwitch - errors', () => {
      it('should have a getPoolDetailsOfSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolDetailsOfSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getPoolDetailsOfSwitch(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoolDetailsOfSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolDataOfSwitchAndPool - errors', () => {
      it('should have a getPoolDataOfSwitchAndPool function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolDataOfSwitchAndPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getPoolDataOfSwitchAndPool(null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoolDataOfSwitchAndPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolName', (done) => {
        try {
          a.getPoolDataOfSwitchAndPool('fakeparam', null, (data, error) => {
            try {
              const displayE = 'poolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPoolDataOfSwitchAndPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listServiceNetworks - errors', () => {
      it('should have a listServiceNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.listServiceNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.listServiceNetworks(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-listServiceNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyIntent - errors', () => {
      it('should have a applyIntent function', (done) => {
        try {
          assert.equal(true, typeof a.applyIntent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.applyIntent(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-applyIntent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIntent - errors', () => {
      it('should have a createIntent function', (done) => {
        try {
          assert.equal(true, typeof a.createIntent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIntent(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createIntent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntent - errors', () => {
      it('should have a deleteIntent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIntent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteIntent(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteIntent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricInfo - errors', () => {
      it('should have a getFabricInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPreviewInfo - errors', () => {
      it('should have a getPreviewInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getPreviewInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.getPreviewInfo(null, null, null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPreviewInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cliTag', (done) => {
        try {
          a.getPreviewInfo('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'cliTag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPreviewInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getPreviewInfo('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPreviewInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchList', (done) => {
        try {
          a.getPreviewInfo('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'switchList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPreviewInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobStatus - errors', () => {
      it('should have a getJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.getJobStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cliTag', (done) => {
        try {
          a.getJobStatus('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'cliTag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getJobStatus('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing verbose', (done) => {
        try {
          a.getJobStatus('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'verbose is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchesInfo - errors', () => {
      it('should have a getSwitchesInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchesInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricId', (done) => {
        try {
          a.getSwitchesInfo(null, (data, error) => {
            try {
              const displayE = 'fabricId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchesInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkNetwork - errors', () => {
      it('should have a createBulkNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkNetwork(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createBulkNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkImportNetwork - errors', () => {
      it('should have a createBulkImportNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkImportNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkImportNetwork(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createBulkImportNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworks - errors', () => {
      it('should have a listNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.listNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-listNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetwork - errors', () => {
      it('should have a createNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.createNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAttachDetails - errors', () => {
      it('should have a getNetworkAttachDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAttachDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getNetworkAttachDetails(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.getNetworkAttachDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachNetworks - errors', () => {
      it('should have a attachNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.attachNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.attachNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.attachNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postdeployNetworks - errors', () => {
      it('should have a postdeployNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.postdeployNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postdeployNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postdeployNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewNetworks - errors', () => {
      it('should have a previewNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.previewNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.previewNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-previewNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.previewNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-previewNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPortsAndStatus - errors', () => {
      it('should have a getAllPortsAndStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPortsAndStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getAllPortsAndStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllPortsAndStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.getAllPortsAndStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllPortsAndStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getAllPortsAndStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllPortsAndStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchDeploymentHistory - errors', () => {
      it('should have a getSwitchDeploymentHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchDeploymentHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getSwitchDeploymentHistory(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.getSwitchDeploymentHistory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getSwitchDeploymentHistory('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetwork - errors', () => {
      it('should have a getNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetwork - errors', () => {
      it('should have a updateNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.updateNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should have a deleteNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.deleteNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleNetworkAttachDetails - errors', () => {
      it('should have a getSingleNetworkAttachDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getSingleNetworkAttachDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getSingleNetworkAttachDetails(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSingleNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getSingleNetworkAttachDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSingleNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachNetwork - errors', () => {
      it('should have a attachNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.attachNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.attachNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.attachNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.attachNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployNetwork - errors', () => {
      it('should have a deployNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deployNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.deployNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewNetwork - errors', () => {
      it('should have a previewNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.previewNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.previewNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-previewNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.previewNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-previewNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkStatusDetails - errors', () => {
      it('should have a networkStatusDetails function', (done) => {
        try {
          assert.equal(true, typeof a.networkStatusDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.networkStatusDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-networkStatusDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.networkStatusDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-networkStatusDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleSwitchDeploymentHistory - errors', () => {
      it('should have a getSingleSwitchDeploymentHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getSingleSwitchDeploymentHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getSingleSwitchDeploymentHistory(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSingleSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getSingleSwitchDeploymentHistory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSingleSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getSingleSwitchDeploymentHistory('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSingleSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPortsAndStatusForSingle - errors', () => {
      it('should have a getAllPortsAndStatusForSingle function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPortsAndStatusForSingle === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getAllPortsAndStatusForSingle(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllPortsAndStatusForSingle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getAllPortsAndStatusForSingle('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllPortsAndStatusForSingle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getAllPortsAndStatusForSingle('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAllPortsAndStatusForSingle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceNetwork - errors', () => {
      it('should have a deleteServiceNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteServiceNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServiceNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.deleteServiceNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServiceNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreateBulkNetwork - errors', () => {
      it('should have a postcreateBulkNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.postcreateBulkNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreateBulkNetwork(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateBulkNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreateBulkImportNetwork - errors', () => {
      it('should have a postcreateBulkImportNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.postcreateBulkImportNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreateBulkImportNetwork(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateBulkImportNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getlistNetworks - errors', () => {
      it('should have a getlistNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getlistNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getlistNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getlistNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreateNetwork - errors', () => {
      it('should have a postcreateNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.postcreateNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postcreateNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreateNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetNetworkAttachDetails - errors', () => {
      it('should have a getgetNetworkAttachDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getgetNetworkAttachDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetNetworkAttachDetails(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.getgetNetworkAttachDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postattachNetworks - errors', () => {
      it('should have a postattachNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.postattachNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postattachNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postattachNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postattachNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postattachNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postdeployNetworks1 - errors', () => {
      it('should have a postdeployNetworks1 function', (done) => {
        try {
          assert.equal(true, typeof a.postdeployNetworks1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postdeployNetworks1(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployNetworks1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postdeployNetworks1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployNetworks1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpreviewNetworks - errors', () => {
      it('should have a getpreviewNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getpreviewNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getpreviewNetworks(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getpreviewNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.getpreviewNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getpreviewNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetAllPortsAndStatus - errors', () => {
      it('should have a getgetAllPortsAndStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getgetAllPortsAndStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetAllPortsAndStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetAllPortsAndStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.getgetAllPortsAndStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetAllPortsAndStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getgetAllPortsAndStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetAllPortsAndStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSwitchDeploymentHistory - errors', () => {
      it('should have a getgetSwitchDeploymentHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getgetSwitchDeploymentHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetSwitchDeploymentHistory(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkNames', (done) => {
        try {
          a.getgetSwitchDeploymentHistory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getgetSwitchDeploymentHistory('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetNetwork - errors', () => {
      it('should have a getgetNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getgetNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getgetNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putupdateNetwork - errors', () => {
      it('should have a putupdateNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.putupdateNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.putupdateNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-putupdateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.putupdateNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-putupdateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putupdateNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-putupdateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteNetwork - errors', () => {
      it('should have a deletedeleteNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deletedeleteNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deletedeleteNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.deletedeleteNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSingleNetworkAttachDetails - errors', () => {
      it('should have a getgetSingleNetworkAttachDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getgetSingleNetworkAttachDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetSingleNetworkAttachDetails(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSingleNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getgetSingleNetworkAttachDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSingleNetworkAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postattachNetwork - errors', () => {
      it('should have a postattachNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.postattachNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postattachNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postattachNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.postattachNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postattachNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postattachNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postattachNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postdeployNetwork - errors', () => {
      it('should have a postdeployNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.postdeployNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postdeployNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.postdeployNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpreviewNetwork - errors', () => {
      it('should have a getpreviewNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getpreviewNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getpreviewNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getpreviewNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getpreviewNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getpreviewNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getnetworkStatusDetails - errors', () => {
      it('should have a getnetworkStatusDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getnetworkStatusDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getnetworkStatusDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getnetworkStatusDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getnetworkStatusDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getnetworkStatusDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSingleSwitchDeploymentHistory - errors', () => {
      it('should have a getgetSingleSwitchDeploymentHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getgetSingleSwitchDeploymentHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetSingleSwitchDeploymentHistory(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSingleSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getgetSingleSwitchDeploymentHistory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSingleSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getgetSingleSwitchDeploymentHistory('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSingleSwitchDeploymentHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetAllPortsAndStatusForSingle - errors', () => {
      it('should have a getgetAllPortsAndStatusForSingle function', (done) => {
        try {
          assert.equal(true, typeof a.getgetAllPortsAndStatusForSingle === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetAllPortsAndStatusForSingle(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetAllPortsAndStatusForSingle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.getgetAllPortsAndStatusForSingle('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetAllPortsAndStatusForSingle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getgetAllPortsAndStatusForSingle('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetAllPortsAndStatusForSingle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteServiceNetwork - errors', () => {
      it('should have a deletedeleteServiceNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deletedeleteServiceNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deletedeleteServiceNetwork(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteServiceNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.deletedeleteServiceNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteServiceNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkVRF - errors', () => {
      it('should have a createBulkVRF function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkVRF(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createBulkVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkImportVRF - errors', () => {
      it('should have a createBulkImportVRF function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkImportVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkImportVRF(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createBulkImportVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCdpNeighbor - errors', () => {
      it('should have a getCdpNeighbor function', (done) => {
        try {
          assert.equal(true, typeof a.getCdpNeighbor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getCdpNeighbor(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getCdpNeighbor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployExtensionPrototype - errors', () => {
      it('should have a deployExtensionPrototype function', (done) => {
        try {
          assert.equal(true, typeof a.deployExtensionPrototype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployExtensionPrototype(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployExtensionPrototype', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deployExtensionPrototype('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployExtensionPrototype', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateExtensionPreview - errors', () => {
      it('should have a generateExtensionPreview function', (done) => {
        try {
          assert.equal(true, typeof a.generateExtensionPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.generateExtensionPreview(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-generateExtensionPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.generateExtensionPreview('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-generateExtensionPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceVRF - errors', () => {
      it('should have a deleteServiceVRF function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteServiceVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServiceVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.deleteServiceVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServiceVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVRF - errors', () => {
      it('should have a listVRF function', (done) => {
        try {
          assert.equal(true, typeof a.listVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.listVRF(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-listVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVRF - errors', () => {
      it('should have a createVRF function', (done) => {
        try {
          assert.equal(true, typeof a.createVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.createVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-createVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVRFAttachDetails - errors', () => {
      it('should have a getVRFAttachDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getVRFAttachDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getVRFAttachDetails(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getVRFAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.getVRFAttachDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getVRFAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachVrfs - errors', () => {
      it('should have a attachVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.attachVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.attachVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.attachVrfs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postdeployVrfs - errors', () => {
      it('should have a postdeployVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.postdeployVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postdeployVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postdeployVrfs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewVrfs - errors', () => {
      it('should have a previewVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.previewVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.previewVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-previewVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.previewVrfs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-previewVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchDetailsByVRF - errors', () => {
      it('should have a getSwitchDetailsByVRF function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchDetailsByVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getSwitchDetailsByVRF(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchDetailsByVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.getSwitchDetailsByVRF('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchDetailsByVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getSwitchDetailsByVRF('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getSwitchDetailsByVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSwitchDeploymentHistory1 - errors', () => {
      it('should have a getgetSwitchDeploymentHistory1 function', (done) => {
        try {
          assert.equal(true, typeof a.getgetSwitchDeploymentHistory1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetSwitchDeploymentHistory1(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.getgetSwitchDeploymentHistory1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getgetSwitchDeploymentHistory1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVRF - errors', () => {
      it('should have a getVRF function', (done) => {
        try {
          assert.equal(true, typeof a.getVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.getVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVRF - errors', () => {
      it('should have a updateVRF function', (done) => {
        try {
          assert.equal(true, typeof a.updateVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateVRF(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.updateVRF('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVRF('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVRF - errors', () => {
      it('should have a deleteVRF function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.deleteVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreateBulkVRF - errors', () => {
      it('should have a postcreateBulkVRF function', (done) => {
        try {
          assert.equal(true, typeof a.postcreateBulkVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreateBulkVRF(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateBulkVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreateBulkImportVRF - errors', () => {
      it('should have a postcreateBulkImportVRF function', (done) => {
        try {
          assert.equal(true, typeof a.postcreateBulkImportVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreateBulkImportVRF(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateBulkImportVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postgetCdpNeighbor - errors', () => {
      it('should have a postgetCdpNeighbor function', (done) => {
        try {
          assert.equal(true, typeof a.postgetCdpNeighbor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postgetCdpNeighbor(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postgetCdpNeighbor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteServiceVRF - errors', () => {
      it('should have a deletedeleteServiceVRF function', (done) => {
        try {
          assert.equal(true, typeof a.deletedeleteServiceVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deletedeleteServiceVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteServiceVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.deletedeleteServiceVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteServiceVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getlistVRF - errors', () => {
      it('should have a getlistVRF function', (done) => {
        try {
          assert.equal(true, typeof a.getlistVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getlistVRF(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getlistVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreateVRF - errors', () => {
      it('should have a postcreateVRF function', (done) => {
        try {
          assert.equal(true, typeof a.postcreateVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postcreateVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreateVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postcreateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetVRFAttachDetails - errors', () => {
      it('should have a getgetVRFAttachDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getgetVRFAttachDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetVRFAttachDetails(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetVRFAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.getgetVRFAttachDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetVRFAttachDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postattachVrfs - errors', () => {
      it('should have a postattachVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.postattachVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postattachVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postattachVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postattachVrfs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postattachVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postdeployVrfs1 - errors', () => {
      it('should have a postdeployVrfs1 function', (done) => {
        try {
          assert.equal(true, typeof a.postdeployVrfs1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.postdeployVrfs1(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployVrfs1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postdeployVrfs1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-postdeployVrfs1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpreviewVrfs - errors', () => {
      it('should have a getpreviewVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.getpreviewVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getpreviewVrfs(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getpreviewVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.getpreviewVrfs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getpreviewVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSwitchDetailsByVRF - errors', () => {
      it('should have a getgetSwitchDetailsByVRF function', (done) => {
        try {
          assert.equal(true, typeof a.getgetSwitchDetailsByVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetSwitchDetailsByVRF(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDetailsByVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.getgetSwitchDetailsByVRF('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDetailsByVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getgetSwitchDetailsByVRF('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDetailsByVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSwitchDeploymentHistory12 - errors', () => {
      it('should have a getgetSwitchDeploymentHistory12 function', (done) => {
        try {
          assert.equal(true, typeof a.getgetSwitchDeploymentHistory12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetSwitchDeploymentHistory12(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfNames', (done) => {
        try {
          a.getgetSwitchDeploymentHistory12('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vrfNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumbers', (done) => {
        try {
          a.getgetSwitchDeploymentHistory12('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumbers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetSwitchDeploymentHistory12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetVRF - errors', () => {
      it('should have a getgetVRF function', (done) => {
        try {
          assert.equal(true, typeof a.getgetVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getgetVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.getgetVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getgetVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putupdateVRF - errors', () => {
      it('should have a putupdateVRF function', (done) => {
        try {
          assert.equal(true, typeof a.putupdateVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.putupdateVRF(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-putupdateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.putupdateVRF('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-putupdateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putupdateVRF('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-putupdateVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteVRF - errors', () => {
      it('should have a deletedeleteVRF function', (done) => {
        try {
          assert.equal(true, typeof a.deletedeleteVRF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deletedeleteVRF(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfName', (done) => {
        try {
          a.deletedeleteVRF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deletedeleteVRF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogsUsingGET - errors', () => {
      it('should have a getAuditLogsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditLogsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuditLogsUsingDELETE - errors', () => {
      it('should have a deleteAuditLogsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuditLogsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing daysToRetain', (done) => {
        try {
          a.deleteAuditLogsUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'daysToRetain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteAuditLogsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePeeringsUsingGET1 - errors', () => {
      it('should have a getRoutePeeringsUsingGET1 function', (done) => {
        try {
          assert.equal(true, typeof a.getRoutePeeringsUsingGET1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getRoutePeeringsUsingGET1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringsUsingGET1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getRoutePeeringsUsingGET1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringsUsingGET1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRoutePeeringUsingPOST - errors', () => {
      it('should have a addRoutePeeringUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addRoutePeeringUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.addRoutePeeringUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addRoutePeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.addRoutePeeringUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addRoutePeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRoutePeeringUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addRoutePeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePeeringUsingDELETE2 - errors', () => {
      it('should have a deleteRoutePeeringUsingDELETE2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoutePeeringUsingDELETE2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE2(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE2('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePeeringsUsingGET - errors', () => {
      it('should have a getRoutePeeringsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRoutePeeringsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getRoutePeeringsUsingGET(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getRoutePeeringsUsingGET('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getRoutePeeringsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePeeringUsingDELETE1 - errors', () => {
      it('should have a deleteRoutePeeringUsingDELETE1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoutePeeringUsingDELETE1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE1(null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachPeeringsUsingPOST - errors', () => {
      it('should have a attachPeeringsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.attachPeeringsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.attachPeeringsUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPeeringsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.attachPeeringsUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPeeringsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.attachPeeringsUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPeeringsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPeeringsUsingDELETE - errors', () => {
      it('should have a detachPeeringsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.detachPeeringsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.detachPeeringsUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.detachPeeringsUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.detachPeeringsUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringNames', (done) => {
        try {
          a.detachPeeringsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployPeeringsUsingPOST - errors', () => {
      it('should have a deployPeeringsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.deployPeeringsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deployPeeringsUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPeeringsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployPeeringsUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPeeringsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deployPeeringsUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPeeringsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePeeringUsingGET - errors', () => {
      it('should have a getRoutePeeringUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRoutePeeringUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getRoutePeeringUsingGET(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getRoutePeeringUsingGET('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getRoutePeeringUsingGET('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.getRoutePeeringUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getRoutePeeringUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRoutePeeringUsingPUT - errors', () => {
      it('should have a updateRoutePeeringUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateRoutePeeringUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.updateRoutePeeringUsingPUT(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateRoutePeeringUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateRoutePeeringUsingPUT('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateRoutePeeringUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.updateRoutePeeringUsingPUT('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateRoutePeeringUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.updateRoutePeeringUsingPUT('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateRoutePeeringUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRoutePeeringUsingPUT('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateRoutePeeringUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePeeringUsingDELETE - errors', () => {
      it('should have a deleteRoutePeeringUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoutePeeringUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.deleteRoutePeeringUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteRoutePeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPeeringAttachmentsUsingGET - errors', () => {
      it('should have a getPeeringAttachmentsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPeeringAttachmentsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getPeeringAttachmentsUsingGET(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPeeringAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getPeeringAttachmentsUsingGET('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPeeringAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getPeeringAttachmentsUsingGET('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPeeringAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.getPeeringAttachmentsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPeeringAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachPeeringUsingPOST - errors', () => {
      it('should have a attachPeeringUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.attachPeeringUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.attachPeeringUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.attachPeeringUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.attachPeeringUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.attachPeeringUsingPOST('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPeeringUsingDELETE - errors', () => {
      it('should have a detachPeeringUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.detachPeeringUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.detachPeeringUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.detachPeeringUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.detachPeeringUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.detachPeeringUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPeeringUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployPeeringUsingPOST - errors', () => {
      it('should have a deployPeeringUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.deployPeeringUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deployPeeringUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployPeeringUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deployPeeringUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.deployPeeringUsingPOST('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPeeringUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachedServiceNodesUsingGET - errors', () => {
      it('should have a getAttachedServiceNodesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachedServiceNodesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getAttachedServiceNodesUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getAttachedServiceNodesUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceNodesUsingGET1 - errors', () => {
      it('should have a getServiceNodesUsingGET1 function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceNodesUsingGET1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getServiceNodesUsingGET1(null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServiceNodesUsingGET1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addServiceNodeUsingPOST - errors', () => {
      it('should have a addServiceNodeUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addServiceNodeUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.addServiceNodeUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addServiceNodeUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addServiceNodeUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addServiceNodeUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceNodeUsingGET - errors', () => {
      it('should have a getServiceNodeUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceNodeUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getServiceNodeUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServiceNodeUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getServiceNodeUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServiceNodeUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServiceNodeUsingPUT1 - errors', () => {
      it('should have a updateServiceNodeUsingPUT1 function', (done) => {
        try {
          assert.equal(true, typeof a.updateServiceNodeUsingPUT1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateServiceNodeUsingPUT1(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServiceNodeUsingPUT1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.updateServiceNodeUsingPUT1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServiceNodeUsingPUT1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateServiceNodeUsingPUT1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServiceNodeUsingPUT1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceNodeUsingDELETE1 - errors', () => {
      it('should have a deleteServiceNodeUsingDELETE1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceNodeUsingDELETE1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteServiceNodeUsingDELETE1(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServiceNodeUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteServiceNodeUsingDELETE1('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServiceNodeUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePoliciesUsingGET1 - errors', () => {
      it('should have a getServicePoliciesUsingGET1 function', (done) => {
        try {
          assert.equal(true, typeof a.getServicePoliciesUsingGET1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getServicePoliciesUsingGET1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePoliciesUsingGET1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getServicePoliciesUsingGET1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePoliciesUsingGET1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addServicePolicyUsingPOST - errors', () => {
      it('should have a addServicePolicyUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addServicePolicyUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.addServicePolicyUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addServicePolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.addServicePolicyUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addServicePolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addServicePolicyUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-addServicePolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicePolicyUsingDELETE2 - errors', () => {
      it('should have a deleteServicePolicyUsingDELETE2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServicePolicyUsingDELETE2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE2(null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE2('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePoliciesUsingGET - errors', () => {
      it('should have a getServicePoliciesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getServicePoliciesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getServicePoliciesUsingGET(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePoliciesUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getServicePoliciesUsingGET('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePoliciesUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getServicePoliciesUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePoliciesUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicePolicyUsingDELETE1 - errors', () => {
      it('should have a deleteServicePolicyUsingDELETE1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServicePolicyUsingDELETE1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE1(null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachPoliciesUsingPOST - errors', () => {
      it('should have a attachPoliciesUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.attachPoliciesUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.attachPoliciesUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPoliciesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.attachPoliciesUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPoliciesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.attachPoliciesUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPoliciesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPoliciesUsingDELETE - errors', () => {
      it('should have a detachPoliciesUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.detachPoliciesUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.detachPoliciesUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPoliciesUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.detachPoliciesUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPoliciesUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.detachPoliciesUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPoliciesUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyNames', (done) => {
        try {
          a.detachPoliciesUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPoliciesUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployPoliciesUsingPOST - errors', () => {
      it('should have a deployPoliciesUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.deployPoliciesUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deployPoliciesUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPoliciesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployPoliciesUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPoliciesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deployPoliciesUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPoliciesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePolicyUsingGET - errors', () => {
      it('should have a getServicePolicyUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getServicePolicyUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getServicePolicyUsingGET(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePolicyUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getServicePolicyUsingGET('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePolicyUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getServicePolicyUsingGET('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePolicyUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.getServicePolicyUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePolicyUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServicePolicyUsingPUT - errors', () => {
      it('should have a updateServicePolicyUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateServicePolicyUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.updateServicePolicyUsingPUT(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServicePolicyUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.updateServicePolicyUsingPUT('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServicePolicyUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.updateServicePolicyUsingPUT('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServicePolicyUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.updateServicePolicyUsingPUT('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServicePolicyUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateServicePolicyUsingPUT('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-updateServicePolicyUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicePolicyUsingDELETE - errors', () => {
      it('should have a deleteServicePolicyUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServicePolicyUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.deleteServicePolicyUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deleteServicePolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyAttachmentsUsingGET - errors', () => {
      it('should have a getPolicyAttachmentsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyAttachmentsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getPolicyAttachmentsUsingGET(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPolicyAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getPolicyAttachmentsUsingGET('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPolicyAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getPolicyAttachmentsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPolicyAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.getPolicyAttachmentsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPolicyAttachmentsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachPolicyUsingPOST - errors', () => {
      it('should have a attachPolicyUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.attachPolicyUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.attachPolicyUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.attachPolicyUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.attachPolicyUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.attachPolicyUsingPOST('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-attachPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachPolicyUsingDELETE - errors', () => {
      it('should have a detachPolicyUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.detachPolicyUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.detachPolicyUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.detachPolicyUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.detachPolicyUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.detachPolicyUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-detachPolicyUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployPolicyUsingPOST - errors', () => {
      it('should have a deployPolicyUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.deployPolicyUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.deployPolicyUsingPOST(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.deployPolicyUsingPOST('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deployPolicyUsingPOST('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.deployPolicyUsingPOST('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-deployPolicyUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPBRStatsUsingGET - errors', () => {
      it('should have a getPBRStatsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPBRStatsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getPBRStatsUsingGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPBRStatsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing end', (done) => {
        try {
          a.getPBRStatsUsingGET('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'end is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPBRStatsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.getPBRStatsUsingGET('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPBRStatsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getPBRStatsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPBRStatsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeType', (done) => {
        try {
          a.getPBRStatsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPBRStatsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.getPBRStatsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPBRStatsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing start', (done) => {
        try {
          a.getPBRStatsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'start is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getPBRStatsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearPBRStatsUsingDELETE - errors', () => {
      it('should have a clearPBRStatsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.clearPBRStatsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.clearPBRStatsUsingDELETE(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-clearPBRStatsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.clearPBRStatsUsingDELETE('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-clearPBRStatsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.clearPBRStatsUsingDELETE('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-clearPBRStatsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeType', (done) => {
        try {
          a.clearPBRStatsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-clearPBRStatsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.clearPBRStatsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-clearPBRStatsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicePoliciesForSwitchUsingGET - errors', () => {
      it('should have a getServicePoliciesForSwitchUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getServicePoliciesForSwitchUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachedFabric', (done) => {
        try {
          a.getServicePoliciesForSwitchUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'attachedFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getServicePoliciesForSwitchUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findEndpointsUsingGET - errors', () => {
      it('should have a findEndpointsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.findEndpointsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabricName', (done) => {
        try {
          a.findEndpointsUsingGET(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fabricName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-findEndpointsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vni', (done) => {
        try {
          a.findEndpointsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vni is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-findEndpointsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveCountUsingGET - errors', () => {
      it('should have a getActiveCountUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveCountUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getActiveCountUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getActiveCountUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveSwitchesGroupedByNetworkUsingGET - errors', () => {
      it('should have a getActiveSwitchesGroupedByNetworkUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveSwitchesGroupedByNetworkUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataTableUsingGET - errors', () => {
      it('should have a getDataTableUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDataTableUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getDataTableUsingGET('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getDataTableUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHistoricDataUsingGET - errors', () => {
      it('should have a getHistoricDataUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getHistoricDataUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endDate', (done) => {
        try {
          a.getHistoricDataUsingGET(null, null, null, null, (data, error) => {
            try {
              const displayE = 'endDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHistoricDataUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startDate', (done) => {
        try {
          a.getHistoricDataUsingGET('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHistoricDataUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getHistoricDataUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHistoricDataUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHistoricDataDownloadUsingGET - errors', () => {
      it('should have a getHistoricDataDownloadUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getHistoricDataDownloadUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endDate', (done) => {
        try {
          a.getHistoricDataDownloadUsingGET(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHistoricDataDownloadUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startDate', (done) => {
        try {
          a.getHistoricDataDownloadUsingGET('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHistoricDataDownloadUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getHistoricDataDownloadUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHistoricDataDownloadUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostLifeDataUsingGET - errors', () => {
      it('should have a getHostLifeDataUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getHostLifeDataUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.getHostLifeDataUsingGET('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHostLifeDataUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vni', (done) => {
        try {
          a.getHostLifeDataUsingGET('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vni is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getHostLifeDataUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNameUsingGET - errors', () => {
      it('should have a getNodeNameUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeNameUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopEndPointCountsUsingGET - errors', () => {
      it('should have a getTopEndPointCountsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getTopEndPointCountsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getTopEndPointCountsUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getTopEndPointCountsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrendsUsingGET - errors', () => {
      it('should have a getTrendsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getTrendsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getTrendsUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_dcnm-adapter-getTrendsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
