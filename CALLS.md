## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco DCNM. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco DCNM.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cisco Data Center Network Management. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">acknowledgeAlarms(body, callback)</td>
    <td style="padding:15px">AcknowledgeAlarms</td>
    <td style="padding:15px">{base_path}/{version}/alarms/acknowledgealarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateDeactivatePolicies(activate, body, callback)</td>
    <td style="padding:15px">ActivateDeactivatePolicies</td>
    <td style="padding:15px">{base_path}/{version}/alarms/activatedeactivatepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmByID(alarmid, callback)</td>
    <td style="padding:15px">getAlarmByID</td>
    <td style="padding:15px">{base_path}/{version}/alarms/alarmbyid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmList(filter, sortorder, history, callback)</td>
    <td style="padding:15px">getAlarmList</td>
    <td style="padding:15px">{base_path}/{version}/alarms/alarmlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAlarms(body, callback)</td>
    <td style="padding:15px">ClearAlarms</td>
    <td style="padding:15px">{base_path}/{version}/alarms/clearalarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(body, callback)</td>
    <td style="padding:15px">CreatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/alarms/createpolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarms(body, callback)</td>
    <td style="padding:15px">DeleteAlarms</td>
    <td style="padding:15px">{base_path}/{version}/alarms/deletealarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicies(body, callback)</td>
    <td style="padding:15px">DeletePolicies</td>
    <td style="padding:15px">{base_path}/{version}/alarms/deletepolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enabledisablealarm(enable, callback)</td>
    <td style="padding:15px">enabledisablealarm</td>
    <td style="padding:15px">{base_path}/{version}/alarms/enabledisablealarm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportAlarmList(history, callback)</td>
    <td style="padding:15px">exportAlarmList</td>
    <td style="padding:15px">{base_path}/{version}/alarms/exportalarmstofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmState(callback)</td>
    <td style="padding:15px">getAlarmState</td>
    <td style="padding:15px">{base_path}/{version}/alarms/getalarmstate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmStatistics(callback)</td>
    <td style="padding:15px">getAlarmStatistics</td>
    <td style="padding:15px">{base_path}/{version}/alarms/getalarmstatistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcachedata(callback)</td>
    <td style="padding:15px">getcachedata</td>
    <td style="padding:15px">{base_path}/{version}/alarms/getcachedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoliciesSummary(callback)</td>
    <td style="padding:15px">GetPoliciesSummary</td>
    <td style="padding:15px">{base_path}/{version}/alarms/policiessummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDetails(name, callback)</td>
    <td style="padding:15px">GetPolicyDetails</td>
    <td style="padding:15px">{base_path}/{version}/alarms/policydetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(name, callback)</td>
    <td style="padding:15px">GetPolicy</td>
    <td style="padding:15px">{base_path}/{version}/alarms/policyinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaisedAlarmCountByCategory(callback)</td>
    <td style="padding:15px">getRaisedAlarmCountByCategory</td>
    <td style="padding:15px">{base_path}/{version}/alarms/raisedalarmcategorycount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaisedAlarmCountByCategoryAndSwitch(callback)</td>
    <td style="padding:15px">getRaisedAlarmCountByCategoryAndSwitch</td>
    <td style="padding:15px">{base_path}/{version}/alarms/raisedalarmswitchcategorycount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recreateAlarmMapping(callback)</td>
    <td style="padding:15px">recreateAlarmMapping</td>
    <td style="padding:15px">{base_path}/{version}/alarms/recreatealarmmapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rediscoverESurl(callback)</td>
    <td style="padding:15px">rediscoverESurl</td>
    <td style="padding:15px">{base_path}/{version}/alarms/rediscoveresurl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unAcknowledgeAlarms(body, callback)</td>
    <td style="padding:15px">UnAcknowledgeAlarms</td>
    <td style="padding:15px">{base_path}/{version}/alarms/unacknowledgealarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarmPolicies(source, destination, refswitch, datatype, callback)</td>
    <td style="padding:15px">updateAlarmPolicies</td>
    <td style="padding:15px">{base_path}/{version}/alarms/updatealarmpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicies(policyName, body, callback)</td>
    <td style="padding:15px">UpdatePolicies</td>
    <td style="padding:15px">{base_path}/{version}/alarms/updatepolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPolicyFile(body, callback)</td>
    <td style="padding:15px">UploadPolicyFile</td>
    <td style="padding:15px">{base_path}/{version}/alarms/uploadpolicyfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverCSR(body, callback)</td>
    <td style="padding:15px">discoverCSR</td>
    <td style="padding:15px">{base_path}/{version}/cloud-extension/discover-csr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverNonNexus(body, callback)</td>
    <td style="padding:15px">discoverNonNexus</td>
    <td style="padding:15px">{base_path}/{version}/cloud-extension/discover-non-nexus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetStats(callback)</td>
    <td style="padding:15px">resetStats</td>
    <td style="padding:15px">{base_path}/{version}/cloud-extension/stats-reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stats(ipAddress, fetchLatestFromSwitch, callback)</td>
    <td style="padding:15px">stats</td>
    <td style="padding:15px">{base_path}/{version}/cloud-extension/stats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployExecConfig(templateName, body, callback)</td>
    <td style="padding:15px">deployExecConfig</td>
    <td style="padding:15px">{base_path}/{version}/config/delivery/{pathv1}/exec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupTracker(callback)</td>
    <td style="padding:15px">backupTracker</td>
    <td style="padding:15px">{base_path}/{version}/dcnmtracker/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdiscoveryEnableStatus(callback)</td>
    <td style="padding:15px">getdiscoveryEnableStatus</td>
    <td style="padding:15px">{base_path}/{version}/dcnmtracker/discoveryenable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatus(fabricName, switches, callback)</td>
    <td style="padding:15px">getStatus</td>
    <td style="padding:15px">{base_path}/{version}/dcnmtracker/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageTracker(fabricName, body, callback)</td>
    <td style="padding:15px">manageTracker</td>
    <td style="padding:15px">{base_path}/{version}/dcnmtracker/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFabrics(callback)</td>
    <td style="padding:15px">getAllFabrics</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFabricAssociations(callback)</td>
    <td style="padding:15px">listFabricAssociations</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/msd/fabric-associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoliciesForRestore(taskId, serialnum, callback)</td>
    <td style="padding:15px">getPoliciesForRestore</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/restoretaskpolicies/{pathv1}/forswitch/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchAsnIneBGPRoutedFabric(serialNumber, callback)</td>
    <td style="padding:15px">getSwitchAsnIneBGPRoutedFabric</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/switch-asn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterfaceGroup(fabricName, groupName, callback)</td>
    <td style="padding:15px">createInterfaceGroup</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfGroupInterfaces(fabricName, groupName, callback)</td>
    <td style="padding:15px">getIfGroup_interfaces</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/groups/{pathv2}/ifassoc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIfGroupIfAssoc(fabricName, groupName, body, callback)</td>
    <td style="padding:15px">createIfGroupIfAssoc</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/groups/{pathv2}/ifassoc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIfGroupNwAssoc(fabricName, groupName, body, callback)</td>
    <td style="padding:15px">createIfGroupNwAssoc</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/groups/{pathv2}/netassoc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfGroupNwAssoc(fabricName, groupName, body, callback)</td>
    <td style="padding:15px">deleteIfGroupNwAssoc</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/groups/{pathv2}/netassoc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfGroupIfAssoc(fabricName, body, callback)</td>
    <td style="padding:15px">deleteIfGroupIfAssoc</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/ifassoc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceGroups(fabricName, callback)</td>
    <td style="padding:15px">getInterfaceGroups</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/ifgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabric(fabricName, callback)</td>
    <td style="padding:15px">getFabric</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessMode(fabricName, callback)</td>
    <td style="padding:15px">getAccessMode</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/accessmode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configDeploy(fabricName, forceShowRun, inclAllMSDSwitches, callback)</td>
    <td style="padding:15px">configDeploy</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/config-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postconfigDeploy(fabricName, switchId, forceShowRun, callback)</td>
    <td style="padding:15px">configDeploy</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/config-deploy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configDiff(fabricName, switchId, forceShowRun, callback)</td>
    <td style="padding:15px">configDiff</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/config-diff/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configPreview(fabricName, forceShowRun, showBrief, inclAllMSDSwitches, shRunOptimization, callback)</td>
    <td style="padding:15px">configPreview</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/config-preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getconfigPreview(fabricName, sourceName, switchId, forceShowRun, showBrief, callback)</td>
    <td style="padding:15px">configPreview</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/config-preview/source/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getconfigPreview1(fabricName, switchId, forceShowRun, showBrief, recomputeMapEnable, shRunOptimization, callback)</td>
    <td style="padding:15px">configPreview</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/config-preview/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configSave(fabricName, callback)</td>
    <td style="padding:15px">configSave</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/config-save?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setFabricFreezeMode(fabricName, body, callback)</td>
    <td style="padding:15px">setFabricFreezeMode</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/fabricfreezeMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployMaintenanceModePti(fabricName, serialNumber, callback)</td>
    <td style="padding:15px">deployMaintenanceModePti</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/switches/{pathv2}/deploy-maintenance-mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFabricWithNvPairs(fabricName, templateName, body, callback)</td>
    <td style="padding:15px">createFabricWithNvPairs</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFabricWithNvPairs(fabricName, templateName, body, callback)</td>
    <td style="padding:15px">updateFabricWithNvPairs</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceUpdateDTO(serialNumber, ifName, callback)</td>
    <td style="padding:15px">getInterfaceUpdateDTO</td>
    <td style="padding:15px">{base_path}/{version}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInterface(body, callback)</td>
    <td style="padding:15px">addInterface</td>
    <td style="padding:15px">{base_path}/{version}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInterface(body, callback)</td>
    <td style="padding:15px">updateInterface</td>
    <td style="padding:15px">{base_path}/{version}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterface(body, callback)</td>
    <td style="padding:15px">deleteInterface</td>
    <td style="padding:15px">{base_path}/{version}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adminstatus(body, callback)</td>
    <td style="padding:15px">adminstatus</td>
    <td style="padding:15px">{base_path}/{version}/interface/adminstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBreakout(body, callback)</td>
    <td style="padding:15px">addBreakout</td>
    <td style="padding:15px">{base_path}/{version}/interface/breakout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deploy(body, callback)</td>
    <td style="padding:15px">deploy</td>
    <td style="padding:15px">{base_path}/{version}/interface/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobaInterface(serialNumber, callback)</td>
    <td style="padding:15px">getGlobaInterface</td>
    <td style="padding:15px">{base_path}/{version}/interface/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCombinedSerialNumber(serialNumber, callback)</td>
    <td style="padding:15px">getCombinedSerialNumber</td>
    <td style="padding:15px">{base_path}/{version}/interface/vpcpair_serial_number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSwitches(fabricName, callback)</td>
    <td style="padding:15px">getAllSwitches</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverSwitches(fabricName, body, callback)</td>
    <td style="padding:15px">discoverSwitches</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/inventory/discover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">switchesWaitingToPOAP(fabricName, callback)</td>
    <td style="padding:15px">switchesWaitingToPOAP</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/inventory/poap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startPoap(fabricName, body, callback)</td>
    <td style="padding:15px">startPoap</td>
    <td style="padding:15px">{base_path}/{version}/control/fabrics/{pathv1}/inventory/poap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLinks(switch1Sn, switch1IfName, switch2Sn, switch2IfName, callback)</td>
    <td style="padding:15px">getAllLinks</td>
    <td style="padding:15px">{base_path}/{version}/control/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLink(body, callback)</td>
    <td style="padding:15px">createLink</td>
    <td style="padding:15px">{base_path}/{version}/control/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExportLinks(fabricName, callback)</td>
    <td style="padding:15px">createExportLinks</td>
    <td style="padding:15px">{base_path}/{version}/control/links/fabrics/export/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricLinks(fabricName, callback)</td>
    <td style="padding:15px">getFabricLinks</td>
    <td style="padding:15px">{base_path}/{version}/control/links/fabrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createImportLinks(body, callback)</td>
    <td style="padding:15px">createImportLinks</td>
    <td style="padding:15px">{base_path}/{version}/control/links/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLogicalLinks(fabricName, serialNumber, swDBID, callback)</td>
    <td style="padding:15px">getAllLogicalLinks</td>
    <td style="padding:15px">{base_path}/{version}/control/links/logical-links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLink(linkUUID, callback)</td>
    <td style="padding:15px">getLink</td>
    <td style="padding:15px">{base_path}/{version}/control/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLink(linkUUID, body, callback)</td>
    <td style="padding:15px">updateLink</td>
    <td style="padding:15px">{base_path}/{version}/control/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLink(linkUUID, isLogicalLink, callback)</td>
    <td style="padding:15px">deleteLink</td>
    <td style="padding:15px">{base_path}/{version}/control/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabrics(msoFabricsOnly, callback)</td>
    <td style="padding:15px">getFabrics</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetFabric(fabricName, msoFabricsOnly, callback)</td>
    <td style="padding:15px">getFabric</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworks(fabricName, body, callback)</td>
    <td style="padding:15px">deleteNetworks</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/delete-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVrfs(fabricName, body, callback)</td>
    <td style="padding:15px">deleteVrfs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/delete-vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultiSiteConfigs(fabricName, callback)</td>
    <td style="padding:15px">getMultiSiteConfigs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/fabric-connectivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployMultiSiteConfigs(fabricName, body, callback)</td>
    <td style="padding:15px">deployMultiSiteConfigs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/fabric-connectivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDciOverlayConfigs(fabricName, callback)</td>
    <td style="padding:15px">getDciOverlayConfigs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/fabric-connectivity/dci-overlay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployDciOverlayConfigs(fabricName, body, callback)</td>
    <td style="padding:15px">deployDciOverlayConfigs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/fabric-connectivity/dci-overlay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDciOverlayConfigs(fabricName, deleteParam, body, callback)</td>
    <td style="padding:15px">updateDciOverlayConfigs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/fabric-connectivity/dci-overlay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDciUnderlayConfigs(fabricName, callback)</td>
    <td style="padding:15px">getDciUnderlayConfigs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/fabric-connectivity/dci-underlay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployDciUnderlayConfigs(fabricName, body, callback)</td>
    <td style="padding:15px">deployDciUnderlayConfigs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/fabric-connectivity/dci-underlay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListofFaults(fabricName, serialNumber, callback)</td>
    <td style="padding:15px">getListofFaults</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/faults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetAllSwitches(fabricName, callback)</td>
    <td style="padding:15px">getAllSwitches</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(fabricName, tenant, callback)</td>
    <td style="padding:15px">getNetworks</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployNetworks(fabricName, body, callback)</td>
    <td style="padding:15px">deployNetworks</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworks(fabricName, body, callback)</td>
    <td style="padding:15px">updateNetworks</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVniUsage(fabricName, callback)</td>
    <td style="padding:15px">getVniUsage</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/resources/vni-usages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFabricToSiteGroup(fabricName, body, callback)</td>
    <td style="padding:15px">addFabricToSiteGroup</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/site-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteGroup(fabricName, body, callback)</td>
    <td style="padding:15px">updateSiteGroup</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/site-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFabricFromSiteGroup(fabricName, callback)</td>
    <td style="padding:15px">deleteFabricFromSiteGroup</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/site-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVrfs(fabricName, tenant, callback)</td>
    <td style="padding:15px">getVrfs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployVrfs(fabricName, body, callback)</td>
    <td style="padding:15px">deployVrfs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVrfs(fabricName, body, callback)</td>
    <td style="padding:15px">updateVrfs</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/fabrics/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetGlobaInterface(serialNumber, showOverlayAttachableOnly, showUnderlayInfraOnly, showLookpbackOnly, ifName, callback)</td>
    <td style="padding:15px">getGlobaInterface</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">convertMsoSiteGroupToMsd(siteGroupName, callback)</td>
    <td style="padding:15px">convertMsoSiteGroupToMsd</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/site-groups/{pathv1}/msd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplates(callback)</td>
    <td style="padding:15px">getTemplates</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatesFields(templateName, callback)</td>
    <td style="padding:15px">getTemplatesFields</td>
    <td style="padding:15px">{base_path}/{version}/control/multisite/templates/{pathv1}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postControlPolicies(body, callback)</td>
    <td style="padding:15px">createPolicy</td>
    <td style="padding:15px">{base_path}/{version}/control/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPolicies(body, callback)</td>
    <td style="padding:15px">deployPolicies</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateKey(type, paramOne, paramTwo, paramThree, paramFour, callback)</td>
    <td style="padding:15px">generateKey</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePoliciesInBulk(policyIds, callback)</td>
    <td style="padding:15px">deletePoliciesInBulk</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/policyIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPtiHistory(serialNumber, callback)</td>
    <td style="padding:15px">getPtiHistory</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/ptiHistory/switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipleSwitchPolicies(serialNumber, source, responseType, callback)</td>
    <td style="padding:15px">getMultipleSwitchPolicies</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPoliciesInSwitchesAndSource(serialNumbers, source, body, callback)</td>
    <td style="padding:15px">createPoliciesInSwitchesAndSource</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePoliciesInSwitchesAndSource(serialNumbers, source, body, callback)</td>
    <td style="padding:15px">updatePoliciesInSwitchesAndSource</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePoliciesInSwitchesAndSource(serialNumbers, source, callback)</td>
    <td style="padding:15px">deletePoliciesInSwitchesAndSource</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchPolicies(serialNumber, source, responseType, callback)</td>
    <td style="padding:15px">getSwitchPolicies</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markDeleteSwitchPolicies(serialNumber, callback)</td>
    <td style="padding:15px">markDeleteSwitchPolicies</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSwitchPolicies(serialNumber, callback)</td>
    <td style="padding:15px">deleteSwitchPolicies</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchIntentConfig(serialNumber, includeDelete, includePolicyid, callback)</td>
    <td style="padding:15px">getSwitchIntentConfig</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches/{pathv1}/intent-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchPolicies4Entity(serialNumber, entityType, entityName, responseType, callback)</td>
    <td style="padding:15px">getSwitchPolicies4Entity</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markDeleteSwitchPolicies4Entity(serialNumber, entityType, entityName, callback)</td>
    <td style="padding:15px">markDeleteSwitchPolicies4Entity</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSwitchPolicies4Entity(serialNumber, entityType, entityName, callback)</td>
    <td style="padding:15px">deleteSwitchPolicies4Entity</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/switches/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControlPoliciesPolicyId(policyId, callback)</td>
    <td style="padding:15px">getPolicy</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(policyId, body, callback)</td>
    <td style="padding:15px">updatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(policyId, callback)</td>
    <td style="padding:15px">deletePolicy</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyConfig(policyId, callback)</td>
    <td style="padding:15px">getPolicyConfig</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/{pathv1}/intent-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markDeletePolicy(policyId, callback)</td>
    <td style="padding:15px">markDeletePolicy</td>
    <td style="padding:15px">{base_path}/{version}/control/policies/{pathv1}/mark-delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchesRole(serialNumber, callback)</td>
    <td style="padding:15px">getSwitchesRole</td>
    <td style="padding:15px">{base_path}/{version}/control/switches/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSwitchesRole(body, callback)</td>
    <td style="padding:15px">setSwitchesRole</td>
    <td style="padding:15px">{base_path}/{version}/control/switches/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricNameForSwitch(serialNumber, callback)</td>
    <td style="padding:15px">getFabricNameForSwitch</td>
    <td style="padding:15px">{base_path}/{version}/control/switches/{pathv1}/fabric-name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAddressForSwitch(serialNumber, callback)</td>
    <td style="padding:15px">getIPAddressForSwitch</td>
    <td style="padding:15px">{base_path}/{version}/control/switches/{pathv1}/myip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMOTD(callback)</td>
    <td style="padding:15px">getMOTD</td>
    <td style="padding:15px">{base_path}/{version}/customization/motd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setMOTD(body, callback)</td>
    <td style="padding:15px">setMOTD</td>
    <td style="padding:15px">{base_path}/{version}/customization/motd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreDefaults(callback)</td>
    <td style="padding:15px">restoreDefaults</td>
    <td style="padding:15px">{base_path}/{version}/customization/restore-defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSplashScreenImage(callback)</td>
    <td style="padding:15px">getSplashScreenImage</td>
    <td style="padding:15px">{base_path}/{version}/customization/splash-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeSplashScreenImage(body, callback)</td>
    <td style="padding:15px">changeSplashScreenImage</td>
    <td style="padding:15px">{base_path}/{version}/customization/splash-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeSplashScreenImage(callback)</td>
    <td style="padding:15px">removeSplashScreenImage</td>
    <td style="padding:15px">{base_path}/{version}/customization/splash-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">triggerBackup(fabricName, tag, callback)</td>
    <td style="padding:15px">triggerBackup</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllBackups(fabricName, callback)</td>
    <td style="padding:15px">deleteAllBackups</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/deleteAllBackups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSingleBackup(fabricName, backupVersion, callback)</td>
    <td style="padding:15px">deleteSingleBackup</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/deleteBackup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUploadedEPLDImagesList(platform, callback)</td>
    <td style="padding:15px">getUploadedEPLDImagesList</td>
    <td style="padding:15px">{base_path}/{version}/epldmgnt/epld-images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEPLDModuleList(ipAddress, epldName, callback)</td>
    <td style="padding:15px">getEPLDModuleList</td>
    <td style="padding:15px">{base_path}/{version}/epldmgnt/epld-module?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">execute(body, callback)</td>
    <td style="padding:15px">execute</td>
    <td style="padding:15px">{base_path}/{version}/epldmgnt/epld-upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachPolicy(body, callback)</td>
    <td style="padding:15px">attachPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/attach-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachedPolicies(serialNumber, callback)</td>
    <td style="padding:15px">getAttachedPolicies</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/attached-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyMapping(serialNumber, callback)</td>
    <td style="padding:15px">deletePolicyMapping</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/detach-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getuploadedNXOSList(platform, callback)</td>
    <td style="padding:15px">getuploadedNXOSList</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/nxos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPackageList(platform, nxosversion, callback)</td>
    <td style="padding:15px">getPackageList</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlatformPolicies(callback)</td>
    <td style="padding:15px">getPlatformPolicies</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/platform-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreatePolicy(body, callback)</td>
    <td style="padding:15px">createPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/platform-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getuploadedPlatformList(callback)</td>
    <td style="padding:15px">getuploadedPlatformList</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/platforms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPolicies(callback)</td>
    <td style="padding:15px">getAllPolicies</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeletePolicy(body, callback)</td>
    <td style="padding:15px">deletePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUmberellaPolicy(body, callback)</td>
    <td style="padding:15px">createUmberellaPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policymgnt/umbrella-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndPorts(callback)</td>
    <td style="padding:15px">getEndPorts</td>
    <td style="padding:15px">{base_path}/{version}/inventory/endports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetFabrics(callback)</td>
    <td style="padding:15px">getFabrics</td>
    <td style="padding:15px">{base_path}/{version}/inventory/fabrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostEnclList(callback)</td>
    <td style="padding:15px">getHostEnclList</td>
    <td style="padding:15px">{base_path}/{version}/inventory/getHostEnclList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLandingDashboardTopNAnalytics(queryScope, queryScopeID, queryType, clientRequestId, callback)</td>
    <td style="padding:15px">getLandingDashboardTopNAnalytics</td>
    <td style="padding:15px">{base_path}/{version}/inventory/getLandingDashboardTopNAnalytics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceDetails(serialNumber, ifName, callback)</td>
    <td style="padding:15px">getInterfaceDetails</td>
    <td style="padding:15px">{base_path}/{version}/inventory/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIsls(callback)</td>
    <td style="padding:15px">getIsls</td>
    <td style="padding:15px">{base_path}/{version}/inventory/isls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenses(callback)</td>
    <td style="padding:15px">getLicenses</td>
    <td style="padding:15px">{base_path}/{version}/inventory/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModules(callback)</td>
    <td style="padding:15px">getModules</td>
    <td style="padding:15px">{base_path}/{version}/inventory/modules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeighborLinks(callback)</td>
    <td style="padding:15px">getNeighborLinks</td>
    <td style="padding:15px">{base_path}/{version}/inventory/neighborlinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeighborLinksBySwitch(callback)</td>
    <td style="padding:15px">getNeighborLinksBySwitch</td>
    <td style="padding:15px">{base_path}/{version}/inventory/neighborlinksBySwitch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNpvLinks(callback)</td>
    <td style="padding:15px">getNpvLinks</td>
    <td style="padding:15px">{base_path}/{version}/inventory/npvlinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitches(callback)</td>
    <td style="padding:15px">getSwitches</td>
    <td style="padding:15px">{base_path}/{version}/inventory/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthScores(callback)</td>
    <td style="padding:15px">updateHealthScores</td>
    <td style="padding:15px">{base_path}/{version}/inventory/updateHealthScores?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVsans(callback)</td>
    <td style="padding:15px">getVsans</td>
    <td style="padding:15px">{base_path}/{version}/inventory/vsans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZones(callback)</td>
    <td style="padding:15px">getZones</td>
    <td style="padding:15px">{base_path}/{version}/inventory/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activatePackage(body, callback)</td>
    <td style="padding:15px">activatePackage</td>
    <td style="padding:15px">{base_path}/{version}/packagemgnt/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deActivatePackage(body, callback)</td>
    <td style="padding:15px">deActivatePackage</td>
    <td style="padding:15px">{base_path}/{version}/packagemgnt/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">install(body, callback)</td>
    <td style="padding:15px">install</td>
    <td style="padding:15px">{base_path}/{version}/packagemgnt/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPackages(fabric, switchName, nxosVersion, pkgName, serialNumber, pkgVersion, pkgType, pkgStatus, callback)</td>
    <td style="padding:15px">getPackages</td>
    <td style="padding:15px">{base_path}/{version}/packagemgnt/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uninstall(body, callback)</td>
    <td style="padding:15px">uninstall</td>
    <td style="padding:15px">{base_path}/{version}/packagemgnt/uninstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUploadedPackageList(platform, version, callback)</td>
    <td style="padding:15px">getUploadedPackageList</td>
    <td style="padding:15px">{base_path}/{version}/packagemgnt/uploaded-packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getISSUSwitches(fabricId, epld, rpm, callback)</td>
    <td style="padding:15px">getISSUSwitches</td>
    <td style="padding:15px">{base_path}/{version}/packagemgnt/{pathv1}/issu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTPCorrections(switchName, date, startTimestamp, endTimestamp, lastXSeconds, callback)</td>
    <td style="padding:15px">getPTPCorrections</td>
    <td style="padding:15px">{base_path}/{version}/ptp/corrections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTPParents(switchName, callback)</td>
    <td style="padding:15px">getPTPParents</td>
    <td style="padding:15px">{base_path}/{version}/ptp/parents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTPPortStatuses(switchName, callback)</td>
    <td style="padding:15px">getPTPPortStatuses</td>
    <td style="padding:15px">{base_path}/{version}/ptp/portStatuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTPThreshold(callback)</td>
    <td style="padding:15px">getPTPThreshold</td>
    <td style="padding:15px">{base_path}/{version}/ptp/threshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBandwidth(body, callback)</td>
    <td style="padding:15px">updateBandwidth</td>
    <td style="padding:15px">{base_path}/{version}/ptp/threshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcnmVersion(callback)</td>
    <td style="padding:15px">getDcnmVersion</td>
    <td style="padding:15px">{base_path}/{version}/dcnm-version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">login(body, callback)</td>
    <td style="padding:15px">login</td>
    <td style="padding:15px">{base_path}/{version}/logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logout(callback)</td>
    <td style="padding:15px">logout</td>
    <td style="padding:15px">{base_path}/{version}/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTemplate(callback)</td>
    <td style="padding:15px">getAllTemplate</td>
    <td style="padding:15px">{base_path}/{version}/config/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateBulk(body, callback)</td>
    <td style="padding:15px">deleteTemplateBulk</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/delete/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplateJson2(templateName, body, callback)</td>
    <td style="padding:15px">createTemplateJson2</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateTemplate(body, callback)</td>
    <td style="padding:15px">validateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplate(templateName, populate, callback)</td>
    <td style="padding:15px">getTemplate</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTemplate(templateName, body, callback)</td>
    <td style="padding:15px">modifyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplate(templateName, callback)</td>
    <td style="padding:15px">deleteTemplate</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateDefaultValue(templateName, body, callback)</td>
    <td style="padding:15px">getTemplateDefaultValue</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/{pathv1}/defaultvalue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPopulatedContent(templateName, deviceIP, body, callback)</td>
    <td style="padding:15px">getPopulatedContent</td>
    <td style="padding:15px">{base_path}/{version}/config/templates/{pathv1}/populate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createJob(body, callback)</td>
    <td style="padding:15px">createJob</td>
    <td style="padding:15px">{base_path}/{version}/imagemanagement/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskList(jobId, callback)</td>
    <td style="padding:15px">getTaskList</td>
    <td style="padding:15px">{base_path}/{version}/imagemanagement/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllJobs(callback)</td>
    <td style="padding:15px">getAllJobs</td>
    <td style="padding:15px">{base_path}/{version}/imagemanagement/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJob(jobId, callback)</td>
    <td style="padding:15px">deleteJob</td>
    <td style="padding:15px">{base_path}/{version}/imagemanagement/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBootMode(switchId, callback)</td>
    <td style="padding:15px">getBootMode</td>
    <td style="padding:15px">{base_path}/{version}/imagemanagement/mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstalTaskForDevice(switchIp, callback)</td>
    <td style="padding:15px">getInstalTaskForDevice</td>
    <td style="padding:15px">{base_path}/{version}/imagemanagement/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServers(callback)</td>
    <td style="padding:15px">listServers</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServer(body, callback)</td>
    <td style="padding:15px">createServer</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartImageUpload(body, callback)</td>
    <td style="padding:15px">SmartImageUpload</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/SmartImageUpload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadFile(serverName, fileType, platformType, body, callback)</td>
    <td style="padding:15px">uploadFile</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/UploadImageFiles/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFiles(serverName, fileType, callback)</td>
    <td style="padding:15px">listFiles</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listImageFiles(serverName, fileType, callback)</td>
    <td style="padding:15px">listImageFiles</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/imagefiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartImageDelete(body, callback)</td>
    <td style="padding:15px">smartImageDelete</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/smart-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartImageDataUpload(body, callback)</td>
    <td style="padding:15px">smartImageDataUpload</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/smart-image-upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartDeleteImages(imageName, callback)</td>
    <td style="padding:15px">smartDeleteImages</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/smartDeleteImages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartListImages(callback)</td>
    <td style="padding:15px">smartListImages</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/smartListImages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerStatus(body, callback)</td>
    <td style="padding:15px">getServerStatus</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartImagesUploadedList(parentId, pathParam, callback)</td>
    <td style="padding:15px">smartImagesUploadedList</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/uploaded-images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServer(serverName, callback)</td>
    <td style="padding:15px">getServer</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServer(serverName, body, callback)</td>
    <td style="padding:15px">updateServer</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServer(serverName, callback)</td>
    <td style="padding:15px">deleteServer</td>
    <td style="padding:15px">{base_path}/{version}/poap/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchDirectorySize(id, type = 'DEVICE', ip, userId, pathParam, onDemand, serverName, callback)</td>
    <td style="padding:15px">fetchDirectorySize</td>
    <td style="padding:15px">{base_path}/{version}/repomanagement/fetchdirsize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchDirectoryContents(id, type = 'DEVICE', ip, userId, filter, pathParam, include, serverName, callback)</td>
    <td style="padding:15px">fetchDirectoryContents</td>
    <td style="padding:15px">{base_path}/{version}/repomanagement/fetchfirstlevelfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFile(pathParam, type = 'DEVICE', ip, userId, serverName, callback)</td>
    <td style="padding:15px">deleteFile</td>
    <td style="padding:15px">{base_path}/{version}/repomanagement/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchListOfFiles(id, type = 'DEVICE', ip, userId, filter, pathParam, serverName, filterText, callback)</td>
    <td style="padding:15px">searchListOfFiles</td>
    <td style="padding:15px">{base_path}/{version}/repomanagement/searchlistfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServers(callback)</td>
    <td style="padding:15px">getConfigServers</td>
    <td style="padding:15px">{base_path}/{version}/repomanagement/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemDefinedDelimiter(callback)</td>
    <td style="padding:15px">getSystemDefinedDelimiter</td>
    <td style="padding:15px">{base_path}/{version}/progress-tracker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateResourceId(body, callback)</td>
    <td style="padding:15px">allocateResourceId</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/allocate-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolDetailsOfFabric(fabricName, callback)</td>
    <td style="padding:15px">getPoolDetailsOfFabric</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/fabric/{pathv1}/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolDataOfFabricAndPool(fabricName, poolName, callback)</td>
    <td style="padding:15px">getPoolDataOfFabricAndPool</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/fabric/{pathv1}/pools/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceUsageforFabric(fabricName, callback)</td>
    <td style="padding:15px">listResourceUsageforFabric</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/fabrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPoolNames(fabricName, callback)</td>
    <td style="padding:15px">listPoolNames</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/fabrics/{pathv1}/pool-names?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResource(fabricName, body, callback)</td>
    <td style="padding:15px">addResource</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/fabrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMetaData(body, callback)</td>
    <td style="padding:15px">addMetaData</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseResourceId(body, callback)</td>
    <td style="padding:15px">releaseResourceId</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/release-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseResource(id, callback)</td>
    <td style="padding:15px">releaseResource</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOrUpdatePool(body, callback)</td>
    <td style="padding:15px">addOrUpdatePool</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/subnet-ip-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateIpResourceAndReturn(body, callback)</td>
    <td style="padding:15px">allocateIpResourceAndReturn</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/subnet-ip-pool/allocate-resource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseResourceIp(rmid, callback)</td>
    <td style="padding:15px">releaseResourceIp</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/subnet-ip-pool/release-resource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkSubnetOrIpResourceAvailable(fabricName, entityType, entityName, oldSubnetIP, newSubnetIP, callback)</td>
    <td style="padding:15px">checkSubnetOrIpResourceAvailable</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/subnet-ip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolDetailsOfSwitch(serialNumber, callback)</td>
    <td style="padding:15px">getPoolDetailsOfSwitch</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/switch/{pathv1}/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolDataOfSwitchAndPool(serialNumber, poolName, callback)</td>
    <td style="padding:15px">getPoolDataOfSwitchAndPool</td>
    <td style="padding:15px">{base_path}/{version}/resource-manager/switch/{pathv1}/pools/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServiceNetworks(fabricName, callback)</td>
    <td style="padding:15px">listServiceNetworks</td>
    <td style="padding:15px">{base_path}/{version}/l4l7-service/fabrics/{pathv1}/servicenetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyIntent(body, callback)</td>
    <td style="padding:15px">applyIntent</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/apply_intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntent(body, callback)</td>
    <td style="padding:15px">createIntent</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/create_intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntent(body, callback)</td>
    <td style="padding:15px">deleteIntent</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/delete_intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricInfo(callback)</td>
    <td style="padding:15px">getFabricInfo</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/fabrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPreviewInfo(appName, cliTag, fabricName, switchList, callback)</td>
    <td style="padding:15px">getPreviewInfo</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/get_preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobStatus(appName, cliTag, jobId, verbose, callback)</td>
    <td style="padding:15px">getJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/job_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchesInfo(fabricId, callback)</td>
    <td style="padding:15px">getSwitchesInfo</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkNetwork(body, callback)</td>
    <td style="padding:15px">createBulkNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/bulk-create/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkImportNetwork(body, callback)</td>
    <td style="padding:15px">createBulkImportNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/bulk-import/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworks(fabricName, vrfName, callback)</td>
    <td style="padding:15px">listNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetwork(fabricName, body, callback)</td>
    <td style="padding:15px">createNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAttachDetails(fabricName, networkNames, callback)</td>
    <td style="padding:15px">getNetworkAttachDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachNetworks(fabricName, body, callback)</td>
    <td style="padding:15px">attachNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postdeployNetworks(fabricName, body, callback)</td>
    <td style="padding:15px">deployNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewNetworks(fabricName, networkNames, callback)</td>
    <td style="padding:15px">previewNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPortsAndStatus(fabricName, networkNames, serialNumbers, callback)</td>
    <td style="padding:15px">getAllPortsAndStatus</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchDeploymentHistory(fabricName, networkNames, serialNumbers, callback)</td>
    <td style="padding:15px">getSwitchDeploymentHistory</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/switches/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">getNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetwork(fabricName, networkName, body, callback)</td>
    <td style="padding:15px">updateNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">deleteNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleNetworkAttachDetails(fabricName, networkName, callback)</td>
    <td style="padding:15px">getSingleNetworkAttachDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachNetwork(fabricName, networkName, body, callback)</td>
    <td style="padding:15px">attachNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">deployNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">previewNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkStatusDetails(fabricName, networkName, serialNumber, callback)</td>
    <td style="padding:15px">networkStatusDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleSwitchDeploymentHistory(fabricName, networkName, serialNumber, callback)</td>
    <td style="padding:15px">getSingleSwitchDeploymentHistory</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}/switches/{pathv3}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPortsAndStatusForSingle(fabricName, networkName, serialNumber, callback)</td>
    <td style="padding:15px">getAllPortsAndStatusForSingle</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/networks/{pathv2}/switches/{pathv3}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">deleteServiceNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/servicenetworks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreateBulkNetwork(body, callback)</td>
    <td style="padding:15px">createBulkNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/bulk-create/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreateBulkImportNetwork(body, callback)</td>
    <td style="padding:15px">createBulkImportNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/bulk-import/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getlistNetworks(fabricName, vrfName, callback)</td>
    <td style="padding:15px">listNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreateNetwork(fabricName, body, callback)</td>
    <td style="padding:15px">createNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetNetworkAttachDetails(fabricName, networkNames, callback)</td>
    <td style="padding:15px">getNetworkAttachDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postattachNetworks(fabricName, body, callback)</td>
    <td style="padding:15px">attachNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postdeployNetworks1(fabricName, body, callback)</td>
    <td style="padding:15px">deployNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getpreviewNetworks(fabricName, networkNames, callback)</td>
    <td style="padding:15px">previewNetworks</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetAllPortsAndStatus(fabricName, networkNames, serialNumbers, callback)</td>
    <td style="padding:15px">getAllPortsAndStatus</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSwitchDeploymentHistory(fabricName, networkNames, serialNumbers, callback)</td>
    <td style="padding:15px">getSwitchDeploymentHistory</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/switches/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">getNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putupdateNetwork(fabricName, networkName, body, callback)</td>
    <td style="padding:15px">updateNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeleteNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">deleteNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSingleNetworkAttachDetails(fabricName, networkName, callback)</td>
    <td style="padding:15px">getSingleNetworkAttachDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postattachNetwork(fabricName, networkName, body, callback)</td>
    <td style="padding:15px">attachNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postdeployNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">deployNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getpreviewNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">previewNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getnetworkStatusDetails(fabricName, networkName, serialNumber, callback)</td>
    <td style="padding:15px">networkStatusDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSingleSwitchDeploymentHistory(fabricName, networkName, serialNumber, callback)</td>
    <td style="padding:15px">getSingleSwitchDeploymentHistory</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}/switches/{pathv3}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetAllPortsAndStatusForSingle(fabricName, networkName, serialNumber, callback)</td>
    <td style="padding:15px">getAllPortsAndStatusForSingle</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/networks/{pathv2}/switches/{pathv3}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeleteServiceNetwork(fabricName, networkName, callback)</td>
    <td style="padding:15px">deleteServiceNetwork</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/servicenetworks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkVRF(body, callback)</td>
    <td style="padding:15px">createBulkVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/bulk-create/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkImportVRF(body, callback)</td>
    <td style="padding:15px">createBulkImportVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/bulk-import/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCdpNeighbor(body, callback)</td>
    <td style="padding:15px">getCdpNeighbor</td>
    <td style="padding:15px">{base_path}/{version}/top-down/cdpneighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployExtensionPrototype(fabricName, body, callback)</td>
    <td style="padding:15px">deployExtensionPrototype</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/extension-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateExtensionPreview(fabricName, body, callback)</td>
    <td style="padding:15px">generateExtensionPreview</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/extension-preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceVRF(fabricName, vrfName, callback)</td>
    <td style="padding:15px">deleteServiceVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/servicevrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVRF(fabricName, callback)</td>
    <td style="padding:15px">listVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVRF(fabricName, body, callback)</td>
    <td style="padding:15px">createVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVRFAttachDetails(fabricName, vrfNames, callback)</td>
    <td style="padding:15px">getVRFAttachDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachVrfs(fabricName, body, callback)</td>
    <td style="padding:15px">attachVrfs</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postdeployVrfs(fabricName, body, callback)</td>
    <td style="padding:15px">deployVrfs</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewVrfs(fabricName, vrfNames, callback)</td>
    <td style="padding:15px">previewVrfs</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchDetailsByVRF(fabricName, vrfNames, serialNumbers, callback)</td>
    <td style="padding:15px">getSwitchDetailsByVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSwitchDeploymentHistory1(fabricName, vrfNames, serialNumbers, callback)</td>
    <td style="padding:15px">getSwitchDeploymentHistory</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/switches/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVRF(fabricName, vrfName, callback)</td>
    <td style="padding:15px">getVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVRF(fabricName, vrfName, body, callback)</td>
    <td style="padding:15px">updateVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVRF(fabricName, vrfName, callback)</td>
    <td style="padding:15px">deleteVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/fabrics/{pathv1}/vrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreateBulkVRF(body, callback)</td>
    <td style="padding:15px">createBulkVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/bulk-create/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreateBulkImportVRF(body, callback)</td>
    <td style="padding:15px">createBulkImportVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/bulk-import/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postgetCdpNeighbor(body, callback)</td>
    <td style="padding:15px">getCdpNeighbor</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/cdpneighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeleteServiceVRF(fabricName, vrfName, callback)</td>
    <td style="padding:15px">deleteServiceVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/servicevrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getlistVRF(fabricName, callback)</td>
    <td style="padding:15px">listVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreateVRF(fabricName, body, callback)</td>
    <td style="padding:15px">createVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetVRFAttachDetails(fabricName, vrfNames, callback)</td>
    <td style="padding:15px">getVRFAttachDetails</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postattachVrfs(fabricName, body, callback)</td>
    <td style="padding:15px">attachVrfs</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postdeployVrfs1(fabricName, body, callback)</td>
    <td style="padding:15px">deployVrfs</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getpreviewVrfs(fabricName, vrfNames, callback)</td>
    <td style="padding:15px">previewVrfs</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSwitchDetailsByVRF(fabricName, vrfNames, serialNumbers, callback)</td>
    <td style="padding:15px">getSwitchDetailsByVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSwitchDeploymentHistory12(fabricName, vrfNames, serialNumbers, callback)</td>
    <td style="padding:15px">getSwitchDeploymentHistory</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/switches/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetVRF(fabricName, vrfName, callback)</td>
    <td style="padding:15px">getVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putupdateVRF(fabricName, vrfName, body, callback)</td>
    <td style="padding:15px">updateVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeleteVRF(fabricName, vrfName, callback)</td>
    <td style="padding:15px">deleteVRF</td>
    <td style="padding:15px">{base_path}/{version}/top-down/v2/fabrics/{pathv1}/vrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogsUsingGET(attachedFabric, end, start, callback)</td>
    <td style="padding:15px">getAuditLogsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/audit-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuditLogsUsingDELETE(daysToRetain, callback)</td>
    <td style="padding:15px">deleteAuditLogsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/audit-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutePeeringsUsingGET1(excludeDetail, fabricName, nodeName, callback)</td>
    <td style="padding:15px">getRoutePeeringsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRoutePeeringUsingPOST(fabricName, nodeName, body, callback)</td>
    <td style="padding:15px">addRoutePeeringUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoutePeeringUsingDELETE2(fabricName, forceDeletion, nodeName, callback)</td>
    <td style="padding:15px">deleteRoutePeeringUsingDELETE_2</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutePeeringsUsingGET(attachedFabric, excludeDetail, fabricName, nodeName, callback)</td>
    <td style="padding:15px">getRoutePeeringsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoutePeeringUsingDELETE1(attachedFabric, fabricName, nodeName, callback)</td>
    <td style="padding:15px">deleteRoutePeeringUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachPeeringsUsingPOST(attachedFabric, fabricName, nodeName, body, callback)</td>
    <td style="padding:15px">attachPeeringsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachPeeringsUsingDELETE(attachedFabric, fabricName, nodeName, peeringNames, callback)</td>
    <td style="padding:15px">detachPeeringsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPeeringsUsingPOST(attachedFabric, fabricName, nodeName, body, callback)</td>
    <td style="padding:15px">deployPeeringsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutePeeringUsingGET(attachedFabric, fabricName, nodeName, peeringName, callback)</td>
    <td style="padding:15px">getRoutePeeringUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoutePeeringUsingPUT(attachedFabric, fabricName, nodeName, peeringName, body, callback)</td>
    <td style="padding:15px">updateRoutePeeringUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoutePeeringUsingDELETE(attachedFabric, fabricName, nodeName, peeringName, callback)</td>
    <td style="padding:15px">deleteRoutePeeringUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPeeringAttachmentsUsingGET(attachedFabric, fabricName, nodeName, peeringName, callback)</td>
    <td style="padding:15px">getPeeringAttachmentsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/{pathv4}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachPeeringUsingPOST(attachedFabric, fabricName, nodeName, peeringName, callback)</td>
    <td style="padding:15px">attachPeeringUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/{pathv4}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachPeeringUsingDELETE(attachedFabric, fabricName, nodeName, peeringName, callback)</td>
    <td style="padding:15px">detachPeeringUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/{pathv4}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPeeringUsingPOST(attachedFabric, fabricName, nodeName, peeringName, callback)</td>
    <td style="padding:15px">deployPeeringUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/peerings/{pathv3}/{pathv4}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachedServiceNodesUsingGET(attachedFabric, switchSerialNumber, callback)</td>
    <td style="padding:15px">getAttachedServiceNodesUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceNodesUsingGET1(fabricName, callback)</td>
    <td style="padding:15px">getServiceNodesUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addServiceNodeUsingPOST(fabricName, body, callback)</td>
    <td style="padding:15px">addServiceNodeUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceNodeUsingGET(fabricName, nodeName, callback)</td>
    <td style="padding:15px">getServiceNodeUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceNodeUsingPUT1(fabricName, nodeName, body, callback)</td>
    <td style="padding:15px">updateServiceNodeUsingPUT_1</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceNodeUsingDELETE1(fabricName, forceDeletion, nodeName, retainSwitch, callback)</td>
    <td style="padding:15px">deleteServiceNodeUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicePoliciesUsingGET1(excludeDetail, fabricName, nodeName, callback)</td>
    <td style="padding:15px">getServicePoliciesUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addServicePolicyUsingPOST(fabricName, nodeName, body, callback)</td>
    <td style="padding:15px">addServicePolicyUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicePolicyUsingDELETE2(fabricName, forceDeletion, nodeName, callback)</td>
    <td style="padding:15px">deleteServicePolicyUsingDELETE_2</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicePoliciesUsingGET(attachedFabric, excludeDetail, fabricName, nodeName, callback)</td>
    <td style="padding:15px">getServicePoliciesUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicePolicyUsingDELETE1(attachedFabric, fabricName, nodeName, callback)</td>
    <td style="padding:15px">deleteServicePolicyUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachPoliciesUsingPOST(attachedFabric, fabricName, nodeName, body, callback)</td>
    <td style="padding:15px">attachPoliciesUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachPoliciesUsingDELETE(attachedFabric, fabricName, nodeName, policyNames, callback)</td>
    <td style="padding:15px">detachPoliciesUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPoliciesUsingPOST(attachedFabric, fabricName, nodeName, body, callback)</td>
    <td style="padding:15px">deployPoliciesUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicePolicyUsingGET(attachedFabric, fabricName, nodeName, policyName, callback)</td>
    <td style="padding:15px">getServicePolicyUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServicePolicyUsingPUT(attachedFabric, fabricName, nodeName, policyName, body, callback)</td>
    <td style="padding:15px">updateServicePolicyUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicePolicyUsingDELETE(attachedFabric, fabricName, nodeName, policyName, callback)</td>
    <td style="padding:15px">deleteServicePolicyUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyAttachmentsUsingGET(attachedFabric, fabricName, includeDestNetwork, includeSrcNetwork, nodeName, policyName, callback)</td>
    <td style="padding:15px">getPolicyAttachmentsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/{pathv4}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachPolicyUsingPOST(attachedFabric, fabricName, nodeName, policyName, callback)</td>
    <td style="padding:15px">attachPolicyUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/{pathv4}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachPolicyUsingDELETE(attachedFabric, fabricName, nodeName, policyName, callback)</td>
    <td style="padding:15px">detachPolicyUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/{pathv4}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPolicyUsingPOST(attachedFabric, fabricName, nodeName, policyName, callback)</td>
    <td style="padding:15px">deployPolicyUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/policies/{pathv3}/{pathv4}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPBRStatsUsingGET(attachedFabric, end, fabricName, nodeName, nodeType = 'Firewall', policyName, start, callback)</td>
    <td style="padding:15px">getPBRStatsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/{pathv3}/policies/{pathv4}/{pathv5}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearPBRStatsUsingDELETE(attachedFabric, fabricName, nodeName, nodeType = 'Firewall', policyName, callback)</td>
    <td style="padding:15px">clearPBRStatsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/fabrics/{pathv1}/service-nodes/{pathv2}/{pathv3}/policies/{pathv4}/{pathv5}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicePoliciesForSwitchUsingGET(attachedFabric, switchSerialNumber, callback)</td>
    <td style="padding:15px">getServicePoliciesForSwitchUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findEndpointsUsingGET(fabricName, ip, mac, vni, callback)</td>
    <td style="padding:15px">findEndpointsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveCountUsingGET(fabricName, type = 'endpoint', callback)</td>
    <td style="padding:15px">getActiveCountUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getActiveCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSwitchesGroupedByNetworkUsingGET(fabricName, callback)</td>
    <td style="padding:15px">getActiveSwitchesGroupedByNetworkUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getActiveSwitchesGroupedByNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataTableUsingGET(fabricName, size, type = 'activeEndPointList', callback)</td>
    <td style="padding:15px">getDataTableUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getDataTable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistoricDataUsingGET(endDate, fabricName, startDate, type = 'endpoint', callback)</td>
    <td style="padding:15px">getHistoricDataUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getHistoricData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistoricDataDownloadUsingGET(endDate, fabricName, startDate, status = 'active', subType, type = 'endpoint', callback)</td>
    <td style="padding:15px">getHistoricDataDownloadUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getHistoricDataDownload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostLifeDataUsingGET(fabricName, ip, vni, callback)</td>
    <td style="padding:15px">getHostLifeDataUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getHostLifeData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeNameUsingGET(macAdd, callback)</td>
    <td style="padding:15px">getNodeNameUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getNodeName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopEndPointCountsUsingGET(fabricName, type = 'switch', callback)</td>
    <td style="padding:15px">getTopEndPointCountsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getTopEndPointCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrendsUsingGET(fabricName, type = 'endpoint', callback)</td>
    <td style="padding:15px">getTrendsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/getTrends?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
